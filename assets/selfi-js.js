var config = {
			show_restart: true,
			show_analyzed_image: true,
			auto_capture: true,
			show_gallery_control: true,
			show_capture_control: true,
			show_instructions: false,
			show_retake_control: true,
			show_accept_control: true,
			show_widget_loading_progress: true,
			analyze_image: true,
			use_tracking_landmarks: true
		};

		var skinConcerns = ["Wrinkles", "Acne", "Spots", "Pores", "Redness", "Dehydration", "Uneven SkinTone", "Lip Roughness", "Dark Circles", "Oiliness"];
		
		//var deepTags = [ "GENDER", "AGEGROUP", "DiscreteAge",  "SKINTYPE",  "GEOLOCATION_CITY", "UVINDEX", "POLLUTION_AQI", "ORIGINAL_IMAGE"];

		var app = angular.module('skincare', []);

		app.controller('skincareController', function ($scope) {
			$scope.config = config;
			$scope.skinConcerns = skinConcerns;
			//$scope.deepTags = deepTags;
		});

		var stirngs = {
			instrctionTitle: "For best results",
			instruction1: "Remove Glasses & <br/> Makeup",
			instruction2: "Pull your hair back",
			instruction3: "Ensure even lighting",
			instruction4: "Center face within outline"
		};

		function startMirror() {

			document.getElementById("btn_start").style.display = "none";
			document.getElementById("config_container").style.display = "none";

			var skinConcernNodes = document.getElementsByName("skincare_concerns");
			var skinConcernList = [];
			for (var i = 0; i < skinConcernNodes.length; i++) {
				var node = skinConcernNodes[i];
				if (node.checked) {
					skinConcernList.push(skinConcerns[i]);
				}
			}

			var deepTagNodes = document.getElementsByName("deep_tags");
			var DeepTagList = [];
			for (var i = 0; i < deepTagNodes.length; i++) {
				var node = deepTagNodes[i];
				if (node.checked) {
					DeepTagList.push(deepTags[i]);
				}
			}
		
			var statTime;
			var btbpConfig = {
				content: "mirror_content",
				key: 'POFR-SWBM-IDIU-OCNH', //License key here
				mirror_config: {
					SkinFeatureList: skinConcernList,
					DeepTagList : [ "GENDER", "AGEGROUP", "DiscreteAge",  "SKINTYPE",  "GEOLOCATION_CITY", "UVINDEX", "POLLUTION_AQI", "ORIGINAL_IMAGE"],
					slow_network_time_interval: 1000 * 16,
					show_analyzed_image: config.show_analyzed_image,
					auto_capture: config.auto_capture,
					show_gallery_control: config.show_gallery_control,
					show_capture_control: config.show_capture_control,
					show_retake_control: config.show_retake_control,
					show_accept_control: config.show_accept_control,
					show_instructions: config.show_instructions,
					widget_strings: stirngs,
					show_restart: config.show_restart,
					show_widget_loading_progress: config.show_widget_loading_progress,
					analyze_image: config.analyze_image,
					use_tracking_landmarks: config.use_tracking_landmarks
				},
				onError: function (errorCode) {
					//console.log("onError : " + errorCode);
				},
				onInit: function () {
					//console.log("onWidgetLoad");
					statTime = new Date().getTime();
					document.getElementById("canvas_container").innerHTML = "";
					document.getElementById("onCaptureSuccess_1").innerHTML = "";
					document.getElementById("resutls_container").style.display = "none";
				},
				onWidgetLoaded: function () {
					//console.log("onWidgetLoaded");
					var endTime = new Date().getTime();
					console.log("TT :" + (endTime - statTime) / 1000);
					//alert("TT :"+ (endTime - statTime)/1000);
				},
				onAnalysisResult: function (jsonData) {
					//console.log(jsonData);
					document.getElementById("resutls_container").style.display = "block";
					document.getElementById("txt_results").innerHTML = JSON.stringify(JSON.parse(jsonData), undefined, 5);
                    //SendSelfiRes(JSON.stringify(JSON.parse(jsonData), undefined, 5));
                      SendSelfiRes(JSON.stringify(JSON.parse(jsonData), undefined, 5));
				},
				onAnalysisRequest: function (data) {
					//console.log("onImageCapture :" + data.length);
				},
				onReset: function () {
					//console.log("onReset");
				},
				onWarning: function (warningCode) {
					alert("slow network");
					//console.log("onWarning : " + warningCode);
				},
				onCaptureSuccess: function (data, faceRect) {
					//console.log("onCaptureSuccess : " + data.length);
					drawCanvas(data, faceRect);
				},
				onRetakeClicked: function () {
					//console.log("onRetakeClicked");
					document.getElementById("canvas_container").innerHTML = "";
					document.getElementById("onCaptureSuccess_1").innerHTML = "";

					document.getElementById("resutls_container").style.display = "none";
				},
				onAcceptClicked: function () {
					//console.log("onAcceptClicked");
				},
				onIQCError: function (errorCode) {
					//console.log(" onIQCError : "+ errorCode);
				},
				onCameraInit: function () {
					//console.log("onCameraInit");
				},
				onCameraError: function (errorCode) {
					
				},
				onLiveIQCError: function (errorCode) {
					//console.log(" onLiveIQCError : "+ errorCode);
				},
				onLiveIQCSuccess: function () {
					//console.log("onLiveIQCSuccess");
				},
				onLoadingProgress: function (progress) {
					//console.log("onLoadingProgress : "+progress);
				},
				onDestroy: function () {
					document.getElementById("btn_start").style.display = "inline-block";

					document.getElementById("canvas_container").innerHTML = "";
					document.getElementById("onCaptureSuccess_1").innerHTML = "";
					document.getElementById("resutls_container").style.display = "none";
				},
				onDimensionChange: function (width, height) {
				},
				onAnalysisRequestError: function (data, errorCode) {

				},
				onAnalysisError: function (errorCode) {

				}
			};

			mirror = new BTBPSkincare(btbpConfig);
			mirror.startMirror();
		}

		function setCallbackText(key, str) {
			try {
				document.getElementById(key).innerHTML = str;
			}
			catch (e) {
				console.log("key :" + key + " " + e);
			}
		}

		function drawCanvas(data, rect) {
			var image = new Image();
			image.onload = function () {
				var width = this.width;
				var height = this.height;

				var imgCanvas = document.createElement("canvas");
				var imgContext = imgCanvas.getContext("2d");
				//displaying resized image
				var dimen = getResizedDimensions(width, height);
				var rWidth = dimen.width;
				var rHeight = dimen.height;
				imgCanvas.width = rWidth;
				imgCanvas.height = rHeight;
				imgContext.drawImage(this, 0, 0, rWidth, rHeight);
				imgContext.strokeStyle = "red";
				imgContext.lineWidth = rWidth * 0.005;
				var xFactor = rWidth/width;
				var yFactor = rHeight/height;
				imgContext.strokeRect(rect[0]*xFactor, rect[1]*yFactor, rect[2]*xFactor, rect[3]*yFactor);
				document.getElementById("canvas_container").innerHTML = "";
				document.getElementById("canvas_container").appendChild(imgCanvas);
				document.getElementById("canvas_container").style.display = "block";

				var str = "face rect [x, y, width, height] " + rect;
				str += "<br/>Resolution " + width + " x " + height + "  (W x H)";
				document.getElementById("onCaptureSuccess_1").innerHTML = str;
				imgCanvas = null;
			};
			image.src = data;//"data:image/jpeg;base64," +
		}

		function getResizedDimensions(width, height) {
			var max_size = 320;
			var scale = max_size / Math.min(width, height);
			var w = (width * scale) | 0;
			var h = (height * scale) | 0;
			return { width: w, height: h };
		}


		function changeBtnState(id, enabled) {
			var ele = document.getElementById(id);
			if (enabled) {
				ele.style.opacity = 1;
				ele.disabled = false;
			}
			else {
				ele.style.opacity = 0.3;
				ele.disabled = true;
			}
		}

function SendSelfiRes(data){
  	SendSelfi(data); // Theme-js Class
}
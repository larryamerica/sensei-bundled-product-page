window.theme = window.theme || {};

/* ================ SLATE ================ */
window.theme = window.theme || {};

theme.Sections = function Sections() {
  this.constructors = {};
  this.instances = [];

  $(document)
    .on('shopify:section:load', this._onSectionLoad.bind(this))
    .on('shopify:section:unload', this._onSectionUnload.bind(this))
    .on('shopify:section:select', this._onSelect.bind(this))
    .on('shopify:section:deselect', this._onDeselect.bind(this))
    .on('shopify:block:select', this._onBlockSelect.bind(this))
    .on('shopify:block:deselect', this._onBlockDeselect.bind(this));
};
theme.Sections.prototype = _.assignIn({}, theme.Sections.prototype, {
  _createInstance: function(container, constructor) {
    var $container = $(container);
    var id = $container.attr('data-section-id');
    var type = $container.attr('data-section-type');

    constructor = constructor || this.constructors[type];

    if (_.isUndefined(constructor)) {
      return;
    }

    var instance = _.assignIn(new constructor(container), {
      id: id,
      type: type,
      container: container
    });

    this.instances.push(instance);
  },

  _onSectionLoad: function(evt) {
    var container = $('[data-section-id]', evt.target)[0];
    if (container) {
      this._createInstance(container);
    }
  },

  _onSectionUnload: function(evt) {
    this.instances = _.filter(this.instances, function(instance) {
      var isEventInstance = instance.id === evt.detail.sectionId;

      if (isEventInstance) {
        if (_.isFunction(instance.onUnload)) {
          instance.onUnload(evt);
        }
      }

      return !isEventInstance;
    });
  },

  _onSelect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = _.find(this.instances, function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (!_.isUndefined(instance) && _.isFunction(instance.onSelect)) {
      instance.onSelect(evt);
    }
  },

  _onDeselect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = _.find(this.instances, function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (!_.isUndefined(instance) && _.isFunction(instance.onDeselect)) {
      instance.onDeselect(evt);
    }
  },

  _onBlockSelect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = _.find(this.instances, function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (!_.isUndefined(instance) && _.isFunction(instance.onBlockSelect)) {
      instance.onBlockSelect(evt);
    }
  },

  _onBlockDeselect: function(evt) {
    // eslint-disable-next-line no-shadow
    var instance = _.find(this.instances, function(instance) {
      return instance.id === evt.detail.sectionId;
    });

    if (!_.isUndefined(instance) && _.isFunction(instance.onBlockDeselect)) {
      instance.onBlockDeselect(evt);
    }
  },

  register: function(type, constructor) {
    this.constructors[type] = constructor;

    $('[data-section-type=' + type + ']').each(
      function(index, container) {
        this._createInstance(container, constructor);
      }.bind(this)
    );
  }
});

window.slate = window.slate || {};

/**
 * iFrames
 * -----------------------------------------------------------------------------
 * Wrap videos in div to force responsive layout.
 *
 * @namespace iframes
 */

slate.rte = {
  /**
   * Wrap tables in a container div to make them scrollable when needed
   *
   * @param {object} options - Options to be used
   * @param {jquery} options.$tables - jquery object(s) of the table(s) to wrap
   * @param {string} options.tableWrapperClass - table wrapper class name
   */
  wrapTable: function(options) {
    options.$tables.wrap(
      '<div class="' + options.tableWrapperClass + '"></div>'
    );
  },

  /**
   * Wrap iframes in a container div to make them responsive
   *
   * @param {object} options - Options to be used
   * @param {jquery} options.$iframes - jquery object(s) of the iframe(s) to wrap
   * @param {string} options.iframeWrapperClass - class name used on the wrapping div
   */
  wrapIframe: function(options) {
    options.$iframes.each(function() {
      // Add wrapper to make video responsive
      $(this).wrap('<div class="' + options.iframeWrapperClass + '"></div>');

      // Re-set the src attribute on each iframe after page load
      // for Chrome's "incorrect iFrame content on 'back'" bug.
      // https://code.google.com/p/chromium/issues/detail?id=395791
      // Need to specifically target video and admin bar
      this.src = this.src;
    });
  }
};

window.slate = window.slate || {};

/**
 * A11y Helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions that help make your theme more accessible
 * to users with visual impairments.
 *
 *
 * @namespace a11y
 */

slate.a11y = {
  /**
   * For use when focus shifts to a container rather than a link
   * eg for In-page links, after scroll, focus shifts to content area so that
   * next `tab` is where user expects if focusing a link, just $link.focus();
   *
   * @param {JQuery} $element - The element to be acted upon
   */
  pageLinkFocus: function($element) {
    var focusClass = 'js-focus-hidden';

    $element
      .first()
      .attr('tabIndex', '-1')
      .focus()
      .addClass(focusClass)
      .one('blur', callback);

    function callback() {
      $element
        .first()
        .removeClass(focusClass)
        .removeAttr('tabindex');
    }
  },

  /**
   * If there's a hash in the url, focus the appropriate element
   */
  focusHash: function() {
    var hash = window.location.hash;

    // is there a hash in the url? is it an element on the page?
    if (hash && document.getElementById(hash.slice(1))) {
      this.pageLinkFocus($(hash));
    }
  },

  /**
   * When an in-page (url w/hash) link is clicked, focus the appropriate element
   */
  bindInPageLinks: function() {
    $('a[href*=#]').on(
      'click',
      function(evt) {
        this.pageLinkFocus($(evt.currentTarget.hash));
      }.bind(this)
    );
  },

  /**
   * Traps the focus in a particular container
   *
   * @param {object} options - Options to be used
   * @param {jQuery} options.$container - Container to trap focus within
   * @param {jQuery} options.$elementToFocus - Element to be focused when focus leaves container
   * @param {string} options.namespace - Namespace used for new focus event handler
   */
  trapFocus: function(options) {
    var eventName = options.namespace
      ? 'focusin.' + options.namespace
      : 'focusin';

    if (!options.$elementToFocus) {
      options.$elementToFocus = options.$container;
    }

    options.$container.attr('tabindex', '-1');
    options.$elementToFocus.focus();

    $(document).off('focusin');

    $(document).on(eventName, function(evt) {
      if (
        options.$container[0] !== evt.target &&
        !options.$container.has(evt.target).length
      ) {
        options.$container.focus();
      }
    });
  },

  /**
   * Removes the trap of focus in a particular container
   *
   * @param {object} options - Options to be used
   * @param {jQuery} options.$container - Container to trap focus within
   * @param {string} options.namespace - Namespace used for new focus event handler
   */
  removeTrapFocus: function(options) {
    var eventName = options.namespace
      ? 'focusin.' + options.namespace
      : 'focusin';

    if (options.$container && options.$container.length) {
      options.$container.removeAttr('tabindex');
    }

    $(document).off(eventName);
  }
};

/**
 * Image Helper Functions
 * -----------------------------------------------------------------------------
 * A collection of functions that help with basic image operations.
 *
 */

theme.Images = (function() {
  /**
   * Preloads an image in memory and uses the browsers cache to store it until needed.
   *
   * @param {Array} images - A list of image urls
   * @param {String} size - A shopify image size attribute
   */

  function preload(images, size) {
    if (typeof images === 'string') {
      images = [images];
    }

    for (var i = 0; i < images.length; i++) {
      var image = images[i];
      this.loadImage(this.getSizedImageUrl(image, size));
    }
  }

  /**
   * Loads and caches an image in the browsers cache.
   * @param {string} path - An image url
   */
  function loadImage(path) {
    new Image().src = path;
  }

  /**
   * Swaps the src of an image for another OR returns the imageURL to the callback function
   * @param image
   * @param element
   * @param callback
   */
  function switchImage(image, element, callback) {
    var size = this.imageSize(element.src);
    var imageUrl = this.getSizedImageUrl(image.src, size);

    if (callback) {
      callback(imageUrl, image, element); // eslint-disable-line callback-return
    } else {
      element.src = imageUrl;
    }
  }

  /**
   * +++ Useful
   * Find the Shopify image attribute size
   *
   * @param {string} src
   * @returns {null}
   */
  function imageSize(src) {
    var match = src.match(
      /.+_((?:pico|icon|thumb|small|compact|medium|large|grande)|\d{1,4}x\d{0,4}|x\d{1,4})[_\\.@]/
    );

    if (match !== null) {
      if (match[2] !== undefined) {
        return match[1] + match[2];
      } else {
        return match[1];
      }
    } else {
      return null;
    }
  }

  /**
   * +++ Useful
   * Adds a Shopify size attribute to a URL
   *
   * @param src
   * @param size
   * @returns {*}
   */
  function getSizedImageUrl(src, size) {
    if (size === null) {
      return src;
    }

    if (size === 'master') {
      return this.removeProtocol(src);
    }

    var match = src.match(
      /\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i
    );

    if (match !== null) {
      var prefix = src.split(match[0]);
      var suffix = match[0];

      return this.removeProtocol(prefix[0] + '_' + size + suffix);
    }

    return null;
  }

  function removeProtocol(path) {
    return path.replace(/http(s)?:/, '');
  }

  return {
    preload: preload,
    loadImage: loadImage,
    switchImage: switchImage,
    imageSize: imageSize,
    getSizedImageUrl: getSizedImageUrl,
    removeProtocol: removeProtocol
  };
})();

/**
 * Currency Helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions that help with currency formatting
 *
 * Current contents
 * - formatMoney - Takes an amount in cents and returns it as a formatted dollar value.
 *
 * Alternatives
 * - Accounting.js - http://openexchangerates.github.io/accounting.js/
 *
 */

theme.Currency = (function() {
  var moneyFormat = '${{amount}}'; // eslint-disable-line camelcase

  function formatMoney(cents, format) {
    if (typeof cents === 'string') {
      cents = cents.replace('.', '');
    }
    var value = '';
    var placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;
    var formatString = format || moneyFormat;

    function formatWithDelimiters(number, precision, thousands, decimal) {
      thousands = thousands || ',';
      decimal = decimal || '.';

      if (isNaN(number) || number === null) {
        return 0;
      }

      number = (number / 100.0).toFixed(precision);

      var parts = number.split('.');
      var dollarsAmount = parts[0].replace(
        /(\d)(?=(\d\d\d)+(?!\d))/g,
        '$1' + thousands
      );
      var centsAmount = parts[1] ? decimal + parts[1] : '';

      return dollarsAmount + centsAmount;
    }

    switch (formatString.match(placeholderRegex)[1]) {
      case 'amount':
        value = formatWithDelimiters(cents, 2);
        break;
      case 'amount_no_decimals':
        value = formatWithDelimiters(cents, 0);
        break;
      case 'amount_with_comma_separator':
        value = formatWithDelimiters(cents, 2, '.', ',');
        break;
      case 'amount_no_decimals_with_comma_separator':
        value = formatWithDelimiters(cents, 0, '.', ',');
        break;
      case 'amount_no_decimals_with_space_separator':
        value = formatWithDelimiters(cents, 0, ' ');
        break;
      case 'amount_with_apostrophe_separator':
        value = formatWithDelimiters(cents, 2, "'");
        break;
    }

    return formatString.replace(placeholderRegex, value);
  }

  return {
    formatMoney: formatMoney
  };
})();

/**
 * Variant Selection scripts
 * ------------------------------------------------------------------------------
 *
 * Handles change events from the variant inputs in any `cart/add` forms that may
 * exist.  Also updates the master select and triggers updates when the variants
 * price or image changes.
 *
 * @namespace variants
 */

slate.Variants = (function() {
  /**
   * Variant constructor
   *
   * @param {object} options - Settings from `product.js`
   */
  function Variants(options) {
    this.$container = options.$container;
    this.product = options.product;
    this.singleOptionSelector = options.singleOptionSelector;
    this.originalSelectorId = options.originalSelectorId;
    this.enableHistoryState = options.enableHistoryState;
    this.currentVariant = this._getVariantFromOptions();

    $(this.singleOptionSelector, this.$container).on(
      'change',
      this._onSelectChange.bind(this)
    );
  }

  Variants.prototype = _.assignIn({}, Variants.prototype, {
    /**
     * Get the currently selected options from add-to-cart form. Works with all
     * form input elements.
     *
     * @return {array} options - Values of currently selected variants
     */
    _getCurrentOptions: function() {
      var currentOptions = _.map(
        $(this.singleOptionSelector, this.$container),
        function(element) {
          var $element = $(element);
          var type = $element.attr('type');
          var currentOption = {};

          if (type === 'radio' || type === 'checkbox') {
            if ($element[0].checked) {
              currentOption.value = $element.val();
              currentOption.index = $element.data('index');

              return currentOption;
            } else {
              return false;
            }
          } else {
            currentOption.value = $element.val();
            currentOption.index = $element.data('index');

            return currentOption;
          }
        }
      );

      // remove any unchecked input values if using radio buttons or checkboxes
      currentOptions = _.compact(currentOptions);

      return currentOptions;
    },

    /**
     * Find variant based on selected values.
     *
     * @param  {array} selectedValues - Values of variant inputs
     * @return {object || undefined} found - Variant object from product.variants
     */
    _getVariantFromOptions: function() {
      var selectedValues = this._getCurrentOptions();
      var variants = this.product.variants;

      var found = _.find(variants, function(variant) {
        return selectedValues.every(function(values) {
          return _.isEqual(variant[values.index], values.value);
        });
      });

      return found;
    },

    /**
     * Event handler for when a variant input changes.
     */
    _onSelectChange: function() {
      var variant = this._getVariantFromOptions();

      this.$container.trigger({
        type: 'variantChange',
        variant: variant
      });

      if (!variant) {
        return;
      }

      this._updateMasterSelect(variant);
      this._updateImages(variant);
      this._updatePrice(variant);
      this._updateSKU(variant);
      this.currentVariant = variant;

      if (this.enableHistoryState) {
        this._updateHistoryState(variant);
      }
    },

    /**
     * Trigger event when variant image changes
     *
     * @param  {object} variant - Currently selected variant
     * @return {event}  variantImageChange
     */
    _updateImages: function(variant) {
      var variantImage = variant.featured_image || {};
      var currentVariantImage = this.currentVariant.featured_image || {};

      if (
        !variant.featured_image ||
        variantImage.src === currentVariantImage.src
      ) {
        return;
      }

      this.$container.trigger({
        type: 'variantImageChange',
        variant: variant
      });
    },

    /**
     * Trigger event when variant price changes.
     *
     * @param  {object} variant - Currently selected variant
     * @return {event} variantPriceChange
     */
    _updatePrice: function(variant) {
      if (
        variant.price === this.currentVariant.price &&
        variant.compare_at_price === this.currentVariant.compare_at_price
      ) {
        return;
      }

      this.$container.trigger({
        type: 'variantPriceChange',
        variant: variant
      });
    },

    /**
     * Trigger event when variant sku changes.
     *
     * @param  {object} variant - Currently selected variant
     * @return {event} variantSKUChange
     */
    _updateSKU: function(variant) {
      if (variant.sku === this.currentVariant.sku) {
        return;
      }

      this.$container.trigger({
        type: 'variantSKUChange',
        variant: variant
      });
    },

    /**
     * Update history state for product deeplinking
     *
     * @param  {variant} variant - Currently selected variant
     * @return {k}         [description]
     */
    _updateHistoryState: function(variant) {
      if (!history.replaceState || !variant) {
        return;
      }

      var newurl =
        window.location.protocol +
        '//' +
        window.location.host +
        window.location.pathname +
        '?variant=' +
        variant.id;
      window.history.replaceState({ path: newurl }, '', newurl);
    },

    /**
     * Update hidden master select of variant change
     *
     * @param  {variant} variant - Currently selected variant
     */
    _updateMasterSelect: function(variant) {
      $(this.originalSelectorId, this.$container).val(variant.id);
    }
  });

  return Variants;
})();


/* ================ GLOBAL ================ */
/*============================================================================
  Drawer modules
==============================================================================*/
theme.Drawers = (function() {
  function Drawer(id, position, options) {
    var defaults = {
      close: '.js-drawer-close',
      open: '.js-drawer-open-' + position,
      openClass: 'js-drawer-open',
      dirOpenClass: 'js-drawer-open-' + position
    };

    this.nodes = {
      $parent: $('html').add('body'),
      $page: $('#PageContainer')
    };

    this.config = $.extend(defaults, options);
    this.position = position;

    this.$drawer = $('#' + id);

    if (!this.$drawer.length) {
      return false;
    }

    this.drawerIsOpen = false;
    this.init();
  }

  Drawer.prototype.init = function() {
    $(this.config.open).on('click', $.proxy(this.open, this));
    this.$drawer.on('click', this.config.close, $.proxy(this.close, this));
  };

  Drawer.prototype.open = function(evt) {
    // Keep track if drawer was opened from a click, or called by another function
    var externalCall = false;

    // Prevent following href if link is clicked
    if (evt) {
      evt.preventDefault();
    } else {
      externalCall = true;
    }

    // Without this, the drawer opens, the click event bubbles up to nodes.$page
    // which closes the drawer.
    if (evt && evt.stopPropagation) {
      evt.stopPropagation();
      // save the source of the click, we'll focus to this on close
      this.$activeSource = $(evt.currentTarget);
    }

    if (this.drawerIsOpen && !externalCall) {
      return this.close();
    }

    // Add is-transitioning class to moved elements on open so drawer can have
    // transition for close animation
    this.$drawer.prepareTransition();

    this.nodes.$parent.addClass(
      this.config.openClass + ' ' + this.config.dirOpenClass
    );
    this.drawerIsOpen = true;

    // Set focus on drawer
    slate.a11y.trapFocus({
      $container: this.$drawer,
      namespace: 'drawer_focus'
    });

    // Run function when draw opens if set
    if (
      this.config.onDrawerOpen &&
      typeof this.config.onDrawerOpen === 'function'
    ) {
      if (!externalCall) {
        this.config.onDrawerOpen();
      }
    }

    if (this.$activeSource && this.$activeSource.attr('aria-expanded')) {
      this.$activeSource.attr('aria-expanded', 'true');
    }

    this.bindEvents();

    return this;
  };

  Drawer.prototype.close = function() {
    if (!this.drawerIsOpen) {
      // don't close a closed drawer
      return;
    }

    // deselect any focused form elements
    $(document.activeElement).trigger('blur');

    // Ensure closing transition is applied to moved elements, like the nav
    this.$drawer.prepareTransition();

    this.nodes.$parent.removeClass(
      this.config.dirOpenClass + ' ' + this.config.openClass
    );

    if (this.$activeSource && this.$activeSource.attr('aria-expanded')) {
      this.$activeSource.attr('aria-expanded', 'false');
    }

    this.drawerIsOpen = false;

    // Remove focus on drawer
    slate.a11y.removeTrapFocus({
      $container: this.$drawer,
      namespace: 'drawer_focus'
    });

    this.unbindEvents();

    // Run function when draw closes if set
    if (
      this.config.onDrawerClose &&
      typeof this.config.onDrawerClose === 'function'
    ) {
      this.config.onDrawerClose();
    }
  };

  Drawer.prototype.bindEvents = function() {
    this.nodes.$parent.on(
      'keyup.drawer',
      $.proxy(function(evt) {
        // close on 'esc' keypress
        if (evt.keyCode === 27) {
          this.close();
          return false;
        } else {
          return true;
        }
      }, this)
    );

    // Lock scrolling on mobile
    this.nodes.$page.on('touchmove.drawer', function() {
      return false;
    });

    this.nodes.$page.on(
      'click.drawer',
      $.proxy(function() {
        this.close();
        return false;
      }, this)
    );
  };

  Drawer.prototype.unbindEvents = function() {
    this.nodes.$page.off('.drawer');
    this.nodes.$parent.off('.drawer');
  };

  return Drawer;
})();


/* ================ MODULES ================ */
window.theme = window.theme || {};

theme.Header = (function() {
  var selectors = {
    body: 'body',
    navigation: '#AccessibleNav',
    siteNavHasDropdown: '.site-nav--has-dropdown',
    siteNavChildLinks: '.site-nav__child-link',
    siteNavActiveDropdown: '.site-nav--active-dropdown',
    siteNavLinkMain: '.site-nav__link--main',
    siteNavChildLink: '.site-nav__link--last'
  };

  var config = {
    activeClass: 'site-nav--active-dropdown',
    childLinkClass: 'site-nav__child-link'
  };

  var cache = {};

  function init() {
    cacheSelectors();

    cache.$parents.on('click.siteNav', function(evt) {
      var $el = $(this);

      if (!$el.hasClass(config.activeClass)) {
        // force stop the click from happening
        evt.preventDefault();
        evt.stopImmediatePropagation();
        showDropdown($el);
      }
    });

    // check when we're leaving a dropdown and close the active dropdown
    $(selectors.siteNavChildLink).on('focusout.siteNav', function() {
      setTimeout(function() {
        if (
          $(document.activeElement).hasClass(config.childLinkClass) ||
          !cache.$activeDropdown.length
        ) {
          return;
        }

        hideDropdown(cache.$activeDropdown);
      });
    });

    // close dropdowns when on top level nav
    cache.$topLevel.on('focus.siteNav', function() {
      if (cache.$activeDropdown.length) {
        hideDropdown(cache.$activeDropdown);
      }
    });

    cache.$subMenuLinks.on('click.siteNav', function(evt) {
      // Prevent click on body from firing instead of link
      evt.stopImmediatePropagation();
    });
  }

  function cacheSelectors() {
    cache = {
      $nav: $(selectors.navigation),
      $topLevel: $(selectors.siteNavLinkMain),
      $parents: $(selectors.navigation).find(selectors.siteNavHasDropdown),
      $subMenuLinks: $(selectors.siteNavChildLinks),
      $activeDropdown: $(selectors.siteNavActiveDropdown)
    };
  }

  function showDropdown($el) {
    $el.addClass(config.activeClass);

    // close open dropdowns
    if (cache.$activeDropdown.length) {
      hideDropdown(cache.$activeDropdown);
    }

    cache.$activeDropdown = $el;

    // set expanded on open dropdown
    $el.find(selectors.siteNavLinkMain).attr('aria-expanded', 'true');

    setTimeout(function() {
      $(window).on('keyup.siteNav', function(evt) {
        if (evt.keyCode === 27) {
          hideDropdown($el);
        }
      });

      $(selectors.body).on('click.siteNav', function() {
        hideDropdown($el);
      });
    }, 250);
  }

  function hideDropdown($el) {
    // remove aria on open dropdown
    $el.find(selectors.siteNavLinkMain).attr('aria-expanded', 'false');
    $el.removeClass(config.activeClass);

    // reset active dropdown
    cache.$activeDropdown = $(selectors.siteNavActiveDropdown);

    $(selectors.body).off('click.siteNav');
    $(window).off('keyup.siteNav');
  }

  function unload() {
    $(window).off('.siteNav');
    cache.$parents.off('.siteNav');
    cache.$subMenuLinks.off('.siteNav');
    cache.$topLevel.off('.siteNav');
    $(selectors.siteNavChildLink).off('.siteNav');
    $(selectors.body).off('.siteNav');
  }

  return {
    init: init,
    unload: unload
  };
})();

window.theme = window.theme || {};

theme.MobileNav = (function() {
  var classes = {
    mobileNavOpenIcon: 'mobile-nav--open',
    mobileNavCloseIcon: 'mobile-nav--close',
    navLinkWrapper: 'mobile-nav__item',
    navLink: 'mobile-nav__link',
    subNavLink: 'mobile-nav__sublist-link',
    return: 'mobile-nav__return-btn',
    subNavActive: 'is-active',
    subNavClosing: 'is-closing',
    navOpen: 'js-menu--is-open',
    subNavShowing: 'sub-nav--is-open',
    thirdNavShowing: 'third-nav--is-open',
    subNavToggleBtn: 'js-toggle-submenu'
  };
  var cache = {};
  var isTransitioning;
  var $activeSubNav;
  var $activeTrigger;
  var menuLevel = 1;
  // Breakpoints from src/stylesheets/global/variables.scss.liquid
  var mediaQuerySmall = 'screen and (max-width: 749px)';

  function init() {
    cacheSelectors();

    cache.$mobileNavToggle.on('mousedown', toggleMobileNav);
    cache.$subNavToggleBtn.on('click.subNav', toggleSubNav);

    // Close mobile nav when unmatching mobile breakpoint
    enquire.register(mediaQuerySmall, {
      unmatch: function() {
        closeMobileNav();
      }
    });
  }

  function toggleMobileNav() {
    if (cache.$mobileNavToggle.hasClass(classes.mobileNavCloseIcon)) {
      closeMobileNav();
    } else {
      openMobileNav();
    }
  }

  function cacheSelectors() {
    cache = {
      $pageContainer: $('#PageContainer'),
      $siteHeader: $('.site-header'),
      $mobileNavToggle: $('.js-mobile-nav-toggle'),
      $mobileNavContainer: $('.mobile-nav-wrapper'),
      $mobileNav: $('#MobileNav'),
      $sectionHeader: $('#shopify-section-header'),
      $subNavToggleBtn: $('.' + classes.subNavToggleBtn)
    };
  }

  function openMobileNav() {
    var translateHeaderHeight =
      cache.$siteHeader.outerHeight() + cache.$siteHeader.position().top;

    cache.$mobileNavContainer.prepareTransition().addClass(classes.navOpen);

    cache.$mobileNavContainer.css({
      transform: 'translateY(' + translateHeaderHeight + 'px)'
    });

    cache.$pageContainer.css({
      transform:
        'translate3d(0, ' + cache.$mobileNavContainer[0].scrollHeight + 'px, 0)'
    });

    slate.a11y.trapFocus({
      $container: cache.$sectionHeader,
      $elementToFocus: cache.$mobileNav
        .find('.' + classes.navLinkWrapper + ':first')
        .find('.' + classes.navLink),
      namespace: 'navFocus'
    });

    cache.$mobileNavToggle
      .addClass(classes.mobileNavCloseIcon)
      .removeClass(classes.mobileNavOpenIcon);

    // close on escape
    $(window).on('keyup.mobileNav', function(evt) {
      if (evt.which === 27) {
        closeMobileNav();
      }
    });
  }

  function closeMobileNav() {
    cache.$mobileNavContainer.prepareTransition().removeClass(classes.navOpen);

    cache.$mobileNavContainer.css({
      transform: 'translateY(-100%)'
    });

    cache.$pageContainer.removeAttr('style');

    slate.a11y.trapFocus({
      $container: $('html'),
      $elementToFocus: $('body')
    });

    cache.$mobileNavContainer.one(
      'TransitionEnd.navToggle webkitTransitionEnd.navToggle transitionend.navToggle oTransitionEnd.navToggle',
      function() {
        slate.a11y.removeTrapFocus({
          $container: cache.$mobileNav,
          namespace: 'navFocus'
        });
        document.activeElement.blur();
      }
    );

    cache.$mobileNavToggle
      .addClass(classes.mobileNavOpenIcon)
      .removeClass(classes.mobileNavCloseIcon);

    $(window).off('keyup.mobileNav');

    scrollTo(0, 0);
  }

  function toggleSubNav(evt) {
    if (isTransitioning) {
      return;
    }

    var $toggleBtn = $(evt.currentTarget);
    var isReturn = $toggleBtn.hasClass(classes.return);
    isTransitioning = true;

    if (isReturn) {
      // Close all subnavs by removing active class on buttons
      $(
        '.' + classes.subNavToggleBtn + '[data-level="' + (menuLevel - 1) + '"]'
      ).removeClass(classes.subNavActive);

      if ($activeTrigger && $activeTrigger.length) {
        $activeTrigger.removeClass(classes.subNavActive);
      }
    } else {
      $toggleBtn.addClass(classes.subNavActive);
    }

    $activeTrigger = $toggleBtn;

    goToSubnav($toggleBtn.data('target'));
  }

  function goToSubnav(target) {
    /*eslint-disable shopify/jquery-dollar-sign-reference */

    var $targetMenu = target
      ? $('.mobile-nav__dropdown[data-parent="' + target + '"]')
      : cache.$mobileNav;

    menuLevel = $targetMenu.data('level') ? $targetMenu.data('level') : 1;

    if ($activeSubNav && $activeSubNav.length) {
      $activeSubNav.prepareTransition().addClass(classes.subNavClosing);
    }

    $activeSubNav = $targetMenu;

    var $elementToFocus = target
      ? $targetMenu.find('.' + classes.subNavLink + ':first')
      : $activeTrigger;

    /*eslint-enable shopify/jquery-dollar-sign-reference */

    var translateMenuHeight = $targetMenu.outerHeight();

    var openNavClass =
      menuLevel > 2 ? classes.thirdNavShowing : classes.subNavShowing;

    cache.$mobileNavContainer
      .css('height', translateMenuHeight)
      .removeClass(classes.thirdNavShowing)
      .addClass(openNavClass);

    if (!target) {
      // Show top level nav
      cache.$mobileNavContainer
        .removeClass(classes.thirdNavShowing)
        .removeClass(classes.subNavShowing);
    }

    // Focusing an item in the subnav early forces element into view and breaks the animation.
    cache.$mobileNavContainer.one(
      'TransitionEnd.subnavToggle webkitTransitionEnd.subnavToggle transitionend.subnavToggle oTransitionEnd.subnavToggle',
      function() {
        slate.a11y.trapFocus({
          $container: $targetMenu,
          $elementToFocus: $elementToFocus,
          namespace: 'subNavFocus'
        });

        cache.$mobileNavContainer.off('.subnavToggle');
        isTransitioning = false;
      }
    );

    // Match height of subnav
    cache.$pageContainer.css({
      transform: 'translateY(' + translateMenuHeight + 'px)'
    });

    $activeSubNav.removeClass(classes.subNavClosing);
  }

  return {
    init: init,
    closeMobileNav: closeMobileNav
  };
})(jQuery);

window.theme = window.theme || {};

theme.Search = (function() {
  var selectors = {
    search: '.search',
    searchSubmit: '.search__submit',
    searchInput: '.search__input',

    siteHeader: '.site-header',
    siteHeaderSearchToggle: '.site-header__search-toggle',
    siteHeaderSearch: '.site-header__search',

    searchDrawer: '.search-bar',
    searchDrawerInput: '.search-bar__input',

    searchHeader: '.search-header',
    searchHeaderInput: '.search-header__input',
    searchHeaderSubmit: '.search-header__submit',

    mobileNavWrapper: '.mobile-nav-wrapper'
  };

  var classes = {
    focus: 'search--focus',
    mobileNavIsOpen: 'js-menu--is-open'
  };

  function init() {
    if (!$(selectors.siteHeader).length) {
      return;
    }

    initDrawer();
    searchSubmit();

    $(selectors.searchHeaderInput)
      .add(selectors.searchHeaderSubmit)
      .on('focus blur', function() {
        $(selectors.searchHeader).toggleClass(classes.focus);
      });

    $(selectors.siteHeaderSearchToggle).on('click', function() {
      var searchHeight = $(selectors.siteHeader).outerHeight();
      var searchOffset = $(selectors.siteHeader).offset().top - searchHeight;

      $(selectors.searchDrawer).css({
        height: searchHeight + 'px',
        top: searchOffset + 'px'
      });
    });
  }

  function initDrawer() {
    // Add required classes to HTML
    $('#PageContainer').addClass('drawer-page-content');
    $('.js-drawer-open-top')
      .attr('aria-controls', 'SearchDrawer')
      .attr('aria-expanded', 'false')
      .attr('aria-haspopup', 'dialog');

    theme.SearchDrawer = new theme.Drawers('SearchDrawer', 'top', {
      onDrawerOpen: searchDrawerFocus,
      onDrawerClose: searchDrawerFocusClose
    });
  }

  function searchDrawerFocus() {
    searchFocus($(selectors.searchDrawerInput));

    if ($(selectors.mobileNavWrapper).hasClass(classes.mobileNavIsOpen)) {
      theme.MobileNav.closeMobileNav();
    }
  }

  function searchFocus($el) {
    $el.focus();
    // set selection range hack for iOS
    $el[0].setSelectionRange(0, $el[0].value.length);
  }

  function searchDrawerFocusClose() {
    $(selectors.siteHeaderSearchToggle).focus();
  }

  function searchSubmit() {
    $(selectors.searchSubmit).on('click', function(evt) {
      var $el = $(evt.target);
      var $input = $el.parents(selectors.search).find(selectors.searchInput);
      if ($input.val().length === 0) {
        evt.preventDefault();
        searchFocus($input);
      }
    });
  }

  return {
    init: init
  };
})();

(function() {
  var selectors = {
    backButton: '.return-link'
  };

  var $backButton = $(selectors.backButton);

  if (!document.referrer || !$backButton.length || !window.history.length) {
    return;
  }

  $backButton.one('click', function(evt) {
    evt.preventDefault();

    var referrerDomain = urlDomain(document.referrer);
    var shopDomain = urlDomain(window.location.href);

    if (shopDomain === referrerDomain) {
      history.back();
    }

    return false;
  });

  function urlDomain(url) {
    var anchor = document.createElement('a');
    anchor.ref = url;

    return anchor.hostname;
  }
})();

theme.Slideshow = (function() {
  this.$slideshow = null;
  var classes = {
    wrapper: 'slideshow-wrapper',
    slideshow: 'slideshow',
    currentSlide: 'slick-current',
    video: 'slideshow__video',
    videoBackground: 'slideshow__video--background',
    closeVideoBtn: 'slideshow__video-control--close',
    pauseButton: 'slideshow__pause',
    isPaused: 'is-paused'
  };

  function slideshow(el) {
    this.$slideshow = $(el);
    this.$wrapper = this.$slideshow.closest('.' + classes.wrapper);
    this.$pause = this.$wrapper.find('.' + classes.pauseButton);

    this.settings = {
      accessibility: true,
      arrows: false,
      dots: true,
      fade: true,
      draggable: true,
      touchThreshold: 20,
      autoplay: this.$slideshow.data('autoplay'),
      autoplaySpeed: this.$slideshow.data('speed')
    };

    this.$slideshow.on('beforeChange', beforeChange.bind(this));
    this.$slideshow.on('init', slideshowA11y.bind(this));
    this.$slideshow.slick(this.settings);

    this.$pause.on('click', this.togglePause.bind(this));
  }

  function slideshowA11y(event, obj) {
    var $slider = obj.$slider;
    var $list = obj.$list;
    var $wrapper = this.$wrapper;
    var autoplay = this.settings.autoplay;

    // Remove default Slick aria-live attr until slider is focused
    $list.removeAttr('aria-live');

    // When an element in the slider is focused
    // pause slideshow and set aria-live.
    $wrapper.on('focusin', function(evt) {
      if (!$wrapper.has(evt.target).length) {
        return;
      }

      $list.attr('aria-live', 'polite');

      if (autoplay) {
        $slider.slick('slickPause');
      }
    });

    // Resume autoplay
    $wrapper.on('focusout', function(evt) {
      if (!$wrapper.has(evt.target).length) {
        return;
      }

      $list.removeAttr('aria-live');

      if (autoplay) {
        // Manual check if the focused element was the video close button
        // to ensure autoplay does not resume when focus goes inside YouTube iframe
        if ($(evt.target).hasClass(classes.closeVideoBtn)) {
          return;
        }

        $slider.slick('slickPlay');
      }
    });

    // Add arrow key support when focused
    if (obj.$dots) {
      obj.$dots.on('keydown', function(evt) {
        if (evt.which === 37) {
          $slider.slick('slickPrev');
        }

        if (evt.which === 39) {
          $slider.slick('slickNext');
        }

        // Update focus on newly selected tab
        if (evt.which === 37 || evt.which === 39) {
          obj.$dots.find('.slick-active button').focus();
        }
      });
    }
  }

  function beforeChange(event, slick, currentSlide, nextSlide) {
    var $slider = slick.$slider;
    var $currentSlide = $slider.find('.' + classes.currentSlide);
    var $nextSlide = $slider.find(
      '.slideshow__slide[data-slick-index="' + nextSlide + '"]'
    );

    if (isVideoInSlide($currentSlide)) {
      var $currentVideo = $currentSlide.find('.' + classes.video);
      var currentVideoId = $currentVideo.attr('id');
      theme.SlideshowVideo.pauseVideo(currentVideoId);
      $currentVideo.attr('tabindex', '-1');
    }

    if (isVideoInSlide($nextSlide)) {
      var $video = $nextSlide.find('.' + classes.video);
      var videoId = $video.attr('id');
      var isBackground = $video.hasClass(classes.videoBackground);
      if (isBackground) {
        theme.SlideshowVideo.playVideo(videoId);
      } else {
        $video.attr('tabindex', '0');
      }
    }
  }

  function isVideoInSlide($slide) {
    return $slide.find('.' + classes.video).length;
  }

  slideshow.prototype.togglePause = function() {
    var slideshowSelector = getSlideshowId(this.$pause);
    if (this.$pause.hasClass(classes.isPaused)) {
      this.$pause.removeClass(classes.isPaused);
      $(slideshowSelector).slick('slickPlay');
    } else {
      this.$pause.addClass(classes.isPaused);
      $(slideshowSelector).slick('slickPause');
    }
  };

  function getSlideshowId($el) {
    return '#Slideshow-' + $el.data('id');
  }

  return slideshow;
})();

// Youtube API callback
// eslint-disable-next-line no-unused-vars
function onYouTubeIframeAPIReady() {
  theme.SlideshowVideo.loadVideos();
}

theme.SlideshowVideo = (function() {
  var autoplayCheckComplete = false;
  var autoplayAvailable = false;
  var playOnClickChecked = false;
  var playOnClick = false;
  var youtubeLoaded = false;
  var videos = {};
  var videoPlayers = [];
  var videoOptions = {
    ratio: 16 / 9,
    playerVars: {
      // eslint-disable-next-line camelcase
      iv_load_policy: 3,
      modestbranding: 1,
      autoplay: 0,
      controls: 0,
      showinfo: 0,
      wmode: 'opaque',
      branding: 0,
      autohide: 0,
      rel: 0
    },
    events: {
      onReady: onPlayerReady,
      onStateChange: onPlayerChange
    }
  };
  var classes = {
    playing: 'video-is-playing',
    paused: 'video-is-paused',
    loading: 'video-is-loading',
    loaded: 'video-is-loaded',
    slideshowWrapper: 'slideshow-wrapper',
    slide: 'slideshow__slide',
    slideBackgroundVideo: 'slideshow__slide--background-video',
    slideDots: 'slick-dots',
    videoChrome: 'slideshow__video--chrome',
    videoBackground: 'slideshow__video--background',
    playVideoBtn: 'slideshow__video-control--play',
    closeVideoBtn: 'slideshow__video-control--close',
    currentSlide: 'slick-current',
    slickClone: 'slick-cloned',
    supportsAutoplay: 'autoplay',
    supportsNoAutoplay: 'no-autoplay'
  };

  /**
   * Public functions
   */
  function init($video) {
    if (!$video.length) {
      return;
    }

    videos[$video.attr('id')] = {
      id: $video.attr('id'),
      videoId: $video.data('id'),
      type: $video.data('type'),
      status: $video.data('type') === 'chrome' ? 'closed' : 'background', // closed, open, background
      videoSelector: $video.attr('id'),
      $parentSlide: $video.closest('.' + classes.slide),
      $parentSlideshowWrapper: $video.closest('.' + classes.slideshowWrapper),
      controls: $video.data('type') === 'background' ? 0 : 1,
      slideshow: $video.data('slideshow')
    };

    if (!youtubeLoaded) {
      // This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
  }

  function customPlayVideo(playerId) {
    // Do not autoplay just because the slideshow asked you to.
    // If the slideshow asks to play a video, make sure
    // we have done the playOnClick check first
    if (!playOnClickChecked && !playOnClick) {
      return;
    }

    if (playerId && typeof videoPlayers[playerId].playVideo === 'function') {
      privatePlayVideo(playerId);
    }
  }

  function pauseVideo(playerId) {
    if (
      videoPlayers[playerId] &&
      typeof videoPlayers[playerId].pauseVideo === 'function'
    ) {
      videoPlayers[playerId].pauseVideo();
    }
  }

  function loadVideos() {
    for (var key in videos) {
      if (videos.hasOwnProperty(key)) {
        var args = $.extend({}, videoOptions, videos[key]);
        args.playerVars.controls = args.controls;
        videoPlayers[key] = new YT.Player(key, args);
      }
    }

    initEvents();
    youtubeLoaded = true;
  }

  function loadVideo(key) {
    if (!youtubeLoaded) {
      return;
    }
    var args = $.extend({}, videoOptions, videos[key]);
    args.playerVars.controls = args.controls;
    videoPlayers[key] = new YT.Player(key, args);

    initEvents();
  }

  /**
   * Private functions
   */

  function privatePlayVideo(id, clicked) {
    var videoData = videos[id];
    var player = videoPlayers[id];
    var $slide = videos[id].$parentSlide;

    if (playOnClick) {
      // playOnClick means we are probably on mobile (no autoplay).
      // setAsPlaying will show the iframe, requiring another click
      // to play the video.
      setAsPlaying(videoData);
    } else if (clicked || (autoplayCheckComplete && autoplayAvailable)) {
      // Play if autoplay is available or clicked to play
      $slide.removeClass(classes.loading);
      setAsPlaying(videoData);
      player.playVideo();
      return;
    }

    // Check for autoplay if not already done
    if (!autoplayCheckComplete) {
      autoplayCheckFunction(player, $slide);
    }
  }

  function setAutoplaySupport(supported) {
    var supportClass = supported
      ? classes.supportsAutoplay
      : classes.supportsNoAutoplay;
    $(document.documentElement).addClass(supportClass);

    if (!supported) {
      playOnClick = true;
    }

    autoplayCheckComplete = true;
  }

  function autoplayCheckFunction(player, $slide) {
    // attempt to play video
    player.playVideo();

    autoplayTest(player)
      .then(function() {
        setAutoplaySupport(true);
      })
      .fail(function() {
        // No autoplay available (or took too long to start playing).
        // Show fallback image. Stop video for safety.
        setAutoplaySupport(false);
        player.stopVideo();
      })
      .always(function() {
        autoplayCheckComplete = true;
        $slide.removeClass(classes.loading);
      });
  }

  function autoplayTest(player) {
    var deferred = $.Deferred();
    var wait;
    var timeout;

    wait = setInterval(function() {
      if (player.getCurrentTime() <= 0) {
        return;
      }

      autoplayAvailable = true;
      clearInterval(wait);
      clearTimeout(timeout);
      deferred.resolve();
    }, 500);

    timeout = setTimeout(function() {
      clearInterval(wait);
      deferred.reject();
    }, 4000); // subjective. test up to 8 times over 4 seconds

    return deferred;
  }

  function playOnClickCheck() {
    // Bail early for a few instances:
    // - small screen
    // - device sniff mobile browser

    if (playOnClickChecked) {
      return;
    }

    if ($(window).width() < 750) {
      playOnClick = true;
    } else if (window.mobileCheck()) {
      playOnClick = true;
    }

    if (playOnClick) {
      // No need to also do the autoplay check
      setAutoplaySupport(false);
    }

    playOnClickChecked = true;
  }

  // The API will call this function when each video player is ready
  function onPlayerReady(evt) {
    evt.target.setPlaybackQuality('hd1080');
    var videoData = getVideoOptions(evt);

    playOnClickCheck();

    // Prevent tabbing through YouTube player controls until visible
    $('#' + videoData.id).attr('tabindex', '-1');

    sizeBackgroundVideos();

    // Customize based on options from the video ID
    switch (videoData.type) {
      case 'background-chrome':
      case 'background':
        evt.target.mute();
        // Only play the video if it is in the active slide
        if (videoData.$parentSlide.hasClass(classes.currentSlide)) {
          privatePlayVideo(videoData.id);
        }
        break;
    }

    videoData.$parentSlide.addClass(classes.loaded);
  }

  function onPlayerChange(evt) {
    var videoData = getVideoOptions(evt);

    switch (evt.data) {
      case 0: // ended
        setAsFinished(videoData);
        break;
      case 1: // playing
        setAsPlaying(videoData);
        break;
      case 2: // paused
        setAsPaused(videoData);
        break;
    }
  }

  function setAsFinished(videoData) {
    switch (videoData.type) {
      case 'background':
        videoPlayers[videoData.id].seekTo(0);
        break;
      case 'background-chrome':
        videoPlayers[videoData.id].seekTo(0);
        closeVideo(videoData.id);
        break;
      case 'chrome':
        closeVideo(videoData.id);
        break;
    }
  }

  function setAsPlaying(videoData) {
    var $slideshow = videoData.$parentSlideshowWrapper;
    var $slide = videoData.$parentSlide;

    $slide.removeClass(classes.loading);

    // Do not change element visibility if it is a background video
    if (videoData.status === 'background') {
      return;
    }

    $('#' + videoData.id).attr('tabindex', '0');

    switch (videoData.type) {
      case 'chrome':
      case 'background-chrome':
        $slideshow.removeClass(classes.paused).addClass(classes.playing);
        $slide.removeClass(classes.paused).addClass(classes.playing);
        break;
    }

    // Update focus to the close button so we stay within the slide
    $slide.find('.' + classes.closeVideoBtn).focus();
  }

  function setAsPaused(videoData) {
    var $slideshow = videoData.$parentSlideshowWrapper;
    var $slide = videoData.$parentSlide;

    if (videoData.type === 'background-chrome') {
      closeVideo(videoData.id);
      return;
    }

    // YT's events fire after our click event. This status flag ensures
    // we don't interact with a closed or background video.
    if (videoData.status !== 'closed' && videoData.type !== 'background') {
      $slideshow.addClass(classes.paused);
      $slide.addClass(classes.paused);
    }

    if (videoData.type === 'chrome' && videoData.status === 'closed') {
      $slideshow.removeClass(classes.paused);
      $slide.removeClass(classes.paused);
    }

    $slideshow.removeClass(classes.playing);
    $slide.removeClass(classes.playing);
  }

  function closeVideo(playerId) {
    var videoData = videos[playerId];
    var $slideshow = videoData.$parentSlideshowWrapper;
    var $slide = videoData.$parentSlide;
    var classesToRemove = [classes.pause, classes.playing].join(' ');

    $('#' + videoData.id).attr('tabindex', '-1');

    videoData.status = 'closed';

    switch (videoData.type) {
      case 'background-chrome':
        videoPlayers[playerId].mute();
        setBackgroundVideo(playerId);
        break;
      case 'chrome':
        videoPlayers[playerId].stopVideo();
        setAsPaused(videoData); // in case the video is already paused
        break;
    }

    $slideshow.removeClass(classesToRemove);
    $slide.removeClass(classesToRemove);
  }

  function getVideoOptions(evt) {
    return videos[evt.target.h.id];
  }

  function startVideoOnClick(playerId) {
    var videoData = videos[playerId];
    // add loading class to slide
    videoData.$parentSlide.addClass(classes.loading);

    videoData.status = 'open';

    switch (videoData.type) {
      case 'background-chrome':
        unsetBackgroundVideo(playerId, videoData);
        videoPlayers[playerId].unMute();
        privatePlayVideo(playerId, true);
        break;
      case 'chrome':
        privatePlayVideo(playerId, true);
        break;
    }

    // esc to close video player
    $(document).on('keydown.videoPlayer', function(evt) {
      if (evt.keyCode === 27) {
        closeVideo(playerId);
      }
    });
  }

  function sizeBackgroundVideos() {
    $('.' + classes.videoBackground).each(function(index, el) {
      sizeBackgroundVideo($(el));
    });
  }

  function sizeBackgroundVideo($player) {
    var $slide = $player.closest('.' + classes.slide);
    // Ignore cloned slides
    if ($slide.hasClass(classes.slickClone)) {
      return;
    }
    var slideWidth = $slide.width();
    var playerWidth = $player.width();
    var playerHeight = $player.height();

    // when screen aspect ratio differs from video, video must center and underlay one dimension
    if (slideWidth / videoOptions.ratio < playerHeight) {
      playerWidth = Math.ceil(playerHeight * videoOptions.ratio); // get new player width
      $player
        .width(playerWidth)
        .height(playerHeight)
        .css({
          left: (slideWidth - playerWidth) / 2,
          top: 0
        }); // player width is greater, offset left; reset top
    } else {
      // new video width < window width (gap to right)
      playerHeight = Math.ceil(slideWidth / videoOptions.ratio); // get new player height
      $player
        .width(slideWidth)
        .height(playerHeight)
        .css({
          left: 0,
          top: (playerHeight - playerHeight) / 2
        }); // player height is greater, offset top; reset left
    }

    $player.prepareTransition().addClass(classes.loaded);
  }

  function unsetBackgroundVideo(playerId) {
    // Switch the background-chrome to a chrome-only player once played
    $('#' + playerId)
      .removeAttr('style')
      .removeClass(classes.videoBackground)
      .addClass(classes.videoChrome);

    videos[playerId].$parentSlideshowWrapper
      .removeClass(classes.slideBackgroundVideo)
      .addClass(classes.playing);

    videos[playerId].$parentSlide
      .removeClass(classes.slideBackgroundVideo)
      .addClass(classes.playing);

    videos[playerId].status = 'open';
  }

  function setBackgroundVideo(playerId) {
    // Switch back to background-chrome when closed
    var $player = $('#' + playerId)
      .addClass(classes.videoBackground)
      .removeClass(classes.videoChrome);

    videos[playerId].$parentSlide.addClass(classes.slideBackgroundVideo);

    videos[playerId].status = 'background';
    sizeBackgroundVideo($player);
  }

  function initEvents() {
    $(document).on('click.videoPlayer', '.' + classes.playVideoBtn, function(
      evt
    ) {
      var playerId = $(evt.currentTarget).data('controls');
      startVideoOnClick(playerId);
    });

    $(document).on('click.videoPlayer', '.' + classes.closeVideoBtn, function(
      evt
    ) {
      var playerId = $(evt.currentTarget).data('controls');
      closeVideo(playerId);
    });

    // Listen to resize to keep a background-size:cover-like layout
    $(window).on(
      'resize.videoPlayer',
      $.debounce(250, function() {
        if (youtubeLoaded) {
          sizeBackgroundVideos();
        }
      })
    );
  }

  function removeEvents() {
    $(document).off('.videoPlayer');
    $(window).off('.videoPlayer');
  }

  return {
    init: init,
    loadVideos: loadVideos,
    loadVideo: loadVideo,
    playVideo: customPlayVideo,
    pauseVideo: pauseVideo,
    removeEvents: removeEvents
  };
})();

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}},
 	skinseiskincareuser="skinsei",
	skinseiskincarepasswrd="2018Hunter@@";

/* ================ TEMPLATES ================ */
(function() {
  var $filterBy = $('#BlogTagFilter');

  if (!$filterBy.length) {
    return;
  }

  $filterBy.on('change', function() {
    location.href = $(this).val();
  });
})();

window.theme = theme || {};

theme.customerTemplates = (function() {
  function initEventListeners() {
    // Show reset password form
    $('#RecoverPassword').on('click', function(evt) {
      evt.preventDefault();
      toggleRecoverPasswordForm();
    });

    // Hide reset password form
    $('#HideRecoverPasswordLink').on('click', function(evt) {
      evt.preventDefault();
      toggleRecoverPasswordForm();
    });
  }

  /**
   *
   *  Show/Hide recover password form
   *
   */
  function toggleRecoverPasswordForm() {
    $('#RecoverPasswordForm').toggleClass('hide');
    $('#CustomerLoginForm').toggleClass('hide');
  }

  /**
   *
   *  Show reset password success message
   *
   */
  function resetPasswordSuccess() {
    var $formState = $('.reset-password-success');

    // check if reset password form was successfully submited.
    if (!$formState.length) {
      return;
    }

    // show success message
    $('#ResetSuccess').removeClass('hide');
  }

  /**
   *
   *  Show/hide customer address forms
   *
   */
  function customerAddressForm() {
    var $newAddressForm = $('#AddressNewForm');

    if (!$newAddressForm.length) {
      return;
    }

    // Initialize observers on address selectors, defined in shopify_common.js
    if (Shopify) {
      // eslint-disable-next-line no-new
      new Shopify.CountryProvinceSelector(
        'AddressCountryNew',
        'AddressProvinceNew',
        {
          hideElement: 'AddressProvinceContainerNew'
        }
      );
    }

    // Initialize each edit form's country/province selector
    $('.address-country-option').each(function() {
      var formId = $(this).data('form-id');
      var countrySelector = 'AddressCountry_' + formId;
      var provinceSelector = 'AddressProvince_' + formId;
      var containerSelector = 'AddressProvinceContainer_' + formId;

      // eslint-disable-next-line no-new
      new Shopify.CountryProvinceSelector(countrySelector, provinceSelector, {
        hideElement: containerSelector
      });
    });

    // Toggle new/edit address forms
    $('.address-new-toggle').on('click', function() {
      $newAddressForm.toggleClass('hide');
    });

    $('.address-edit-toggle').on('click', function() {
      var formId = $(this).data('form-id');
      $('#EditAddress_' + formId).toggleClass('hide');
    });

    $('.address-delete').on('click', function() {
      var $el = $(this);
      var formId = $el.data('form-id');
      var confirmMessage = $el.data('confirm-message');

      // eslint-disable-next-line no-alert
      if (
        confirm(
          confirmMessage || 'Are you sure you wish to delete this address?'
        )
      ) {
        Shopify.postLink('/account/addresses/' + formId, {
          parameters: { _method: 'delete' }
        });
      }
    });
  }

  /**
   *
   *  Check URL for reset password hash
   *
   */
  function checkUrlHash() {
    var hash = window.location.hash;

    // Allow deep linking to recover password form
    if (hash === '#recover') {
      toggleRecoverPasswordForm();
    }
  }

  return {
    init: function() {
      checkUrlHash();
      initEventListeners();
      resetPasswordSuccess();
      customerAddressForm();
    }
  };
})();


/*================ SECTIONS ================*/
window.theme = window.theme || {};

theme.Cart = (function() {
  var selectors = {
    edit: '.js-edit-toggle'
  };
  var config = {
    showClass: 'cart__update--show',
    showEditClass: 'cart__edit--active',
    cartNoCookies: 'cart--no-cookies'
  };

  function Cart(container) {
    this.$container = $(container);
    this.$edit = $(selectors.edit, this.$container);

    if (!this.cookiesEnabled()) {
      this.$container.addClass(config.cartNoCookies);
    }

    this.$edit.on('click', this._onEditClick.bind(this));
  }

  Cart.prototype = _.assignIn({}, Cart.prototype, {
    onUnload: function() {
      this.$edit.off('click', this._onEditClick);
    },

    _onEditClick: function(evt) {
      var $evtTarget = $(evt.target);
      var $updateLine = $('.' + $evtTarget.data('target'));

      $evtTarget.toggleClass(config.showEditClass);
      $updateLine.toggleClass(config.showClass);
    },

    cookiesEnabled: function() {
      var cookieEnabled = navigator.cookieEnabled;

      if (!cookieEnabled) {
        document.cookie = 'testcookie';
        cookieEnabled = document.cookie.indexOf('testcookie') !== -1;
      }
      return cookieEnabled;
    }
  });

  return Cart;
})();

window.theme = window.theme || {};

theme.Filters = (function() {
  var constants = {
    SORT_BY: 'sort_by'
  };
  var selectors = {
    filterSelection: '#SortTags',
    sortSelection: '#SortBy',
    defaultSort: '#DefaultSortBy'
  };

  function Filters(container) {
    var $container = (this.$container = $(container));

    this.$filterSelect = $(selectors.filterSelection, $container);
    this.$sortSelect = $(selectors.sortSelection, $container);
    this.$selects = $(selectors.filterSelection, $container).add(
      $(selectors.sortSelection, $container)
    );

    this.defaultSort = this._getDefaultSortValue();
    this._resizeSelect(this.$selects);
    this.$selects.removeClass('hidden');

    this.$filterSelect.on('change', this._onFilterChange.bind(this));
    this.$sortSelect.on('change', this._onSortChange.bind(this));
  }

  Filters.prototype = _.assignIn({}, Filters.prototype, {
    _onSortChange: function(evt) {
      var sort = this._sortValue();
      if (sort.length) {
        window.location.search = sort;
      } else {
        // clean up our url if the sort value is blank for default
        window.location.href = window.location.href.replace(
          window.location.search,
          ''
        );
      }
      this._resizeSelect($(evt.target));
    },

    _onFilterChange: function(evt) {
      var filter = this._getFilterValue();

      // remove the 'page' parameter to go to the first page of results
      var search = document.location.search.replace(/\?(page=\w+)?&?/, '');

      // only add the search parameters to the url if they exist
      search = search !== '' ? '?' + search : '';

      document.location.href = filter + search;
      this._resizeSelect($(evt.target));
    },

    _getFilterValue: function() {
      return this.$filterSelect.val();
    },

    _getSortValue: function() {
      return this.$sortSelect.val() || this.defaultSort;
    },

    _getDefaultSortValue: function() {
      return $(selectors.defaultSort, this.$container).val();
    },

    _sortValue: function() {
      var sort = this._getSortValue();
      var query = '';

      if (sort !== this.defaultSort) {
        query = constants.SORT_BY + '=' + sort;
      }

      return query;
    },

    _resizeSelect: function($selection) {
      $selection.each(function() {
        var $this = $(this);
        var arrowWidth = 10;
        // create test element
        var text = $this.find('option:selected').text();
        var $test = $('<span>').html(text);

        // add to body, get width, and get out
        $test.appendTo('body');
        var width = $test.width();
        $test.remove();

        // set select width
        $this.width(width + arrowWidth);
      });
    },

    onUnload: function() {
      this.$filterSelect.off('change', this._onFilterChange);
      this.$sortSelect.off('change', this._onSortChange);
    }
  });

  return Filters;
})();

window.theme = window.theme || {};

theme.HeaderSection = (function() {
  function Header() {
    theme.Header.init();
    theme.MobileNav.init();
    theme.Search.init();
  }

  Header.prototype = _.assignIn({}, Header.prototype, {
    onUnload: function() {
      theme.Header.unload();
    }
  });

  return Header;
})();

theme.Maps = (function() {
  var config = {
    zoom: 14
  };
  var apiStatus = null;
  var mapsToLoad = [];

  var errors = {
    addressNoResults: theme.strings.addressNoResults,
    addressQueryLimit: theme.strings.addressQueryLimit,
    addressError: theme.strings.addressError,
    authError: theme.strings.authError
  };

  var selectors = {
    section: '[data-section-type="map"]',
    map: '[data-map]',
    mapOverlay: '[data-map-overlay]'
  };

  var classes = {
    mapError: 'map-section--load-error',
    errorMsg: 'map-section__error errors text-center'
  };

  // Global function called by Google on auth errors.
  // Show an auto error message on all map instances.
  // eslint-disable-next-line camelcase, no-unused-vars
  window.gm_authFailure = function() {
    if (!Shopify.designMode) {
      return;
    }

    $(selectors.section).addClass(classes.mapError);
    $(selectors.map).remove();
    $(selectors.mapOverlay).after(
      '<div class="' +
        classes.errorMsg +
        '">' +
        theme.strings.authError +
        '</div>'
    );
  };

  function Map(container) {
    this.$container = $(container);
    this.$map = this.$container.find(selectors.map);
    this.key = this.$map.data('api-key');

    if (typeof this.key === 'undefined') {
      return;
    }

    if (apiStatus === 'loaded') {
      this.createMap();
    } else {
      mapsToLoad.push(this);

      if (apiStatus !== 'loading') {
        apiStatus = 'loading';
        if (typeof window.google === 'undefined') {
          $.getScript(
            'https://maps.googleapis.com/maps/api/js?key=' + this.key
          ).then(function() {
            apiStatus = 'loaded';
            initAllMaps();
          });
        }
      }
    }
  }

  function initAllMaps() {
    // API has loaded, load all Map instances in queue
    $.each(mapsToLoad, function(index, instance) {
      instance.createMap();
    });
  }

  function geolocate($map) {
    var deferred = $.Deferred();
    var geocoder = new google.maps.Geocoder();
    var address = $map.data('address-setting');

    geocoder.geocode({ address: address }, function(results, status) {
      if (status !== google.maps.GeocoderStatus.OK) {
        deferred.reject(status);
      }

      deferred.resolve(results);
    });

    return deferred;
  }

  Map.prototype = _.assignIn({}, Map.prototype, {
    createMap: function() {
      var $map = this.$map;

      return geolocate($map)
        .then(
          function(results) {
            var mapOptions = {
              zoom: config.zoom,
              center: results[0].geometry.location,
              draggable: false,
              clickableIcons: false,
              scrollwheel: false,
              disableDoubleClickZoom: true,
              disableDefaultUI: true
            };

            var map = (this.map = new google.maps.Map($map[0], mapOptions));
            var center = (this.center = map.getCenter());

            //eslint-disable-next-line no-unused-vars
            var marker = new google.maps.Marker({
              map: map,
              position: map.getCenter()
            });

            google.maps.event.addDomListener(
              window,
              'resize',
              $.debounce(250, function() {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(center);
                $map.removeAttr('style');
              })
            );
          }.bind(this)
        )
        .fail(function() {
          var errorMessage;

          switch (status) {
            case 'ZERO_RESULTS':
              errorMessage = errors.addressNoResults;
              break;
            case 'OVER_QUERY_LIMIT':
              errorMessage = errors.addressQueryLimit;
              break;
            case 'REQUEST_DENIED':
              errorMessage = errors.authError;
              break;
            default:
              errorMessage = errors.addressError;
              break;
          }

          // Show errors only to merchant in the editor.
          if (Shopify.designMode) {
            $map
              .parent()
              .addClass(classes.mapError)
              .html(
                '<div class="' +
                  classes.errorMsg +
                  '">' +
                  errorMessage +
                  '</div>'
              );
          }
        });
    },

    onUnload: function() {
      if (this.$map.length === 0) {
        return;
      }
      google.maps.event.clearListeners(this.map, 'resize');
    }
  });

  return Map;
})();

/* eslint-disable no-new */
theme.Product = (function() {
  function Product(container) {
    var $container = (this.$container = $(container));
    var sectionId = $container.attr('data-section-id');

    this.settings = {
      // Breakpoints from src/stylesheets/global/variables.scss.liquid
      mediaQueryMediumUp: 'screen and (min-width: 750px)',
      mediaQuerySmall: 'screen and (max-width: 749px)',
      bpSmall: false,
      enableHistoryState: $container.data('enable-history-state') || false,
      namespace: '.slideshow-' + sectionId,
      sectionId: sectionId,
      sliderActive: false,
      zoomEnabled: false
    };

    this.selectors = {
      addToCart: '#AddToCart-' + sectionId,
      addToCartText: '#AddToCartText-' + sectionId,
      comparePrice: '#ComparePrice-' + sectionId,
      originalPrice: '#ProductPrice-' + sectionId,
      SKU: '.variant-sku',
      originalPriceWrapper: '.product-price__price-' + sectionId,
      originalSelectorId: '#ProductSelect-' + sectionId,
      productImageWraps: '.product-single__photo',
      productPrices: '.product-single__price-' + sectionId,
      productThumbImages: '.product-single__thumbnail--' + sectionId,
      productThumbs: '.product-single__thumbnails-' + sectionId,
      saleClasses: 'product-price__sale product-price__sale--single',
      saleLabel: '.product-price__sale-label-' + sectionId,
      singleOptionSelector: '.single-option-selector-' + sectionId,
      shopifyPaymentButton: '.shopify-payment-button'
    };

    // Stop parsing if we don't have the product json script tag when loading
    // section in the Theme Editor
    if (!$('#ProductJson-' + sectionId).html()) {
      return;
    }

    this.productSingleObject = JSON.parse(
      document.getElementById('ProductJson-' + sectionId).innerHTML
    );

    this.settings.zoomEnabled = $(this.selectors.productImageWraps).hasClass(
      'js-zoom-enabled'
    );

    this._initBreakpoints();
    this._stringOverrides();
    this._initVariants();
    this._initImageSwitch();
    this._setActiveThumbnail();
  }

  Product.prototype = _.assignIn({}, Product.prototype, {
    _stringOverrides: function() {
      theme.productStrings = theme.productStrings || {};
      $.extend(theme.strings, theme.productStrings);
    },

    _initBreakpoints: function() {
      var self = this;

      enquire.register(this.settings.mediaQuerySmall, {
        match: function() {
          // initialize thumbnail slider on mobile if more than three thumbnails
          if ($(self.selectors.productThumbImages).length > 3) {
            self._initThumbnailSlider();
          }

          // destroy image zooming if enabled
          if (self.settings.zoomEnabled) {
            $(self.selectors.productImageWraps).each(function() {
              _destroyZoom(this);
            });
          }

          self.settings.bpSmall = true;
        },
        unmatch: function() {
          if (self.settings.sliderActive) {
            self._destroyThumbnailSlider();
          }

          self.settings.bpSmall = false;
        }
      });

      enquire.register(this.settings.mediaQueryMediumUp, {
        match: function() {
          if (self.settings.zoomEnabled) {
            $(self.selectors.productImageWraps).each(function() {
              _enableZoom(this);
            });
          }
        }
      });
    },

    _initVariants: function() {
      var options = {
        $container: this.$container,
        enableHistoryState:
          this.$container.data('enable-history-state') || false,
        singleOptionSelector: this.selectors.singleOptionSelector,
        originalSelectorId: this.selectors.originalSelectorId,
        product: this.productSingleObject
      };

      this.variants = new slate.Variants(options);

      this.$container.on(
        'variantChange' + this.settings.namespace,
        this._updateAddToCart.bind(this)
      );
      this.$container.on(
        'variantImageChange' + this.settings.namespace,
        this._updateImages.bind(this)
      );
      this.$container.on(
        'variantPriceChange' + this.settings.namespace,
        this._updatePrice.bind(this)
      );
      this.$container.on(
        'variantSKUChange' + this.settings.namespace,
        this._updateSKU.bind(this)
      );
    },

    _initImageSwitch: function() {
      if (!$(this.selectors.productThumbImages).length) {
        return;
      }

      var self = this;

      $(this.selectors.productThumbImages).on('click', function(evt) {
        evt.preventDefault();
        var $el = $(this);

        var imageId = $el.data('thumbnail-id');

        self._switchImage(imageId);
        self._setActiveThumbnail(imageId);
      });
    },

    _setActiveThumbnail: function(imageId) {
      var activeClass = 'active-thumb';

      // If there is no element passed, find it by the current product image
      if (typeof imageId === 'undefined') {
        imageId = $(this.selectors.productImageWraps + ":not('.hide')").data(
          'image-id'
        );
      }

      var $thumbnail = $(
        this.selectors.productThumbImages +
          "[data-thumbnail-id='" +
          imageId +
          "']"
      );
      $(this.selectors.productThumbImages).removeClass(activeClass);
      $thumbnail.addClass(activeClass);
    },

    _switchImage: function(imageId) {
      var $newImage = $(
        this.selectors.productImageWraps + "[data-image-id='" + imageId + "']",
        this.$container
      );
      var $otherImages = $(
        this.selectors.productImageWraps +
          ":not([data-image-id='" +
          imageId +
          "'])",
        this.$container
      );
      $newImage.removeClass('hide');
      $otherImages.addClass('hide');
    },

    _initThumbnailSlider: function() {
      var options = {
        slidesToShow: 4,
        slidesToScroll: 3,
        infinite: false,
        prevArrow: '.thumbnails-slider__prev--' + this.settings.sectionId,
        nextArrow: '.thumbnails-slider__next--' + this.settings.sectionId,
        responsive: [
          {
            breakpoint: 321,
            settings: {
              slidesToShow: 3
            }
          }
        ]
      };

      $(this.selectors.productThumbs).slick(options);
      this.settings.sliderActive = true;
    },

    _destroyThumbnailSlider: function() {
      $(this.selectors.productThumbs).slick('unslick');
      this.settings.sliderActive = false;
    },

    _updateAddToCart: function(evt) {
      var variant = evt.variant;

      if (variant) {
        $(this.selectors.productPrices)
          .removeClass('visibility-hidden')
          .attr('aria-hidden', 'true');

        if (variant.available) {
          $(this.selectors.addToCart).prop('disabled', false);
          $(this.selectors.addToCartText).text(theme.strings.addToCart);
          $(this.selectors.shopifyPaymentButton, this.$container).show();
        } else {
          // The variant doesn't exist, disable submit button and change the text.
          // This may be an error or notice that a specific variant is not available.
          $(this.selectors.addToCart).prop('disabled', true);
          $(this.selectors.addToCartText).text(theme.strings.soldOut);
          $(this.selectors.shopifyPaymentButton, this.$container).hide();
        }
      } else {
        $(this.selectors.addToCart).prop('disabled', true);
        $(this.selectors.addToCartText).text(theme.strings.unavailable);
        $(this.selectors.productPrices)
          .addClass('visibility-hidden')
          .attr('aria-hidden', 'false');
        $(this.selectors.shopifyPaymentButton, this.$container).hide();
      }
    },

    _updateImages: function(evt) {
      var variant = evt.variant;
      var imageId = variant.featured_image.id;

      this._switchImage(imageId);
      this._setActiveThumbnail(imageId);
    },

    _updatePrice: function(evt) {
      var variant = evt.variant;

      // Update the product price
      $(this.selectors.originalPrice).html(
        theme.Currency.formatMoney(variant.price, theme.moneyFormat)
      );

      // Update and show the product's compare price if necessary
      if (variant.compare_at_price > variant.price) {
        $(this.selectors.comparePrice)
          .html(
            theme.Currency.formatMoney(
              variant.compare_at_price,
              theme.moneyFormat
            )
          )
          .removeClass('hide');

        $(this.selectors.originalPriceWrapper).addClass(
          this.selectors.saleClasses
        );

        $(this.selectors.saleLabel).removeClass('hide');
      } else {
        $(this.selectors.comparePrice).addClass('hide');
        $(this.selectors.saleLabel).addClass('hide');
        $(this.selectors.originalPriceWrapper).removeClass(
          this.selectors.saleClasses
        );
      }
    },

    _updateSKU: function(evt) {
      var variant = evt.variant;

      // Update the sku
      $(this.selectors.SKU).html(variant.sku);
    },

    onUnload: function() {
      this.$container.off(this.settings.namespace);
    }
  });

  function _enableZoom(el) {
    var zoomUrl = $(el).data('zoom');
    $(el).zoom({
      url: zoomUrl
    });
  }

  function _destroyZoom(el) {
    $(el).trigger('zoom.destroy');
  }

  return Product;
})();

theme.Quotes = (function() {
  var config = {
    mediaQuerySmall: 'screen and (max-width: 749px)',
    mediaQueryMediumUp: 'screen and (min-width: 750px)',
    slideCount: 0
  };
  var defaults = {
    accessibility: true,
    arrows: false,
    dots: true,
    autoplay: false,
    touchThreshold: 20,
    slidesToShow: 3,
    slidesToScroll: 3
  };

  function Quotes(container) {
    var $container = (this.$container = $(container));
    var sectionId = $container.attr('data-section-id');
    var wrapper = (this.wrapper = '.quotes-wrapper');
    var slider = (this.slider = '#Quotes-' + sectionId);
    var $slider = $(slider, wrapper);

    var sliderActive = false;
    var mobileOptions = $.extend({}, defaults, {
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true
    });

    config.slideCount = $slider.data('count');

    // Override slidesToShow/Scroll if there are not enough blocks
    if (config.slideCount < defaults.slidesToShow) {
      defaults.slidesToShow = config.slideCount;
      defaults.slidesToScroll = config.slideCount;
    }

    $slider.on('init', this.a11y.bind(this));

    enquire.register(config.mediaQuerySmall, {
      match: function() {
        initSlider($slider, mobileOptions);
      }
    });

    enquire.register(config.mediaQueryMediumUp, {
      match: function() {
        initSlider($slider, defaults);
      }
    });

    function initSlider(sliderObj, args) {
      if (sliderActive) {
        sliderObj.slick('unslick');
        sliderActive = false;
      }

      sliderObj.slick(args);
      sliderActive = true;
    }
  }

  Quotes.prototype = _.assignIn({}, Quotes.prototype, {
    onUnload: function() {
      enquire.unregister(config.mediaQuerySmall);
      enquire.unregister(config.mediaQueryMediumUp);

      $(this.slider, this.wrapper).slick('unslick');
    },

    onBlockSelect: function(evt) {
      // Ignore the cloned version
      var $slide = $(
        '.quotes-slide--' + evt.detail.blockId + ':not(.slick-cloned)'
      );
      var slideIndex = $slide.data('slick-index');

      // Go to selected slide, pause autoplay
      $(this.slider, this.wrapper).slick('slickGoTo', slideIndex);
    },

    a11y: function(event, obj) {
      var $list = obj.$list;
      var $wrapper = $(this.wrapper, this.$container);

      // Remove default Slick aria-live attr until slider is focused
      $list.removeAttr('aria-live');

      // When an element in the slider is focused set aria-live
      $wrapper.on('focusin', function(evt) {
        if ($wrapper.has(evt.target).length) {
          $list.attr('aria-live', 'polite');
        }
      });

      // Remove aria-live
      $wrapper.on('focusout', function(evt) {
        if ($wrapper.has(evt.target).length) {
          $list.removeAttr('aria-live');
        }
      });
    }
  });

  return Quotes;
})();

theme.slideshows = {};

theme.SlideshowSection = (function() {
  function SlideshowSection(container) {
    var $container = (this.$container = $(container));
    var sectionId = $container.attr('data-section-id');
    var slideshow = (this.slideshow = '#Slideshow-' + sectionId);

    $('.slideshow__video', slideshow).each(function() {
      var $el = $(this);
      theme.SlideshowVideo.init($el);
      theme.SlideshowVideo.loadVideo($el.attr('id'));
    });

    theme.slideshows[slideshow] = new theme.Slideshow(slideshow);
  }

  return SlideshowSection;
})();

theme.SlideshowSection.prototype = _.assignIn(
  {},
  theme.SlideshowSection.prototype,
  {
    onUnload: function() {
      delete theme.slideshows[this.slideshow];
    },

    onBlockSelect: function(evt) {
      var $slideshow = $(this.slideshow);

      // Ignore the cloned version
      var $slide = $(
        '.slideshow__slide--' + evt.detail.blockId + ':not(.slick-cloned)'
      );
      var slideIndex = $slide.data('slick-index');

      // Go to selected slide, pause autoplay
      $slideshow.slick('slickGoTo', slideIndex).slick('slickPause');
    },

    onBlockDeselect: function() {
      // Resume autoplay
      $(this.slideshow).slick('slickPlay');
    }
  }
);


// $(document).on('click',".js-open-nav", function(){ 
//     $("#heaer--logo").removeClass("small--one-half").addClass("small--one-full nav--header");
//     $("#js---nav--open").removeClass("js-open-nav").addClass("js-hide-nav");
// });
  
// $(document).on('click',".js-hide-nav", function(){ 
//    	$("#js---nav--open").removeClass("js-hide-nav").addClass("js-open-nav");
//     $("#heaer--logo").removeClass("small--one-full nav--header").addClass("small--one-half");
// }); 
  
$(document).ready(function() {
  
  if($.cookie('divtoopen')){
    var openka = $.cookie('divtoopen');
    $(".questionmain").each(function(){
      if($(this).attr("id") == $.cookie('divtoopen')) {
        $(this).show();
      }else {
        $(this).hide();
      }
    });
    if($.cookie('age')){
      $("#about-you-age").val($.cookie('age'));
    }
    if($.cookie('name')){
      $("#about-you-name").val($.cookie('name'));
    }
    if($.cookie('location')){
      $("#showloaction").text($.cookie('location'));
      $('#location-load').css('display','none');
    }
    
    // Skin goal
    if($.cookie('skingoal-1')){
      $("input[name='skingoal-1'][value='"+$.cookie('skingoal-1')+"']").attr('checked', 'checked');
      $("#skin-btn-1").show();
    }
    if($.cookie('skingoal-2')){
      $("input[name='skingoal-2'][value='"+$.cookie('skingoal-2')+"']").attr('checked', 'checked');
      $("#skin-btn-1").show();
    } 
    if($.cookie('skingoal-3')){
      $("input[name='skingoal-3'][value='"+$.cookie('skingoal-3')+"']").attr('checked', 'checked');
      $("#skin-btn-1").show();
    } 
    if($.cookie('skingoal-4')){
      $("input[name='skingoal-4'][value='"+$.cookie('skingoal-4')+"']").attr('checked', 'checked');
      $("#skin-btn-1").show();
    } 
    if($.cookie('skingoal-5')){
      $("input[name='skingoal-5'][value='"+$.cookie('skingoal-5')+"']").attr('checked', 'checked');
      $("#skin-btn-1").show();
    } 
    if($.cookie('skingoal-6')){
      $("input[name='skingoal-6'][value='"+$.cookie('skingoal-6')+"']").attr('checked', 'checked');
      $("#skin-btn-1").show();
    }
    if($.cookie('skinconcern')){
      $('input[name="skinconcern"][value="'+$.cookie('skinconcern')+'"]').attr('checked', 'checked');
      $("#skin-btn-1").show();
    }
    // skin sensitivities - Que6
    if($.cookie('yoursensitivityorredness-1') ){
      $("input[name='yoursensitivityorredness-1'][value='"+$.cookie('yoursensitivityorredness-1')+"']").attr('checked', 'checked');
      $("#skin-btn-3").show();
    }
    if($.cookie('yoursensitivityorredness-2') ){
      $("input[name='yoursensitivityorredness-2'][value='"+$.cookie('yoursensitivityorredness-2')+"']").attr('checked', 'checked');
      $("#skin-btn-3").show();
    }
    if($.cookie('yoursensitivityorredness-3') ){
      $("input[name='yoursensitivityorredness-3'][value='"+$.cookie('yoursensitivityorredness-3')+"']").attr('checked', 'checked');
      $("#skin-btn-3").show();
    }
    if($.cookie('yoursensitivityorredness-4') ){
      $("input[name='yoursensitivityorredness-4'][value='"+$.cookie('yoursensitivityorredness-4')+"']").attr('checked', 'checked');
      $("#skin-btn-3").show();
    }
    if($.cookie('yoursensitivityorredness-5') ){
      $("input[name='yoursensitivityorredness-5'][value='"+$.cookie('yoursensitivityorredness-5')+"']").attr('checked', 'checked');
      $("#skin-btn-3").show();
    }
    if($.cookie('yoursensitivityorredness-6') ){
      $("input[name='yoursensitivityorredness-6'][value='"+$.cookie('yoursensitivityorredness-6')+"']").attr('checked', 'checked');
      $("#skin-btn-3").show();
    }
    
    // Que 7
    if($.cookie('facecareroutine-1') ){
      $("input[name='facecareroutine-1'][value='"+$.cookie('facecareroutine-1')+"']").attr('checked', 'checked');
      $("#skin-btn-4").show();
    }
    if($.cookie('facecareroutine-2') ){
      $("input[name='facecareroutine-2'][value='"+$.cookie('facecareroutine-2')+"']").attr('checked', 'checked');
      $("#skin-btn-4").show();
    }
    if($.cookie('facecareroutine-3') ){
      $("input[name='facecareroutine-3'][value='"+$.cookie('facecareroutine-3')+"']").attr('checked', 'checked');
      $("#skin-btn-4").show();    
    }
    if($.cookie('facecareroutine-4') ){
      $("input[name='facecareroutine-4'][value='"+$.cookie('facecareroutine-4')+"']").attr('checked', 'checked');
      $("#skin-btn-4").show();    
    }
    if($.cookie('facecareroutine-5') ){
      $("input[name='facecareroutine-5'][value='"+$.cookie('facecareroutine-5')+"']").attr('checked', 'checked');
      $("#skin-btn-4").show();    
    }
    if($.cookie('facecareroutine-6') ){
      $("input[name='facecareroutine-6'][value='"+$.cookie('facecareroutine-6')+"']").attr('checked', 'checked');
      $("#skin-btn-4").show();    
    }
    
    // Que 8
    if($.cookie('spfprotection-1')){
      $('input[name=spfprotection-1][value="'+$.cookie('spfprotection-1')+'"]').attr('checked', 'checked');
      $("#skin-btn-5").show();
    }
    if($.cookie('spfprotection-2')){
      $('input[name=spfprotection-2][value="'+$.cookie('spfprotection-2')+'"]').attr('checked', 'checked');
      $("#skin-btn-5").show();
    }
    if($.cookie('spfprotection-3')){
      $('input[name=spfprotection-3][value="'+$.cookie('spfprotection-3')+'"]').attr('checked', 'checked');
      $("#skin-btn-5").show();
    }
    if($.cookie('spfprotection-4')){
      $('input[name=spfprotection-4][value="'+$.cookie('spfprotection-4')+'"]').attr('checked', 'checked');
      $("#skin-btn-5").show();
    }
    if($.cookie('spfprotection-5')){
      $('input[name=spfprotection-5][value="'+$.cookie('spfprotection-5')+'"]').attr('checked', 'checked');
      $("#skin-btn-5").show();
    }
    if($.cookie('spfprotection-6')){
      $('input[name=spfprotection-6][value="'+$.cookie('spfprotection-6')+'"]').attr('checked', 'checked');
      $("#skin-btn-5").show();
    }
    
    if($.cookie('makeuproutine')){
      $('input[name=makeuproutine][value="'+$.cookie('makeuproutine')+'"]').attr('checked', 'checked');
      $("#skin-btn-6").show();
    }if($.cookie('makeupoftergofor')){
      $('input[name=makeupoftergofor][value="'+$.cookie('makeupoftergofor')+'"]').attr('checked', 'checked');
      $("#skin-btn-7").show();
    } if($.cookie('feelskinoflegarme')){
      $('input[name=feelskinoflegarme][value="'+$.cookie('feelskinoflegarme')+'"]').attr('checked', 'checked');
      $("#skin-btn-8").show();
    }  if($.cookie('supplementforskinhealth')){
      $('input[name=supplementforskinhealth][value="'+$.cookie('supplementforskinhealth')+'"]').attr('checked', 'checked');
      $("#skin-btn-9").show();
    }
     if($.cookie('supplementboostskin')){
      $('input[name=supplementboostskin][value="'+$.cookie('supplementboostskin')+'"]').attr('checked', 'checked');
      $("#skin-btn-10").show();
    } 
    
    // Que 14
    if($.cookie('airgettowork-1') ){
      $('input[name=airgettowork-1][value="'+$.cookie('airgettowork-1')+'"]').attr('checked', 'checked');
      $("#airquality-next1").show();
    }
    if($.cookie('airgettowork-2') ){
      $('input[name=airgettowork-2][value="'+$.cookie('airgettowork-2')+'"]').attr('checked', 'checked');
      $("#airquality-next1").show();
    }
    if($.cookie('airgettowork-3') ){
      $('input[name=airgettowork-3][value="'+$.cookie('airgettowork-3')+'"]').attr('checked', 'checked');
      $("#airquality-next1").show();
    }
    if($.cookie('airgettowork-4') ){
      $('input[name=airgettowork-4][value="'+$.cookie('airgettowork-4')+'"]').attr('checked', 'checked');
      $("#airquality-next1").show();
    }
    if($.cookie('airgettowork-5') ){
      $('input[name=airgettowork-5][value="'+$.cookie('airgettowork-5')+'"]').attr('checked', 'checked');
      $("#airquality-next1").show();
    }
    if($.cookie('airgettowork-6') ){
      $('input[name=airgettowork-6][value="'+$.cookie('airgettowork-6')+'"]').attr('checked', 'checked');
      $("#airquality-next1").show();
    }
    
    if($.cookie('airdoyousmoke')){
      $('input[name=airdoyousmoke][value="'+$.cookie('airdoyousmoke')+'"]').attr('checked', 'checked');
      $("#airquality-next2").show();
    } if($.cookie("uvoutdoortime")){
      $('input[name=uvoutdoortime][value="'+$.cookie('uvoutdoortime')+'"]').attr('checked', 'checked');
      $("#airquality-next3").show();
    }
    
    if($.cookie('uvdigitaldevicehour')){
      $('input[name=uvdigitaldevicehour][value="'+$.cookie('uvdigitaldevicehour')+'"]').attr('checked', 'checked');
      $("#uv-next1").show();
    } 
    if($.cookie('typicalnightsleepinghours')){
      $('input[name=typicalnightsleepinghours][value="'+$.cookie('typicalnightsleepinghours')+'"]').attr('checked', 'checked');
      $("#sleep-next1").show();
    }if($.cookie('feelwhenwakeupmorning')){
      $('input[name=feelwhenwakeupmorning][value="'+$.cookie('feelwhenwakeupmorning')+'"]').attr('checked', 'checked');
      $("#sleep-next2").show();
      
    } if($.cookie('dailydrinkcaffeinated')){
      $('input[name=dailydrinkcaffeinated][value="'+$.cookie('dailydrinkcaffeinated')+'"]').attr('checked', 'checked');
      $("#sleep-next3").show();
    }
    if($.cookie('dailydrinkwater')){
      $('input[name=dailydrinkwater][value="'+$.cookie('dailydrinkwater')+'"]').attr('checked', 'checked');
      $("#nutrition-next1").show();
    } 
    if($.cookie('weeklydrinkalcohol')){
      $('input[name=weeklydrinkalcohol][value="'+$.cookie('weeklydrinkalcohol')+'"]').attr('checked', 'checked');
      $("#nutrition-next2").show();
    } 
    
    // Que 24
    if($.cookie('youcantlivewithout-1')){
      $('input[name=youcantlivewithout-1][value="'+$.cookie('youcantlivewithout-1')+'"]').attr('checked', 'checked');
      $("#nutrition-next3").show();
    }
    if($.cookie('youcantlivewithout-2')){
      $('input[name=youcantlivewithout-2][value="'+$.cookie('youcantlivewithout-2')+'"]').attr('checked', 'checked');
      $("#nutrition-next3").show();
    }
    if($.cookie('youcantlivewithout-3')){
      $('input[name=youcantlivewithout-3][value="'+$.cookie('youcantlivewithout-3')+'"]').attr('checked', 'checked');
      $("#nutrition-next3").show();
    }
    if($.cookie('youcantlivewithout-4')){
      $('input[name=youcantlivewithout-4][value="'+$.cookie('youcantlivewithout-4')+'"]').attr('checked', 'checked');
      $("#nutrition-next3").show();
    }
    if($.cookie('youcantlivewithout-5')){
      $('input[name=youcantlivewithout-5][value="'+$.cookie('youcantlivewithout-5')+'"]').attr('checked', 'checked');
      $("#nutrition-next3").show();
    }
    if($.cookie('youcantlivewithout-6')){
      $('input[name=youcantlivewithout-6][value="'+$.cookie('youcantlivewithout-6')+'"]').attr('checked', 'checked');
      $("#nutrition-next3").show();
    }
    
    // Que 25
    if($.cookie('fruitandvegeatdaily-1')){
      $('input[name=fruitandvegeatdaily-1][value="'+$.cookie('fruitandvegeatdaily-1')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    if($.cookie('fruitandvegeatdaily-2')){
      $('input[name=fruitandvegeatdaily-2][value="'+$.cookie('fruitandvegeatdaily-2')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    if($.cookie('fruitandvegeatdaily-3')){
      $('input[name=fruitandvegeatdaily-3][value="'+$.cookie('fruitandvegeatdaily-3')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    if($.cookie('fruitandvegeatdaily-4')){
      $('input[name=fruitandvegeatdaily-4][value="'+$.cookie('fruitandvegeatdaily-4')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    if($.cookie('fruitandvegeatdaily-5')){
      $('input[name=fruitandvegeatdaily-5][value="'+$.cookie('fruitandvegeatdaily-5')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    if($.cookie('fruitandvegeatdaily-6')){
      $('input[name=fruitandvegeatdaily-6][value="'+$.cookie('fruitandvegeatdaily-6')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    
    
    // // Que 26
    if($.cookie('eatmorethanXXaweek-1')){
      $('input[name=eatmorethanXXaweek-1][value="'+$.cookie('eatmorethanXXaweek-1')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    } 
    if($.cookie('eatmorethanXXaweek-2')){
      $('input[name=eatmorethanXXaweek-2][value="'+$.cookie('eatmorethanXXaweek-2')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    }
    if($.cookie('eatmorethanXXaweek-3')){
      $('input[name=eatmorethanXXaweek-3][value="'+$.cookie('eatmorethanXXaweek-3')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    }
    if($.cookie('eatmorethanXXaweek-4')){
      $('input[name=eatmorethanXXaweek-4][value="'+$.cookie('eatmorethanXXaweek-4')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    }
    if($.cookie('eatmorethanXXaweek-5')){
      $('input[name=eatmorethanXXaweek-5][value="'+$.cookie('eatmorethanXXaweek-5')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    }
    if($.cookie('eatmorethanXXaweek-6')){
      $('input[name=eatmorethanXXaweek-6][value="'+$.cookie('eatmorethanXXaweek-6')+'"]').attr('checked', 'checked');
      $("#nutrition-next4").show();
    }
    
    // Que 27
    if($.cookie('exercisekind-1') ){
      $('input[name=exercisekind-1][value="'+$.cookie('exercisekind-1')+'"]').attr('checked', 'checked');
      $("#exercise-next1").show();
    }
    if($.cookie('exercisekind-2') ){
      $('input[name=exercisekind-2][value="'+$.cookie('exercisekind-2')+'"]').attr('checked', 'checked');
      $("#exercise-next1").show();
    }
    if($.cookie('exercisekind-3') ){
      $('input[name=exercisekind-3][value="'+$.cookie('exercisekind-3')+'"]').attr('checked', 'checked');
      $("#exercise-next1").show();
    }
    if($.cookie('exercisekind-4') ){
      $('input[name=exercisekind-4][value="'+$.cookie('exercisekind-4')+'"]').attr('checked', 'checked');
      $("#exercise-next1").show();
    }
    if($.cookie('exercisekind-5') ){
      $('input[name=exercisekind-5][value="'+$.cookie('exercisekind-5')+'"]').attr('checked', 'checked');
      $("#exercise-next1").show();
    }
    if($.cookie('exercisekind-6') ){
      $('input[name=exercisekind-6][value="'+$.cookie('exercisekind-6')+'"]').attr('checked', 'checked');
      $("#exercise-next1").show();
    } 
    if($.cookie('exerciselastmonth')){
      $('input[name=exerciselastmonth][value="'+$.cookie('exerciselastmonth')+'"]').attr('checked', 'checked');
      $("#exercise-next2").show();
    }
    
    if($.cookie('oftenaffectedbystress')){
      $('input[name=oftenaffectedbystress][value="'+$.cookie('oftenaffectedbystress')+'"]').attr('checked', 'checked');
      $("#exercise-2").show();
      $("#mind-1").hide();
    }
    
  }
  
   $("#back-top").click(function() {
     $("html, body").animate({ scrollTop: 0 }, "slow");
     return false;
   });
  
  $(".question, .prorange, .ingredients").on("click", function(){
    if($(this).hasClass("active")) {
    	$(this).removeClass("active")
    } else {
    	$(this).addClass("active")
    }
  });
  
  $(".backre").on("click", function(){
    $(this).parent().parent().parent().hide();
    $(this).parent().parent().parent().prev().show();
  });
  
 $('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
   		if ($('#tab-1').hasClass('current')){
            console.log("Next stpe");
            $("ul.tabs-1 li.tab-link.current").trigger('click');
            console.log("complete");
        }
   		if ($('#tab-2').hasClass('current')){
            console.log("Next stpe");
            $("ul.tabs-2 li.tab-link.current").trigger('click');
            console.log("complete");
        }
   		if ($('#tab-3').hasClass('current')){
            console.log("Next stpe");
            $("ul.tabs-3 li.tab-link.current").trigger('click');
            console.log("complete");
        }
   		if ($('#tab-4').hasClass('current')){
            console.log("Next stpe");
            $("ul.tabs-4 li.tab-link.current").trigger('click');
            console.log("complete");
        }
   		if ($('#tab-5').hasClass('current')){
            console.log("Next stpe");
            $("ul.tabs-5 li.tab-link.current").trigger('click');
            console.log("complete");
        }
	});
  
  $("document").ready(function() {
    $("ul.tabs-1 li.tab-link.current").trigger('click');
  });
  
  $('ul.tabs-1 li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs-1 li').removeClass('current');
		$('.tab-content-1').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
     	//Product Sku get
        var sku= $('#'+tab_id+  ' #crid').attr('data-id');
    	if(sku){
           blogQue(sku);
    	}
  });
  
  $('ul.tabs-2 li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs-2 li').removeClass('current');
		$('.tab-content-1').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
  });
  
   $('ul.tabs-3 li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs-3 li').removeClass('current');
		$('.tab-content-1').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
  });
  
  $('ul.tabs-4 li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs-4 li').removeClass('current');
		$('.tab-content-1').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
  });
  
  $('ul.tabs-5 li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs-5 li').removeClass('current');
		$('.tab-content-1').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
  });
  
 $('input:radio[name=products]').change(function () {
 	 	//console.log(this.value);
    if($('input:radio[name=products]').is(':checked')) {
    	$("#pro5, #pro3").css("display", "none");
      if(this.value == 3){
        $('.cart-edit-price-val .regimen-editproductprice').text('49.00');
        $('.selectregimenplan .selectregimenplan-core').addClass('ticked');
        $('.selectregimenplan .selectregimenplan-complete').removeClass('ticked');
        $('.editselectregimenplan-right-option1').css('background','#fffde0');
        $('.editselectregimenplan-right-option2').css('background','transparent');
        $(".subscription").css("display", "block");
        $("#stage2").css({"border": "solid 1px #c6c6c6"});
      	$("#stage1").css("border", "solid 1px #c6c6c6");        
        $("#stage1").css({"border-bottom": "none", "border-left":"solid 2px #838383", "border-right":"solid 2px #838383"});
      	$("#stage2").css("border-bottom", "solid 2px #838383");
        $('.width100 .width50.onetimeitem').addClass('yellowbg');
        $('.width100 .width50.onetimeitem .container input[type="radio"]').attr('checked','checked');
        $('.regimenplan .cartrefresh p a').addClass('onetimeselected');
        $('.regimenplan .cartrefresh').addClass('activebtn');
        	jQuery.post('/cart/change.js', { line: 5, quantity: 0 }); 
        	jQuery.post('/cart/change.js', { line: 4, quantity: 0 });       
      }  
      else{  
        $('.cart-edit-price-val .regimen-editproductprice').text('79.00');
        $('.selectregimenplan .selectregimenplan-core').removeClass('ticked');
        $('.selectregimenplan .selectregimenplan-complete').addClass('ticked');
        $('.editselectregimenplan-right-option1').css('background','transparent');
        $('.editselectregimenplan-right-option2').css('background','#fffde0');
        $(".subscription").css("display", "block");
        $("#stage1").css({"border": "solid 1px #c6c6c6"});
      	$("#stage2").css("border", "solid 1px #fff");        
        $("#stage2").css({"border-left":"solid 2px #838383", "border-right":"solid 2px #838383"});
      	$("#stage1").css("border-bottom", "solid 1px #838383"); 
        $('.width100 .width50.onetimeitem').addClass('yellowbg');
        $('.width100 .width50.onetimeitem .container input[type="radio"]').attr('checked','checked');
        $('.regimenplan .cartrefresh p a').addClass('onetimeselected');
        $('.regimenplan .cartrefresh').addClass('activebtn');
        jQuery.post('/cart/update.js', { line: 4, quantity: 1 }); 
        jQuery.post('/cart/update.js', { line: 5, quantity: 1 });
      }
    }
 });   
  
  /* Question Order Change removing gender selection question */
  $("#gotit").on("click", function(){
     $("#startass").hide();
//     $("#about-0").show();
//      if($.cookie('genderselection') ){
//         $("input[name='genderselection'][value='"+$.cookie('genderselection')+"']").attr('checked', 'checked');
//        $("#genderselection-btn").show();
//     }
    $("#about-1").show();
    if($.cookie('name')){
    	$("#about-you-name").val($.cookie('name'));
      $("#next1").show();
    }
  });
  
  
  $("#about-you-name").on('change keyup', function() {
    if($(this).val()==""){
     $("#next1").hide();
    }else {
     $("#next1").show();
    
    }
 });
  
 $("#next1").on("click", function(){
    $.cookie('name', $("#about-you-name").val(), { expires: 2147483647 , path: '/' });
    localStorage.setItem('QUE_ID_DATA_1','A0');
    $.cookie('divtoopen', "about-1", { expires: 2147483647 , path: '/' });
  	$("#about-1").hide();
    $("#about-2").show();
    if($.cookie('age')){
    	$("#about-you-age").val($.cookie('age'));
        $("#next-2").show();
    }
  });
   
  $("#about-you-age").on('change keyup', function() {
    if($(this).val()==""){
     $("#next-2").hide();
    }else {
     $("#next-2").show();
    }
 }); 
  
  $("#next-2").on("click", function(){
    
    if($("#about-you-age").val() >= 18){
      if($(".ageerror").hasClass("active")){
        $(".ageerror").removeClass("active");
      }
      $.cookie('age', $("#about-you-age").val(), { expires: 2147483647, path: '/' });
      localStorage.setItem('QUE_ID_DATA_2','A0');
      $.cookie('divtoopen', "about-2", { expires: 2147483647, path: '/' });
      
      $("#about-2").hide();
      $("#climate-1").show();
      //$(".count .pageno").text("4");
      if($.cookie('location')){
          $("#climate-1").hide();
          $("#climate-2").hide();
          $("#showloaction").text($.cookie('location'));
          $("#climate-3").show();
          //$(".count .pageno").text("3");
      }
    }else {
    	$(".ageerror").addClass("active");
    }
    
  });
  
  $(".locationbtn.auto").on("click", function(){
    if (navigator.geolocation) {
    	initialize(); // Get Current City
      	navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  	}
   	if($.cookie('location')){
        $("#showloaction").text($.cookie('location'));
        $('#location-load').css('display','none');
    }
    setTimeout(function(){
      $("#climate-1").hide();
      $("#climate-3").show();
      //$(".count .pageno").text("4");
    }, 1200);

    
  });
  
  $(".locationbtn.manual").on("click", function(){
    $("#climate-1").hide();
    $("#climate-2").show();
  });
  
  
  $("#locatemanually").on("click", function(){
    $.cookie('location', $("#climate-location").val(), { expires: 2147483647, path: '/' });
    $("#climate-2").hide();
    $("#climate-3").show();
    if($.cookie('location')){
    	$("#showloaction").text($.cookie('location'));
        $('#location-load').css('display','none');
    }
  });


  $("#next3").on("click", function(){
      $("#climate-3").hide();
      $("#climate-4").show();
      localStorage.setItem('QUE_ID_DATA_3', 'A0');
      $("#climate-4 .prog").addClass("active");
      setTimeout(function(){ 
        $("#climate-4").hide(); 
     }, 3000);
     setTimeout(function(){
       $("#skin-1").show();
     }, 3200);
  });
   

  
  $(".formlabel").on("click", function(){
      
      if ($(this).hasClass("active")) {
      
      } else {
        $(".formlabel").removeClass("active");
         $(this).addClass("active");
        $("#next1").show();
      }
    
    });
  
  $(".formlabelhalf").on("click", function(){
     if ($(this).parent().attr("id") == "lestbegin1" ){
            $("#genderselection-btn").show();
    }
    if ($(this).parent().attr("id") == "skininput1" ){
            $("#skin-btn-1").show();
    }
    if ($(this).parent().attr("id") == "skininput2") {
            $("#skin-btn-2").show();
    }  
     if ($(this).parent().attr("id") == "skininput3") {
            $("#skin-btn-3").show();
    }  
     if ($(this).parent().attr("id") == "skininput4") {
            $("#skin-btn-4").show();
    }  
    if ($(this).parent().attr("id") == "skininput5") {
            $("#skin-btn-5").show();
    }  
      if ($(this).parent().attr("id") == "skininput6") {
            $("#skin-btn-6").show();
    }  
    if ($(this).parent().attr("id") == "skininput7") {
            $("#skin-btn-7").show();
    }  
    if ($(this).parent().attr("id") == "skininput8") {
            $("#skin-btn-8").show();
    }  
    if ($(this).parent().attr("id") == "skininput9") {
            $("#skin-btn-9").show();
    } 
    if ($(this).parent().attr("id") == "skininput10") {
            $("#skin-btn-10").show();
    }
 
    if ($(this).parent().attr("id") == "airinput1") {
            $("#airquality-next1").show();
    }  
    if ($(this).parent().attr("id") == "airinput2") {
            $("#airquality-next2").show();
    }  
     if ($(this).parent().attr("id") == "airinput3") {
            $("#airquality-next3").show();
    }  
    if ($(this).parent().attr("id") == "uvinput1") {
            $("#uv-next1").show();
    }   
    
     if ($(this).parent().attr("id") == "sleepinput1") {
            $("#sleep-next1").show();
    } 
     if ($(this).parent().attr("id") == "sleepinput2") {
            $("#sleep-next2").show();
    } 
     if ($(this).parent().attr("id") == "sleepinput3") {
            $("#sleep-next3").show();
    }
    if ($(this).parent().attr("id") == "exerciseinput1") {
            $("#exercise-next1").show();
    }  
    if ($(this).parent().attr("id") == "exerciseinput2") {
            $("#exercise-next2").show();
    }  
    
    if ($(this).parent().attr("id") == "nutriinput1") {
            $("#nutrition-next1").show();
    }  
    if ($(this).parent().attr("id") == "nutriinput2") {
            $("#nutrition-next2").show();
    }  
    if ($(this).parent().attr("id") == "nutriinput3") {
            $("#nutrition-next3").show();
    }  
    if ($(this).parent().attr("id") == "nutriinput4") {
            $("#nutrition-next4").show();
    } 
     if ($(this).parent().attr("id") == "nutriinput5") {
            $("#nutrition-next5").show();
    } 
     if ($(this).parent().attr("id") == "mindinput1") {
            $("#mind-next1").show();
    } 
     if ($(this).parent().attr("id") == "mindinput2") {
            $("#mind-next2").show();
    } 
     if ($(this).parent().attr("id") == "mindinput3") {
            $("#mind-next3").show();
    } 
     if ($(this).parent().attr("id") == "mindinput4") {
            $("#mind-next4").show();
    } 
  });
  
  
  $("#skin-btn-1").on("click", function(){
    
    $.cookie('skingoal-1', $('input[name=skingoal-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('skingoal-2', $('input[name=skingoal-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('skingoal-3', $('input[name=skingoal-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('skingoal-4', $('input[name=skingoal-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('skingoal-5', $('input[name=skingoal-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('skingoal-6', $('input[name=skingoal-6]:checked').val(), { expires: 2147483647, path: '/' });
    
    $.cookie('divtoopen', "skin-1", { expires: 2147483647, path: '/' });
    $("#skin-1").hide();
    
    if($.cookie('skingoal-1') ){
        $("input[name='skingoal-1'][value='"+$.cookie('skingoal-1')+"']").attr('checked', 'checked');
    	$("#skin-2").show();
    }
    if($.cookie('skingoal-2') ){
        $("input[name='skingoal-2'][value='"+$.cookie('skingoal-2')+"']").attr('checked', 'checked');
    	$("#skin-2").show();
    }
    if($.cookie('skingoal-3') ){
        $("input[name='skingoal-3'][value='"+$.cookie('skingoal-3')+"']").attr('checked', 'checked');
    	$("#skin-2").show();
    }
    if($.cookie('skingoal-4') ){
        $("input[name='skingoal-4'][value='"+$.cookie('skingoal-4')+"']").attr('checked', 'checked');
    	$("#skin-2").show();
    }
    if($.cookie('skingoal-5') ){
        $("input[name='skingoal-5'][value='"+$.cookie('skingoal-5')+"']").attr('checked', 'checked');
    	$("#skin-2").show();
    }
    if($.cookie('skingoal-6') ){
        $("input[name='skingoal-6'][value='"+$.cookie('skingoal-6')+"']").attr('checked', 'checked');
    	$("#skin-2").show();
    }
    
   	if((!$("input[name='skingoal-1']").is(':checked')) && (!$("input[name='skingoal-2']").is(':checked')) && (!$("input[name='skingoal-3']").is(':checked')) && (!$("input[name='skingoal-4']").is(':checked')) && (!$("input[name='skingoal-5']").is(':checked')) && (!$("input[name='skingoal-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#skin-1").show();
      	$("#skin-2").hide();
    }
    else {
      $("#skin-1").hide();
      $("#skin-2").show();
    }
    $("#skin-2 input").each(function(){
      if($(this).is(":checked")){
        $("#skin-btn-2").show();
      }
    });
       
  });
  $("#skin-btn-2").on("click", function(){
    $.cookie('skinconcern', $('input[name=skinconcern]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_5', $('input[name=skinconcern]:checked').data("id"));
    $.cookie('divtoopen', "skin-2", { expires: 2147483647, path: '/' });
    $("#skin-2").hide();
    $("#skin-3").show();

    $("#skin-3 input").each(function(){
      if($(this).is(":checked")){
        $("#skin-btn-3").show();
      }
    });   
    
  });
  $("#skin-btn-3").on("click", function(){
    
    $.cookie('yoursensitivityorredness-1', $('input[name=yoursensitivityorredness-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('yoursensitivityorredness-2', $('input[name=yoursensitivityorredness-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('yoursensitivityorredness-3', $('input[name=yoursensitivityorredness-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('yoursensitivityorredness-4', $('input[name=yoursensitivityorredness-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('yoursensitivityorredness-5', $('input[name=yoursensitivityorredness-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('yoursensitivityorredness-6', $('input[name=yoursensitivityorredness-6]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('divtoopen', "skin-3", { expires: 2147483647, path: '/' });
//     $("#skin-3").hide();
//     $("#skin-4").show();
   if($.cookie('facecareroutine-1') ){
        $("input[name='facecareroutine-1'][value='"+$.cookie('facecareroutine-1')+"']").attr('checked', 'checked');
    }
    if($.cookie('facecareroutine-2') ){
        $("input[name='facecareroutine-2'][value='"+$.cookie('facecareroutine-2')+"']").attr('checked', 'checked');
    }
    if($.cookie('facecareroutine-3') ){
        $("input[name='facecareroutine-3'][value='"+$.cookie('facecareroutine-3')+"']").attr('checked', 'checked');
    }
    if($.cookie('facecareroutine-4') ){
        $("input[name='facecareroutine-4'][value='"+$.cookie('facecareroutine-4')+"']").attr('checked', 'checked');
    }
    if($.cookie('facecareroutine-5') ){
        $("input[name='facecareroutine-5'][value='"+$.cookie('facecareroutine-5')+"']").attr('checked', 'checked');
    }
    if($.cookie('facecareroutine-6') ){
        $("input[name='facecareroutine-6'][value='"+$.cookie('facecareroutine-6')+"']").attr('checked', 'checked');
    }
    
    if((!$("input[name='yoursensitivityorredness-1']").is(':checked')) && (!$("input[name='yoursensitivityorredness-2']").is(':checked')) && (!$("input[name='yoursensitivityorredness-3']").is(':checked')) && (!$("input[name='yoursensitivityorredness-4']").is(':checked')) && (!$("input[name='yoursensitivityorredness-5']").is(':checked')) && (!$("input[name='yoursensitivityorredness-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#skin-3").show();
    }
    else {
      $("#skin-3").hide();
      $("#skin-4").show();
    }
    
    $("#skin-3 input").each(function(){
      if($(this).is(":checked")){
        $("#skin-btn-4").show();
      }
    });

  });
   
  $("#skin-btn-4").on("click", function(){
    
    $.cookie('facecareroutine-1', $('input[name=facecareroutine-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('facecareroutine-2', $('input[name=facecareroutine-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('facecareroutine-3', $('input[name=facecareroutine-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('facecareroutine-4', $('input[name=facecareroutine-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('facecareroutine-5', $('input[name=facecareroutine-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('facecareroutine-6', $('input[name=facecareroutine-6]:checked').val(), { expires: 2147483647, path: '/' });
    
    $.cookie('divtoopen', "skin-4", { expires: 2147483647, path: '/' });
//     $("#skin-4").hide();
//     $("#skin-5").show();  
   if($.cookie('spfprotection-1')){
        $('input[name=spfprotection-1][value="'+$.cookie('spfprotection-1')+'"]').attr('checked', 'checked');
   		$("#skin-btn-5").show(); 
   }  
    if($.cookie('spfprotection-2')){
        $('input[name=spfprotection-2][value="'+$.cookie('spfprotection-2')+'"]').attr('checked', 'checked');
    	$("#skin-btn-5").show();
    }  
    if($.cookie('spfprotection-3')){
        $('input[name=spfprotection-3][value="'+$.cookie('spfprotection-3')+'"]').attr('checked', 'checked');
    	$("#skin-btn-5").show();
    }  
    if($.cookie('spfprotection-4')){
        $('input[name=spfprotection-4][value="'+$.cookie('spfprotection-4')+'"]').attr('checked', 'checked');
    	$("#skin-btn-5").show();
    }  
    if($.cookie('spfprotection-5')){
        $('input[name=spfprotection-5][value="'+$.cookie('spfprotection-5')+'"]').attr('checked', 'checked');
    	$("#skin-btn-5").show();
    }  
    if($.cookie('spfprotection-6')){
        $('input[name=spfprotection-6][value="'+$.cookie('spfprotection-6')+'"]').attr('checked', 'checked');
    	$("#skin-btn-5").show();
    }      
    
    if((!$("input[name='facecareroutine-1']").is(':checked')) && (!$("input[name='facecareroutine-2']").is(':checked')) && (!$("input[name='facecareroutine-3']").is(':checked')) && (!$("input[name='facecareroutine-4']").is(':checked')) && (!$("input[name='facecareroutine-5']").is(':checked')) && (!$("input[name='facecareroutine-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#skin-4").show();
    }
    else {
      $("#skin-4").hide();
      $("#skin-5").show();
    }
    
  });
   
$("#skin-btn-5").on("click", function(){
    $.cookie('spfprotection-1', $('input[name=spfprotection-1]:checked').val(), { expires: 2147483647, path: '/' });
  	$.cookie('spfprotection-2', $('input[name=spfprotection-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('spfprotection-3', $('input[name=spfprotection-3]:checked').val(), { expires: 2147483647, path: '/' });
  	$.cookie('spfprotection-4', $('input[name=spfprotection-4]:checked').val(), { expires: 2147483647, path: '/' });
  	$.cookie('spfprotection-5', $('input[name=spfprotection-5]:checked').val(), { expires: 2147483647, path: '/' });
  	$.cookie('spfprotection-6', $('input[name=spfprotection-6]:checked').val(), { expires: 2147483647, path: '/' });
  	$.cookie('divtoopen', "skin-5", { expires: 2147483647, path: '/' });
//     $("#skin-5").hide();
//     $("#skin-6").show();
  	if($.cookie('makeuproutine')){
        $('input[name=makeuproutine][value="'+$.cookie('makeuproutine')+'"]').attr('checked', 'checked');
        $("#skin-btn-6").show();
    }   
    if((!$("input[name='spfprotection-1']").is(':checked')) && (!$("input[name='spfprotection-2']").is(':checked')) && (!$("input[name='spfprotection-3']").is(':checked')) && (!$("input[name='spfprotection-4']").is(':checked')) && (!$("input[name='spfprotection-5']").is(':checked')) && (!$("input[name='spfprotection-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#skin-5").show();
    }
    else {
      $("#skin-5").hide();
      $("#skin-6").show();
    }
});
   
$("#skin-btn-6").on("click", function(){
    $.cookie('makeuproutine', $('input[name=makeuproutine]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_27', $('input[name=makeuproutine]:checked').data("id"));
    $.cookie('divtoopen', "skin-6", { expires: 2147483647, path: '/' });
    $("#skin-6").hide();
    $("#skin-7").show();
});
  
// For Oredr Id:10 and Que No: 29  
$("#skin-btn-7").on("click", function(){
    $.cookie('makeupoftergofor', $('input[name=makeupoftergofor]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_29', $('input[name=makeupoftergofor]:checked').data("id"));
	$.cookie('divtoopen', "skin-7", { expires: 2147483647, path: '/' });
    $("#skin-7").hide();
    $("#skin-8").show();   
  });
  
  // For Oder Id :11 and Que No :9  
$("#skin-btn-8").on("click", function(){
    $.cookie('feelskinoflegarme', $('input[name=feelskinoflegarme]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_9', $('input[name=feelskinoflegarme]:checked').data("id"));
  	$.cookie('divtoopen', "skin-8", { expires: 2147483647, path: '/' });
    $("#skin-8").hide();
    $("#skin-9").show();
  });
  
  // For Orer Id: 12 and Que No:28 
$("#skin-btn-9").on("click", function(){
    $.cookie('supplementforskinhealth', $('input[name=supplementforskinhealth]:checked').val(), { expires: 2147483647, path: '/' });
   localStorage.setItem('QUE_ID_DATA_28', $('input[name=supplementforskinhealth]:checked').data("id"));
  
	$.cookie('divtoopen', "skin-9", { expires: 2147483647, path: '/' });
    $("#skin-9").hide();
    $("#skin-10").show();
});

// For Order Id : 13 and Que No:25
$("#skin-btn-10").on("click", function(){
    $.cookie('supplementboostskin', $('input[name=supplementboostskin]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_25', $('input[name=supplementboostskin]:checked').data("id"));
  
	$.cookie('divtoopen', "skin-10", { expires: 2147483647, path: '/' });
    $("#skin-10").hide();
    //$(".count .pageno").text("6");
    $("#skin-11").show(); 
    $("#skin-11 .prog").addClass("active"); 
    setTimeout(function(){ 
      $("#skin-11").hide(); 
    }, 3000);
    setTimeout(function(){
     $("#airquality-1").show(); 
    }, 3200);
  
    $("#airquality-1 input").each(function(){
      if($(this).is(":checked")){
        $("#airquality-next1").show();
      }
    });
});
   
  $("#airquality-next1").on("click", function(){
    $.cookie('airgettowork-1', $('input[name=airgettowork-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('airgettowork-2', $('input[name=airgettowork-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('airgettowork-3', $('input[name=airgettowork-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('airgettowork-4', $('input[name=airgettowork-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('airgettowork-5', $('input[name=airgettowork-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('airgettowork-6', $('input[name=airgettowork-6]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('divtoopen', "airquality-1", { expires: 2147483647, path: '/' });
//     $("#airquality-1").hide();
//     $("#airquality-2").show();
    if($.cookie('airdoyousmoke')){
        $('input[name=airdoyousmoke][value="'+$.cookie('airdoyousmoke')+'"]').attr('checked', 'checked');
        $("#airquality-next2").show();
    } 
    if((!$("input[name='airgettowork-1']").is(':checked')) && (!$("input[name='airgettowork-2']").is(':checked')) && (!$("input[name='airgettowork-3']").is(':checked')) && (!$("input[name='airgettowork-4']").is(':checked')) && (!$("input[name='airgettowork-5']").is(':checked')) && (!$("input[name='airgettowork-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#airquality-1").show();
    }
    else {
      $("#airquality-1").hide();
      $("#airquality-2").show();
    }
    
  });

  // For Oredr Id is :15 Que id:11  
$("#airquality-next2").on("click", function(){
    $.cookie('airdoyousmoke', $('input[name=airdoyousmoke]:checked').val(), { expires: 2147483647, path: '/' });
   localStorage.setItem('QUE_ID_DATA_11', $('input[name=airdoyousmoke]:checked').data("id"));
  
    $.cookie('divtoopen', "airquality-2", { expires: 2147483647, path: '/' });
  
    $("#airquality-2").hide();
    $("#airquality-3").show();     
});

  // For order id is :16 / Que id: 12
$("#airquality-next3").on("click", function(){
    $.cookie('uvoutdoortime', $('input[name=uvoutdoortime]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_12', $('input[name=uvoutdoortime]:checked').data("id"));
  
    $.cookie('divtoopen', "airquality-3", { expires: 2147483647, path: '/' });
    $("#airquality-3").hide();
    $("#airquality-4").show(); 
    $("#airquality-4 .prog").addClass("active");
    setTimeout(function(){ 
      $("#airquality-4").hide();
    }, 3000);
    setTimeout(function(){
      $("#sleep-1").show(); 
    }, 3200);
}); 
 
  // For Oredr Id:17 / Que Id : 20
$("#sleep-next1").on("click", function(){
    $.cookie('typicalnightsleepinghours', $('input[name=typicalnightsleepinghours]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_20', $('input[name=typicalnightsleepinghours]:checked').data("id"));
  
	$.cookie('divtoopen', "sleep-1", { expires: 2147483647, path: '/' });
    $("#sleep-1").hide();
    $("#sleep-2").show();
});
  
// For Oredr Id:18 / Que Id : 21  
$("#sleep-next2").on("click", function(){
  $.cookie('feelwhenwakeupmorning', $('input[name=feelwhenwakeupmorning]:checked').val(), { expires: 2147483647, path: '/' });
  localStorage.setItem('QUE_ID_DATA_21', $('input[name=feelwhenwakeupmorning]:checked').data("id"));
  
  $.cookie('divtoopen', "sleep-2", { expires: 2147483647, path: '/' });
  $("#sleep-2").hide();
  $("#sleep-3").show();
});
  
// For Oredr Id:19 / Que Id : 22    
$("#sleep-next3").on("click", function(){
    $.cookie('dailydrinkcaffeinated', $('input[name=dailydrinkcaffeinated]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_22', $('input[name=dailydrinkcaffeinated]:checked').data("id"));
	$.cookie('divtoopen', "sleep-3", { expires: 2147483647, path: '/' });
    $("#sleep-3").hide();
    $("#sleep-4").show();
    $("#sleep-4 .prog").addClass("active");
    setTimeout(function(){ 
      $("#sleep-4").hide(); 
    }, 3000);
     
     setTimeout(function(){
      $("#mind-1").show(); 
    }, 3200);
 });
  
 // For Oredr Id:20 / Que Id : 23 
$("#mind-next1").on("click", function(){ 
    $.cookie('oftenaffectedbystress', $('input[name=oftenaffectedbystress]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_23', $('input[name=oftenaffectedbystress]:checked').data("id"));
	$.cookie('divtoopen', "mind-1", { expires: 2147483647, path: '/' });
    $("#mind-1").hide();
    $("#mind-2").show(); 
    $("#mind-2 .prog").addClass("active");
    setTimeout(function(){
      $("#mind-2").hide(); 
    }, 3000);
    
    setTimeout(function(){
      $("#uv-1").show(); 
    }, 3200);    
});

 // For Oredr Id:21 / Que Id : 23  
$("#uv-next1").on("click", function(){
    $.cookie('uvdigitaldevicehour', $('input[name=uvdigitaldevicehour]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_24', $('input[name=uvdigitaldevicehour]:checked').data("id"));
  
 	$.cookie('divtoopen', "uv-1", { expires: 2147483647, path: '/' });
    $("#uv-1").hide();
    $("#uv-3").show(); 
    $("#uv-3 .prog").addClass("active");
    setTimeout(function(){ 
      $("#uv-3").hide(); 

    }, 3000);
    
    setTimeout(function(){
      $("#nutrition-1").show(); 
    }, 3200);      
});

 // For Oredr Id:22 / Que Id : 18   
$("#nutrition-next1").on("click", function(){
    $.cookie('dailydrinkwater', $('input[name=dailydrinkwater]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_18', $('input[name=dailydrinkwater]:checked').data("id"));
  
 	$.cookie('divtoopen', "nutrition-1", { expires: 2147483647, path: '/' });
    $("#nutrition-1").hide();
    $("#nutrition-2").show();
});
  
 // For Oredr Id:23 / Que Id : 19  
$("#nutrition-next2").on("click", function(){
  
    $.cookie('weeklydrinkalcohol', $('input[name=weeklydrinkalcohol]:checked').val(), { expires: 2147483647, path: '/' });
    localStorage.setItem('QUE_ID_DATA_19', $('input[name=weeklydrinkalcohol]:checked').data("id"));
  
    $.cookie('divtoopen', "nutrition-2", { expires: 2147483647, path: '/' });
    $("#nutrition-2").hide();
    $("#nutrition-3").show();
});
  
  $("#nutrition-next3").on("click", function(){
    $.cookie('youcantlivewithout-1', $('input[name=youcantlivewithout-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('youcantlivewithout-2', $('input[name=youcantlivewithout-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('youcantlivewithout-3', $('input[name=youcantlivewithout-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('youcantlivewithout-4', $('input[name=youcantlivewithout-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('youcantlivewithout-5', $('input[name=youcantlivewithout-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('youcantlivewithout-6', $('input[name=youcantlivewithout-6]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('divtoopen', "nutrition-3", { expires: 2147483647, path: '/' });
//     $("#nutrition-3").hide();
//     $("#nutrition-4").show();
    if($.cookie('fruitandvegeatdaily-1')){
        $('input[name=fruitandvegeatdaily-1][value="'+$.cookie('fruitandvegeatdaily-1')+'"]').attr('checked', 'checked');
        $("#nutrition-next4").show();
    }
    if($.cookie('fruitandvegeatdaily-2')){
        $('input[name=fruitandvegeatdaily-2][value="'+$.cookie('fruitandvegeatdaily-2')+'"]').attr('checked', 'checked');
        $("#nutrition-next4").show();
    }
    if($.cookie('fruitandvegeatdaily-3')){
        $('input[name=fruitandvegeatdaily-3][value="'+$.cookie('fruitandvegeatdaily-3')+'"]').attr('checked', 'checked');
        $("#nutrition-next4").show();
    }
    if($.cookie('fruitandvegeatdaily-4')){
        $('input[name=fruitandvegeatdaily-4][value="'+$.cookie('fruitandvegeatdaily-4')+'"]').attr('checked', 'checked');
        $("#nutrition-next4").show();
    }
    if($.cookie('fruitandvegeatdaily-5')){
        $('input[name=fruitandvegeatdaily-5][value="'+$.cookie('fruitandvegeatdaily-5')+'"]').attr('checked', 'checked');
        $("#nutrition-next4").show();
    }
    if($.cookie('fruitandvegeatdaily-6')){
        $('input[name=fruitandvegeatdaily-6][value="'+$.cookie('fruitandvegeatdaily-6')+'"]').attr('checked', 'checked');
        $("#nutrition-next4").show();
    }
    
    if((!$("input[name='youcantlivewithout-1']").is(':checked')) && (!$("input[name='youcantlivewithout-2']").is(':checked')) && (!$("input[name='youcantlivewithout-3']").is(':checked')) && (!$("input[name='youcantlivewithout-4']").is(':checked')) && (!$("input[name='youcantlivewithout-5']").is(':checked')) && (!$("input[name='youcantlivewithout-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#nutrition-3").show();
        $("#nutrition-4").hide();
    }
    else {
      $("#nutrition-3").hide();
      $("#nutrition-4").show();
    }
     
  });
  
   $("#nutrition-next4").on("click", function(){
     $.cookie('fruitandvegeatdaily-1', $('input[name=fruitandvegeatdaily-1]:checked').val(), { expires: 2147483647, path: '/' });
     $.cookie('fruitandvegeatdaily-2', $('input[name=fruitandvegeatdaily-2]:checked').val(), { expires: 2147483647, path: '/' });
     $.cookie('fruitandvegeatdaily-3', $('input[name=fruitandvegeatdaily-3]:checked').val(), { expires: 2147483647, path: '/' });
     $.cookie('fruitandvegeatdaily-4', $('input[name=fruitandvegeatdaily-4]:checked').val(), { expires: 2147483647, path: '/' });
     $.cookie('fruitandvegeatdaily-5', $('input[name=fruitandvegeatdaily-5]:checked').val(), { expires: 2147483647, path: '/' });
     $.cookie('fruitandvegeatdaily-6', $('input[name=fruitandvegeatdaily-6]:checked').val(), { expires: 2147483647, path: '/' });
	 $.cookie('divtoopen', "nutrition-4", { expires: 2147483647, path: '/' });
//      $("#nutrition-4").hide();
//      $("#nutrition-5").show();
     if($.cookie('eatmorethanXXaweek-1')){
       $('input[name=eatmorethanXXaweek-1][value="'+$.cookie('eatmorethanXXaweek-1')+'"]').attr('checked', 'checked');
       $("#nutrition-next5").show();
     }
     if($.cookie('eatmorethanXXaweek-2')){
       $('input[name=eatmorethanXXaweek-2][value="'+$.cookie('eatmorethanXXaweek-2')+'"]').attr('checked', 'checked');
       $("#nutrition-next5").show();
     }
     if($.cookie('eatmorethanXXaweek-3')){
       $('input[name=eatmorethanXXaweek-3][value="'+$.cookie('eatmorethanXXaweek-3')+'"]').attr('checked', 'checked');
       $("#nutrition-next5").show();
     }
     if($.cookie('eatmorethanXXaweek-4')){
       $('input[name=eatmorethanXXaweek-4][value="'+$.cookie('eatmorethanXXaweek-4')+'"]').attr('checked', 'checked');
       $("#nutrition-next5").show();
     }
     if($.cookie('eatmorethanXXaweek-5')){
       $('input[name=eatmorethanXXaweek-5][value="'+$.cookie('eatmorethanXXaweek-5')+'"]').attr('checked', 'checked');
       $("#nutrition-next5").show();
     }
     if($.cookie('eatmorethanXXaweek-6')){
       $('input[name=eatmorethanXXaweek-6][value="'+$.cookie('eatmorethanXXaweek-6')+'"]').attr('checked', 'checked');
       $("#nutrition-next5").show();
     }
     
     if((!$("input[name='fruitandvegeatdaily-1']").is(':checked')) && (!$("input[name='fruitandvegeatdaily-2']").is(':checked')) && (!$("input[name='fruitandvegeatdaily-3']").is(':checked')) && (!$("input[name='fruitandvegeatdaily-4']").is(':checked')) && (!$("input[name='fruitandvegeatdaily-5']").is(':checked')) && (!$("input[name='fruitandvegeatdaily-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#nutrition-4").show();
        $("#nutrition-5").hide();
    }
    else {
      $("#nutrition-4").hide();
      $("#nutrition-5").show();
    }
     
  });
     
  $("#nutrition-next5").on("click", function(){
    $.cookie('eatmorethanXXaweek-1', $('input[name=eatmorethanXXaweek-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('eatmorethanXXaweek-2', $('input[name=eatmorethanXXaweek-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('eatmorethanXXaweek-3', $('input[name=eatmorethanXXaweek-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('eatmorethanXXaweek-4', $('input[name=eatmorethanXXaweek-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('eatmorethanXXaweek-5', $('input[name=eatmorethanXXaweek-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('eatmorethanXXaweek-6', $('input[name=eatmorethanXXaweek-6]:checked').val(), { expires: 2147483647, path: '/' });
	$.cookie('divtoopen', "nutrition-4", { expires: 2147483647, path: '/' });
//     $("#nutrition-5").hide();
//     $("#nutrition-6").show();
    $("#nutrition-6 .prog").addClass("active");
    setTimeout(function(){ 
      $("#nutrition-6").hide(); 

    }, 3000);
    setTimeout(function(){
      $("#exercise-1").show(); 
    }, 3200);
    //$(".count .pageno").text("22");
    
    if($.cookie('exercisekind-1') ){
        $('input[name=exercisekind-1][value="'+$.cookie('exercisekind-1')+'"]').attr('checked', 'checked');
    }
    if($.cookie('exercisekind-2') ){
        $('input[name=exercisekind-2][value="'+$.cookie('exercisekind-2')+'"]').attr('checked', 'checked');
    }
    if($.cookie('exercisekind-3') ){
        $('input[name=exercisekind-3][value="'+$.cookie('exercisekind-3')+'"]').attr('checked', 'checked');
    }
    if($.cookie('exercisekind-4') ){
        $('input[name=exercisekind-4][value="'+$.cookie('exercisekind-4')+'"]').attr('checked', 'checked');
    }
    if($.cookie('exercisekind-5') ){
        $('input[name=exercisekind-5][value="'+$.cookie('exercisekind-5')+'"]').attr('checked', 'checked');
    }
    if($.cookie('exercisekind-6') ){
        $('input[name=exercisekind-6][value="'+$.cookie('exercisekind-6')+'"]').attr('checked', 'checked');
    }
    $("#exercise-1 input").each(function(){
      if($(this).is(":checked")){
        $("#exercise-next1").show();
      }
    });
    
    if((!$("input[name='eatmorethanXXaweek-1']").is(':checked')) && (!$("input[name='eatmorethanXXaweek-2']").is(':checked')) && (!$("input[name='eatmorethanXXaweek-3']").is(':checked')) && (!$("input[name='eatmorethanXXaweek-4']").is(':checked')) && (!$("input[name='eatmorethanXXaweek-5']").is(':checked')) && (!$("input[name='eatmorethanXXaweek-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#nutrition-5").show();
      	$("#nutrition-6").hide();
    }
    else {
      $("#nutrition-5").hide();
      $("#nutrition-6").show();
    }

    
  });
  
  
$("#exercise-next1").on("click", function(){
    $.cookie('exercisekind-1', $('input[name=exercisekind-1]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('exercisekind-2', $('input[name=exercisekind-2]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('exercisekind-3', $('input[name=exercisekind-3]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('exercisekind-4', $('input[name=exercisekind-4]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('exercisekind-5', $('input[name=exercisekind-5]:checked').val(), { expires: 2147483647, path: '/' });
    $.cookie('exercisekind-6', $('input[name=exercisekind-6]:checked').val(), { expires: 2147483647, path: '/' });
      $.cookie('divtoopen', "exercise-1", { expires: 2147483647, path: '/' });
//     $("#exercise-1").hide();
//     $("#exercise-2").show();
    if($.cookie('exerciselastmonth')){
        $('input[name=exerciselastmonth][value="'+$.cookie('exerciselastmonth')+'"]').attr('checked', 'checked');
        $("#exercise-next2").show();
    }
  
  if((!$("input[name='exercisekind-1']").is(':checked')) && (!$("input[name='exercisekind-2']").is(':checked')) && (!$("input[name='exercisekind-3']").is(':checked')) && (!$("input[name='exercisekind-4']").is(':checked')) && (!$("input[name='exercisekind-5']").is(':checked')) && (!$("input[name='exercisekind-6']").is(':checked'))) {      
   		alert("Please select at least one answer");
      	$("#exercise-5").show();
    }
    else {
      $("#exercise-1").hide();
      $("#exercise-2").show();
    }
});
 
//For Oredr Id :28 / Que Id: 14
  $("#exercise-next2").on("click", function(){
    $.cookie('exerciselastmonth', $('input[name=exerciselastmonth]:checked').val(), { expires: 2147483647, path: '/' }); 
    localStorage.setItem('QUE_ID_DATA_14', $('input[name=exerciselastmonth]:checked').data("id")); 
    
    $.cookie('divtoopen', "exercise-2", { expires: 2147483647, path: '/' });
    $("#exercise-2").hide();
    $("#exercise-3").show(); 
    $("#exercise-3 .prog").addClass("active");
    setTimeout(function(){ 
      $("#exercise-3").hide(); 

    }, 3000);
     
    CreateJsonData(); 
  });
 
  
 /* AWs Create Json Formate 
     CreateJsonData
  */
function CreateJsonData(){
    $.cookie('oftenaffectedbystress', $('input[name=oftenaffectedbystress]:checked').val(), { expires: 2147483647, path: '/' });
	$.cookie('divtoopen', "mind-1", { expires: 2147483647, path: '/' });
      
      // Multiple Selection
      var youcantlivewithout = new Array();
      $("#nutriinput3 input:checkbox:checked").each(function (index, data) {
        	youcantlivewithout.push('"'+data.value+'"');
      });
      var fruitveg = new Array();
      $("#nutriinput4 input:checkbox:checked").each(function (index, data) {
        	fruitveg.push('"'+data.value+'"');
      }); 
      var xxtimeawek = new Array();
      $("#nutriinput5 input:checkbox:checked").each(function (index, data) {
        	xxtimeawek.push('"'+data.value+'"');
      });
      var skingoal = new Array();
      $("#skininput1 input:checkbox:checked").each(function (index, data) {
        	skingoal.push('"'+data.value+'"');
      });      
      var facecarerou = new Array();
      $("#skininput4 input:checkbox:checked").each(function (index, data) {
        	facecarerou.push('"'+data.value+'"');
      });     
      var yoursensitivityorredness = new Array();
      $("#skininput3 input:checkbox:checked").each(function (index, data) {
        	yoursensitivityorredness.push('"'+data.value+'"');
      });
      var facecarerou = new Array();
      $("#skininput4 input:checkbox:checked").each(function (index, data) {
        	facecarerou.push('"'+data.value+'"');
      });
      var spfpro = new Array(); // spfpro
      $("#skininput5 input:checkbox:checked").each(function (index, data) {
        	spfpro.push('"'+data.value+'"');
      });
       var airgettowork = new Array();
      $("#airinput1 input:checkbox:checked").each(function (index, data) {
        	airgettowork.push('"'+data.value+'"');
      });  
      var exercisekind = new Array();
      $("#exerciseinput1 input:checkbox:checked").each(function (index, data) {
        	exercisekind.push('"'+data.value+'"');
      }); 
      var exerciselastmonth = new Array();
      $("#exerciseinput2 input:checkbox:checked").each(function (index, data) {
        	exerciselastmonth.push('"'+data.value+'"');
      }); 

      // Multiple Selection Answer ID
      var QUE_4_data = new Array();
      var receiptNos = $("#skininput1 input:checkbox:checked").map(function () {
  		 QUE_4_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
      var QUE_6_data = new Array();
      var receiptNos = $("#skininput3 input:checkbox:checked").map(function () {
  		 QUE_6_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
  
      var QUE_7_data = new Array(); // #skininput5 input:checkbox:checked
      var receiptNos = $("#skininput4 input:checkbox:checked").map(function () {
  		 QUE_7_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
  
      var QUE_8_data = new Array();
      var receiptNos = $("#skininput5 input:checkbox:checked").map(function () {
  		 QUE_8_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
      var QUE_14_data = new Array();
      var receiptNos = $("#airinput1 input:checkbox:checked").map(function () {
  		 QUE_14_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
      var QUE_24_data = new Array();
      var receiptNos = $("#nutriinput3 input:checkbox:checked").map(function () {
  		 QUE_24_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
      var QUE_25_data = new Array();
      var receiptNos = $("#nutriinput4 input:checkbox:checked").map(function () {
  		 QUE_25_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
      var QUE_26_data = new Array();
      var receiptNos = $("#nutriinput5 input:checkbox:checked").map(function () {
  		 QUE_26_data.push('"'+$(this).attr('data-id')+'"');
	  }).get(); 
      var QUE_27_data = new Array();
      var receiptNos = $("#exerciseinput1 input:checkbox:checked").map(function () {
  		 QUE_27_data.push('"'+$(this).attr('data-id')+'"');
	  }).get();
     
      /* All Cookie */
      localStorage.setItem('QUE_ID_DATA_4', QUE_4_data);
      localStorage.setItem('QUE_ID_DATA_6', QUE_6_data);
      localStorage.setItem('QUE_ID_DATA_8', QUE_7_data);
      localStorage.setItem('QUE_ID_DATA_10', QUE_14_data);
      localStorage.setItem('QUE_ID_DATA_15', QUE_24_data);
      localStorage.setItem('QUE_ID_DATA_16', QUE_25_data);
      localStorage.setItem('QUE_ID_DATA_17', QUE_26_data);
      localStorage.setItem('QUE_ID_DATA_13', QUE_27_data);
      localStorage.setItem('QUE_ID_DATA_26', QUE_8_data);

      $.cookie('QUE_ANS_4', skingoal, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_6', yoursensitivityorredness, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_7', facecarerou, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_8', spfpro, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_14', airgettowork, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_24', youcantlivewithout, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_25', fruitveg, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_26', xxtimeawek, { expires: 2147483647, path: '/' });
      $.cookie('QUE_ANS_27', exercisekind, { expires: 2147483647, path: '/' });
       
      location.href = "/pages/selfie-skin"; // Redirect to Selfi Page..
}
$("#regimen-kit-btn").on("click", function(){
  	location.href = '/cart'
});
 
$("input#diagnostic-report-btn").on("click", function(){
    location.href = '/pages/diagnotic-detail'
});
  
  var sections = new theme.Sections();

  sections.register('cart-template', theme.Cart);
  sections.register('product', theme.Product);
  sections.register('collection-template', theme.Filters);
  sections.register('product-template', theme.Product);
  sections.register('header-section', theme.HeaderSection);
  sections.register('map', theme.Maps);
  sections.register('slideshow-section', theme.SlideshowSection);
  sections.register('quotes', theme.Quotes);
});

theme.init = function() {
  theme.customerTemplates.init();

  // Theme-specific selectors to make tables scrollable
  var tableSelectors = '.rte table,' + '.custom__item-inner--html table';

  slate.rte.wrapTable({
    $tables: $(tableSelectors),
    tableWrapperClass: 'scrollable-wrapper'
  });

  // Theme-specific selectors to make iframes responsive
  var iframeSelectors =
    '.rte iframe[src*="youtube.com/embed"],' +
    '.rte iframe[src*="player.vimeo"],' +
    '.custom__item-inner--html iframe[src*="youtube.com/embed"],' +
    '.custom__item-inner--html iframe[src*="player.vimeo"]';

  slate.rte.wrapIframe({
    $iframes: $(iframeSelectors),
    iframeWrapperClass: 'video-wrapper'
  });

  // Common a11y fixes
  slate.a11y.pageLinkFocus($(window.location.hash));

  $('.in-page-link').on('click', function(evt) {
    slate.a11y.pageLinkFocus($(evt.currentTarget.hash));
  });

  $('a[href="#"]').on('click', function(evt) {
    evt.preventDefault();
  });
};

$(document).ready(function() {
  	
  $('.container input').click(function() { 
    $(this).parents().eq(3).addClass('yellowbg'); 
  });
  
  $( ".width50.subscribeitem" ).on( "click", function() {
    if(($('.width100').hasClass("yellowbg")) && ($('.width50').hasClass("yellowbg"))) { 
      $('.regimenplan .subcartrefresh').addClass('activebtn');
      $('.regimenplan .cartrefresh p a').attr('disabled', true);
      $('.regimenplan .subcartrefresh p a').attr('disabled', false);
      $('.regimenplan .cartrefresh').removeClass('activebtn');
      $('.regimenplan .themecolor.selectregimenplan').css('display','none');
    }
  });
  $( ".width50.onetimeitem" ).on( "click", function() {
    if(($('.width100').hasClass("yellowbg")) && ($('.width50').hasClass("yellowbg"))) { 
      $('.regimenplan .cartrefresh').addClass('activebtn');
      $('.regimenplan .cartrefresh p a').attr('disabled', false);
      $('.regimenplan .subcartrefresh p a').attr('disabled', true);
      $('.regimenplan .subcartrefresh').removeClass('activebtn');
      $('.regimenplan .themecolor.selectregimenplan').css('display','none');
    }
  });
  
  $('.subscription > .width50.subscribeitem > .width25 input[type="radio"]').click(function() {
   	$('.regimenplan .cartrefresh p a').addClass('subscribeselected');
	$('.regimenplan .cartrefresh p a').removeClass('onetimeselected');
  });
  $('.subscription > .width50.onetimeitem > .width25 input[type="radio"]').click(function() {
      $('.regimenplan .cartrefresh p a').addClass('onetimeselected');
      $('.regimenplan .cartrefresh p a').removeClass('subscribeselected');
  });
  
  $(".container input:radio").attr("checked", false).parents().eq(3).removeClass('yellowbg'); 
  
    setTimeout(function(){
        $('.progressbar-data').addClass('intervalcls');
    }, 8000);
  
  $('#stage1 .container input#3').click(function() { 
    //var 3prodsubprice = $('.subscribeitem .width75 .themecolor.font30 strong'); 
    //document.getElementById("monthsub").innerHTML = "25";
   // document.getElementById("onesub").innerHTML = "30";    
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-44"]').css('display','none');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-55"]').css('display','none');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-11"]').css('width','33.3%');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-22"]').css('width','33.3%');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-33"]').css('width','33.3%');    
  });
  $('#stage2 .container input#5').click(function() { 
    //var 3prodsubprice = $('.subscribeitem .width75 .themecolor.font30 strong'); 
    //document.getElementById("monthsub").innerHTML = "45";
    //document.getElementById("onesub").innerHTML = "50";    
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-44"]').css('display','block');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-55"]').css('display','block');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-11"]').css('width','20%');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-22"]').css('width','20%');
//     $('.cartpage .tabsection ul.tabs-1 li[data-tab="tab-33"]').css('width','20%');    
  });
  
});
//Skin Page

$(document).ready(function() { 
    $('#sk-two').hide();
    $('#sk-three').hide();
  $("#ok-got-it").click( function() {
     $(window).scrollTop(0);
     $('#sk-one').hide();
     $('#sk-two').show();
  });
  $("#skin-two").click( function() {
      $(window).scrollTop(0);
     $('#sk-one').show();
  	 $('#sk-two').hide();
     $('#sk-three').hide();
  });
  $("#take-sk").click( function() {
     $(window).scrollTop(0);
     $('#sk-one').hide();
     $('#sk-three').show();
     $('#sk-two').hide();
     $("#java-s-hide-img").hide(); 
     startMirror(); // Selfi skin function call 
  }); 
  $("#skin-one").click( function() {
     location.href = '/pages/assessment';
  });
  
   $("#skip-face").click( function() {
      $(this).hide();
     $(".show-loading").css("display", "block");
      var allQuev = QueToAns();// Get all Que and Ans
      SendAwsRquest(allQuev);// Function call Product add to cart
      //location.href = '/pages/processing-results';
  });
});

/* Tab Start */
$("li.navbar-li").click(function () {
    $("li.navbar-li").removeClass("active");
    $(this).addClass("active");
    var cl = $(this).attr('id');
     $(".diagostic-details").removeClass("active");
    $('.'+cl).addClass("active");
});
/* Tab End */

$(document).ready(function() {
  $("#diagnotic-detail-back").click( function() {
     //location.href = '/pages/diagnotic';
     var urls = document.location.origin;
     window.location.href = urls+"/pages/diagnostic";
  });
  /* Skin Default Active  */
  	if(getlocalStorage('awsresponces','1') != null)
  	{
   		var skinData = getlocalStorage('awsresponces','1').ratings.Skin;
  	}
    if(getlocalStorage('questionnaires','1') != null)
    {
      var questionnaires = getlocalStorage('questionnaires','1');
    }  
            $('.diagostic-details.skin-p .title-th').css('color','#4a90e2');
            $('.diagnostic-detail-blogque-que-que').css('color','#4a90e2');

            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.skin span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.skin span.active-icon").css("display", "block");
        
            if(skinData != null){
                $(".java-skin-cat").text(skinData.SkinCategory);
                $(".java-skin-rating").text(skinData.SkinRating +'/5');
                if(skinData.SkinRating == 2){
                	$(".progress-circle.skin").addClass('progress-41');
                }else if(skinData.SkinRating == 3){
                    $(".progress-circle.skin").addClass('progress-60');
                }else if(skinData.SkinRating == 4){
                    $(".progress-circle.skin").addClass('progress-80');
                }else if(skinData.SkinRating == 5){
                    $(".progress-circle.skin").addClass('progress-100');
                }else if(skinData.SkinRating == 1){
                    $(".progress-circle.skin").addClass('progress-25');
                }else{
                    $(".progress-circle.skin").addClass('progress-0');
                }
            }
  		if(questionnaires != null){
             $(".java-skin-goal").text(((questionnaires[3].answer).toString()).replace(/\"/g, ""));
             $(".java-arms-legs").text(questionnaires[10].answer);
             $(".java-skin-concern").text(questionnaires[4].answer);
             $(".java-sensivity").text(((questionnaires[5].answer).toString()).replace(/\"/g, ""));  
         } 
              
   /* End */
   if($.cookie('name')){
    $(".ft-text").text($.cookie('name'));
  }
  /* Diagnostic page blog area question text color */
    $('.diagnotic-navbar .navbar-menu .navbar-li').on('click', function() {
        /* exercise  Start */
    	if ($('.diagostic-details.exercise-p').hasClass('active')) {
          if(getlocalStorage('awsresponces','1') != null){
          	var exrciseData = getlocalStorage('awsresponces','1').ratings.Exercise;
          }
          if(getlocalStorage('questionnaires','1') != null)
    	  {
      		var questionnaires = getlocalStorage('questionnaires','1');
    	  } 
            
            $('.diagostic-details.exercise-p .title-th').css('color','#1d9986');
            $('.diagnostic-detail-blogque-que-que').css('color','#1d9986');
            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.exercise span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.exercise span.active-icon").css("display", "block");
        
            if( exrciseData != null ){ // cat show
                $(".cat-exe").text(exrciseData.ExerciseCategory);
                $("span.java-exer-Rating").text(exrciseData.ExerciseRating +'/5');
              	   // Ratting Show
                  if(exrciseData.ExerciseRating == 2){
                      $(".progress-circle.exer").addClass('progress-41');
                  }else if(exrciseData.ExerciseRating == 3){
                      $(".progress-circle.exer").addClass('progress-60');
                  }else if(exrciseData.ExerciseRating == 4){
                      $(".progress-circle.exer").addClass('progress-80');
                  }else if(exrciseData.ExerciseRating == 5){
                      $(".progress-circle.exer").addClass('progress-100');
                  }else if(exrciseData.ExerciseRating == 1){
                      $(".progress-circle.exer").addClass('progress-25');
                  }else{
                      $(".progress-circle.exer").addClass('progress-0');
                  }
            }
          
          if(questionnaires != null){
          	 $(".java-exe-type").text(((questionnaires[26].answer).toString()).replace(/\"/g, ""));
             $(".java-exe-freq").text(questionnaires[27].answer);
          }
           // Blog Search
           if(localStorage.getItem('Blog-EXERCISE') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-EXERCISE'));
           }else {
                CreateBlogSearchTag(1,'EXERCISE');
           }
   		}
        /* exercise  End */
        /* diet Start  */
        if ($('.diagostic-details.diet-p').hasClass('active')) {
          	if(getlocalStorage('awsresponces','1') != null){
              var dietData = getlocalStorage('awsresponces','1').ratings.Diet;
            }
            if(getlocalStorage('questionnaires','1') != null){
      			var questionnaires = getlocalStorage('questionnaires','1');
            }
            
            $('.diagostic-details.diet-p .title-th').css('color','#83b83f');
            $('.diagnostic-detail-blogque-que-que').css('color','#83b83f');

            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.diet span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.diet span.active-icon").css("display", "block");
        
            if(dietData.DietCategory != null){
                $(".java-diet-cat").text(dietData.DietCategory);
                $(".java-diet-rating").text(dietData.DietRating +'/5');
                if(dietData.DietRating == 2){
                	$(".progress-circle.diet").addClass('progress-41');
                }else if(dietData.DietRating == 3){
                    $(".progress-circle.diet").addClass('progress-60');
                }else if(dietData.DietRating == 4){
                    $(".progress-circle.diet").addClass('progress-80');
                }else if(dietData.DietRating == 5){
                    $(".progress-circle.diet").addClass('progress-100');
                }else if(dietData.DietRating == 1){
                    $(".progress-circle.diet").addClass('progress-25');
                }else{
                    $(".progress-circle.diet").addClass('progress-0');
                }
            }

            // Questionnaire data's
          if(questionnaires != null){
              $(".java-preference").text(((questionnaires[23].answer).toString()).replace(/\"/g, ""));
              $(".java-hydration").text(questionnaires[21].answer);
              $(".java-fruit-veg").text(((questionnaires[24].answer).toString()).replace(/\"/g, ""));
              $(".java-alcohol-unit").text(questionnaires[22].answer);
          }

       	    // Blog Search
            if(localStorage.getItem('Blog-DIET') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-DIET'));
            }else {
                CreateBlogSearchTag(1,'DIET');
            }
        }
        /* Diet End */
        /* Skin Page Start */
        if ($('.diagostic-details.skin-p').hasClass('active')) {
          if(getlocalStorage('awsresponces','1') != null){
            var skinData = getlocalStorage('awsresponces','1').ratings.Skin;
          }
          if(getlocalStorage('questionnaires','1') != null){
      	    var questionnaires = getlocalStorage('questionnaires','1');
          }
            $('.diagostic-details.skin-p .title-th').css('color','#4a90e2');
            $('.diagnostic-detail-blogque-que-que').css('color','#4a90e2');

            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.skin span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.skin span.active-icon").css("display", "block");
        
            if(skinData != null){
                $(".java-skin-cat").text(skinData.SkinCategory);
                $(".java-skin-rating").text(skinData.SkinRating +'/5');
              	if(skinData.SkinRating == 2){
                	$(".progress-circle.skin").addClass('progress-41');
                }else if(skinData.SkinRating == 3){
                    $(".progress-circle.skin").addClass('progress-60');
                }else if(skinData.SkinRating == 4){
                    $(".progress-circle.skin").addClass('progress-80');
                }else if(skinData.SkinRating == 5){
                    $(".progress-circle.skin").addClass('progress-100');
                }else if(skinData.SkinRating == 1){
                    $(".progress-circle.skin").addClass('progress-25');
                }else{
                    $(".progress-circle.skin").addClass('progress-0');
                }
            }

          
            if(questionnaires != null){
             	$(".java-skin-goal").text(((questionnaires[3].answer).toString()).replace(/\"/g, ""));
             	$(".java-arms-legs").text(questionnaires[10].answer);
             	$(".java-skin-concern").text(questionnaires[4].answer);
             	$(".java-sensivity").text(((questionnaires[5].answer).toString()).replace(/\"/g, ""));  
         	} 
            // Blog Search    
            if(localStorage.getItem('Blog-SKIN') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-SKIN'));
            }else {
                CreateBlogSearchTag(2,'SKIN');
            }
        }
        /* Skin Page End */
        /* Air Quality Satrt */
        if ($('.diagostic-details.air-quality-p').hasClass('active')) {
          if(getlocalStorage('awsresponces','1')) {
            var AQIData = getlocalStorage('awsresponces','1').ratings.AQI;
          }
          if(getlocalStorage('questionnaires','1') != null){
      	    var questionnaires = getlocalStorage('questionnaires','1');
          }
            
            $('.diagostic-details.air-quality-p .title-th').css('color','#b74992');
            $('.diagnostic-detail-blogque-que-que').css('color','#b74992');
        
            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.air-quality span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.air-quality span.active-icon").css("display", "block");                                                   
        
            if(AQIData != null) {
                $(".java-air-index").text(AQIData.AQI);
                $("span.java-AQIRating").text(AQIData.AQIRating +'/5');
                $(".java-ait-cat").text(AQIData.AQICategory);
              	if(AQIData.AQIRating == 2){
                	$(".progress-circle.air").addClass('progress-41');
                }else if(AQIData.AQIRating == 3){
                    $(".progress-circle.air").addClass('progress-60');
                }else if(AQIData.AQIRating == 4){
                    $(".progress-circle.air").addClass('progress-80');
                }else if(AQIData.AQIRating == 5){
                    $(".progress-circle.air").addClass('progress-100');
                }else if(AQIData.AQIRating == 1){
                    $(".progress-circle.air").addClass('progress-25');
                }else{
                    $(".progress-circle.air").addClass('progress-0');
                }
            }
            
          if(questionnaires != null){ 
          	$(".java--air-hour-out").text(questionnaires[15].answer);
            $(".java-location-air").text(questionnaires[2].answer);
            $(".java-air-smo").text(questionnaires[14].answer);
          }
            // Blog Search
            if(localStorage.getItem('Blog-AQI') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-AQI'));
            }else {
                CreateBlogSearchTag(1,'AQI');
            }
        }
        /* Air Quality End */
        /* UV Start */
        if ($('.diagostic-details.uv-p').hasClass('active')){
            if(getlocalStorage('awsresponces','1') != null){
              	var UVData = getlocalStorage('awsresponces','1').ratings.uv;
            }
            if(getlocalStorage('questionnaires','1') != null){
      	    	var questionnaires = getlocalStorage('questionnaires','1');
          	}
            
            $('.diagostic-details.uv-p .title-th').css('color','#e3226c');
            $('.diagnostic-detail-blogque-que-que').css('color','#e3226c');
            
            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.uv span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.uv span.active-icon").css("display", "block");
            
            //Page Data Show
            if(UVData != null){
                $(".java-uv-index").text(UVData.UV_INDEX);
                $("span.java-UVRating").text(UVData.UVRating +'/5');
                $(".java-uv-cat").text(UVData.UVCategory);
                $(".java-uv-tag").text(UVData.UVTag);
                $(".java-skin-type").text(UVData.UVTags.ST); 
                if(UVData.UVRating == 2){
                  $(".progress-circle.uvr").addClass('progress-41');
                }else if(UVData.UVRating == 3){
                    $(".progress-circle.uvr").addClass('progress-60');
                }else if(UVData.UVRating == 4){
                    $(".progress-circle.uvr").addClass('progress-80');
                }else if(UVData.UVRating == 5){
                    $(".progress-circle.uvr").addClass('progress-100');
                }else if(UVData.UVRating == 1){
                    $(".progress-circle.uvr").addClass('progress-25');
                }else{
                    $(".progress-circle.uvr").addClass('progress-0');
                }
            }
          	if(questionnaires != null){ 
          		   $(".java-out-uv-hour").text(questionnaires[15].answer);
          	}  
            // Blog Search Start 
            if(localStorage.getItem('Blog-UV') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-UV'));
            }else {
                CreateBlogSearchTag(1,'UV');
            }
        }
        /* UV End */
        /* Climate Start */
        if ($('.diagostic-details.climate-p').hasClass('active')) {
          if(getlocalStorage('awsresponces','1') != null) {
            var climateData = getlocalStorage('awsresponces','1').ratings.climate;
          }
          if(getlocalStorage('questionnaires','1') != null){
      	     var questionnaires = getlocalStorage('questionnaires','1');
          }
            
            $('.diagostic-details.climate-p .title-th').css('color','#6d499a');
            $('.diagnostic-detail-blogque-que-que').css('color','#6d499a');
            
            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.climate span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.climate span.active-icon").css("display", "block");
                
                var result = $.cookie('resratings'),
                    cityId = climateData.cityId;

                    window.myWidgetParam ? window.myWidgetParam : window.myWidgetParam = []; window.myWidgetParam.push(
                        {id: 11,cityid: cityId ,appid: '5af950efe0dab591195a487a853b0c6b',units: 'metric',containerid: 'openweathermap-widget-11', }
                    );

                    (function() {var script = document.createElement('script');
                        script.async = true;script.charset = "utf-8";
                        script.src = "//openweathermap.org/themes/openweathermap/assets/vendor/owm/js/weather-widget-generator.js";
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(script, s); 
                    })();

            if(climateData != null){
                $(".java-temp").text(climateData.temp);
                $(".java-humi").text(climateData.humidity);
                $("span.java-climateRating").text(climateData.climateRating +'/5');
                $(".java-climate-cat").text(climateData.climateCategory);
                
              	if(climateData.climateRating == 2){
                	$(".progress-circle.cli").addClass('progress-41');
                }else if(climateData.climateRating == 3){
                    $(".progress-circle.cli").addClass('progress-60');
                }else if(climateData.climateRating == 4){
                    $(".progress-circle.cli").addClass('progress-80');
                }else if(climateData.climateRating == 5){
                    $(".progress-circle.cli").addClass('progress-100');
                }else if(climateData.climateRating == 1){
                    $(".progress-circle.cli").addClass('progress-25');
                }else{
                    $(".progress-circle.cli").addClass('progress-0');
                }
            }

            
            // Blog Search Start 
            if(localStorage.getItem('Blog-CLIMATE') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-CLIMATE'));
            }else {
                CreateBlogSearchTag(1,'CLIMATE');
            }       
        }
        /* Climate End*/
        /* Sleep Start */
        if ($('.diagostic-details.sleep-p').hasClass('active')) {
          if(getlocalStorage('awsresponces','1') != null){
            var SleepData = getlocalStorage('awsresponces','1').ratings.Sleep;
          }
          if(getlocalStorage('questionnaires','1') != null){
      	     var questionnaires = getlocalStorage('questionnaires','1');
          }
            $('.diagostic-details.sleep-p .title-th').css('color','#ed8017');
            $('.diagnostic-detail-blogque-que-que').css('color','#ed8017');
            
            // icon chnage
            $("span.non-ac-icon").css("display", "block");
            $("li.navbar-li.sleep span.non-ac-icon").css("display", "none");
            $("span.active-icon").css("display", "none");  
            $("li.navbar-li.sleep span.active-icon").css("display", "block");
            
            //Page Data Show
            if(SleepData != null){ // Category Chnage
                $(".java-sleep-cat").text(SleepData.SleepCategory);
                $("span.java-sleep-rating").text(SleepData.SleepRating +'/5');
              	if(SleepData.SleepRating == 2){
                	$(".progress-circle.sleep").addClass('progress-41');
                }else if(SleepData.SleepRating == 3){
                    $(".progress-circle.sleep").addClass('progress-60');
                }else if(SleepData.SleepRating == 4){
                    $(".progress-circle.sleep").addClass('progress-80');
                }else if(SleepData.SleepRating == 5){
                    $(".progress-circle.sleep").addClass('progress-100');
                }else if(SleepData.SleepRating == 1){
                    $(".progress-circle.sleep").addClass('progress-25');
                }else{
                    $(".progress-circle.sleep").addClass('progress-0');
                }
            }
                
            
            // Questionnaire data's
          if(questionnaires != null){ 
          	 $(".java-hours-per-night").text(questionnaires[16].answer);
             $(".java-sleep-quality").text(questionnaires[17].answer);
             $(".java-caffine-consumption").text(questionnaires[18].answer);
           }
            // Blog Search
            if(localStorage.getItem('Blog-MINDFULNESS') != null){
                $("#detais--blog-third-ans").text(localStorage.getItem('Blog-MINDFULNESS'));
            }else {
                CreateBlogSearchTag(1,'MINDFULNESS');
            }
        }
        /* Sleep End*/ 
    });
});

/* Gio Location start */
var geocoder;   
    //Get the latitude and the longitude;
    function successFunction(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        codeLatLng(lat, lng);
    }
    function errorFunction(){
        console.log("Geocoder failed");
    }
  	function initialize() {
    	geocoder = new google.maps.Geocoder();
  	}
  	function codeLatLng(lat, lng) {
    	var latlng = new google.maps.LatLng(lat, lng);
    	geocoder.geocode({'latLng': latlng}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                for (var i=0; i<results[0].address_components.length; i++) {
                  for (var b=0;b<results[0].address_components[i].types.length;b++) {
                      if (results[0].address_components[i].types[b] == "locality") {
                          city= results[0].address_components[i];
                          break;
                      }
                  }
              }
              $.cookie('location', city.long_name, { expires: 2147483647, path: '/' });
              $("#showloaction").text($.cookie('location'));
              $('#location-load').css('display','none');
               return city.long_name;
            } else {
              console.log("No results found");
            }
      	}else {
        console.log("Geocoder failed due to: " + status);
      }
    });
  }
/* Gio Location End */
/* get customer */
function Registercustomer(){
  var email = $("#reg-email").val(),
      pass = $("#reg-pass").val(),
	  data = {
    'customer[email]': email,
    'customer[password]': pass,
     form_type: 'create_customer',  
     utf8: '✓'
  };
  if(pass.length >= 9){
      $.ajax({
        url: '/account',
        crossDomain: true,
        beforeSend: function ( xhr ) {
          xhr.setRequestHeader( 'Authorization', 'Basic ' + Base64.encode( 'e6d9573cc005b5999a39cc318292c4f4:9fa30b1a2d5b99edbacb40733bfc0051' ) );
        },
        method: 'post',
        data: data,
        dataType: 'html',
        async: true,
        success: function(response){
      		getcustomer(email);
        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
 	 });      	  
   }
}
function getcustomer(email){
  $.ajax({
      type: 'GET',
      url: '/admin/customers.json',
      crossDomain: true,
      beforeSend: function ( xhr ) {
        xhr.setRequestHeader( 'Authorization', 'Basic ' + Base64.encode( 'e6d9573cc005b5999a39cc318292c4f4:9fa30b1a2d5b99edbacb40733bfc0051' ) );
      },
      async: false,
      dataType: 'json',
      success: function(responcess) {
        //console.log(responcess.customers[0].id);
        var RequestId = getlocalStorage('awsresponces','1').requestId,
         	custString = '{"requestId": "'+RequestId+'", "customerId": "'+ responcess.customers[0].id +'"}';
              	localStorage.setItem('lastupId',RequestId);
                localStorage.setItem('customerid',responcess.customers[0].id);
        		if(email != responcess.customers[0].email){
                  //location.href = 'challenge';
                  window.location.replace(window.location.protocol+'//'+window.location.host+'/challenge');
                }else{
                	updateCustomerId(JSON.parse(custString)); // Customer Id Update
                	$("#with-out-login").hide();
                	$("#save-and-show").show();
                }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("customer Not Found");
      }
    });
}
/* get customer END */
/* Product add to cart */
function cheoutoutFunction(){
  var selectedProducts = $("input:radio[name=products]:checked").val(),
        cart_items = $('#CartCount span:first-child').text();  
  
  if(selectedProducts == '3') {
    var first_p1 = $.cookie('Product-1'),
    	  second_p1 = $.cookie('Product-2'),
    	  third_p1 = $.cookie('Product-3'),
          MG1 =  {}; 
        MG1[first_p1] = 1;
        MG1[second_p1] = 1;
        MG1[third_p1]= 1;   
    var jsonPretty = JSON.stringify(MG1);
    if(cart_items == '5') {      
      jQuery.post('/cart/clear.js');
      		$.ajax({
                type: 'POST',
                url: '/cart/update.js', 
                data: {updates:MG1}, 
                dataType: 'json',
                success: function(responcess) { 
                    location.href = 'cart/checkout';
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error");
                    console.log(jqXHR.status);
                    console.log(textStatus);
                    console.log(errorThrown);
                }              	
            });
      
    }
    else {
      location.href = 'cart/checkout';
    }  
  }
  if(selectedProducts == '5') { 
    var first_p = $.cookie('Product-1'),
    	    second_p = $.cookie('Product-2'),
    	    third_p = $.cookie('Product-3'),
    	    four_p = $.cookie('Product-4'),
            five_p = $.cookie('Product-5'),
            MG =  {}; 
        MG[first_p] = 1;
        MG[second_p] = 1;
        MG[third_p]= 1;      
        MG[four_p] = 1;
        MG[five_p] = 1;
        var jsonPretty = JSON.stringify(MG);
        if(cart_items == '3') {
          jQuery.post('/cart/clear.js');
            $.ajax({
                type: 'POST',
                url: '/cart/update.js', 
                data: {updates:MG}, 
                dataType: 'json',
                success: function(responcess) { 
                    location.href = 'cart/checkout';
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error");
                    console.log(jqXHR.status);
                    console.log(textStatus);
                    console.log(errorThrown);
                }              	
            }); 
        }
        else {
            location.href = 'cart/checkout';
        }
    }    
}
// function cheoutoutFunction(){
//   var selectedProducts = $("input:radio[name=products]:checked").val(),
//         cart_items = $('#CartCount span:first-child').text();

//   if(selectedProducts == '3') {
//     if(cart_items == '5') {
//       jQuery.post('/cart/change.js', { line: 4, quantity: 0 });
//       jQuery.post('/cart/change.js', { line: 5, quantity: 0 });
//     }
//     location.href = 'cart/checkout';
//   }
//   if(selectedProducts == '5') {  
//         var first_p = $.cookie('Product-1'),
//     	    second_p = $.cookie('Product-2'),
//     	    third_p = $.cookie('Product-3'),
//     	    four_p = $.cookie('Product-4'),
//             five_p = $.cookie('Product-5'),
//             MG =  {}; 
//         MG[first_p] = 1;
//         MG[second_p] = 1;
//         MG[third_p]= 1;      
//         MG[four_p] = 1;
//         MG[five_p] = 1;

//         var jsonPretty = JSON.stringify(MG);
//         if(cart_items == '3') {
//             $.ajax({
//                 type: 'POST',
//                 url: '/cart/update.js', 
//                 data: {updates:MG}, 
//                 dataType: 'json',
//                 success: function(responcess) { 
//                     location.href = 'cart/checkout';
//                 },
//                 error: function(jqXHR, textStatus, errorThrown) {
//                     console.log("error");
//                     console.log(jqXHR.status);
//                     console.log(textStatus);
//                     console.log(errorThrown);
//                 }              	
//             }); 
//         }
//         else {
//             location.href = 'cart/checkout';
//         }
//     }    
// }
/* Product Remove to cart */

// Blog Artical Satrt 

/* Create Ans Tag 
  * function getAnsTag
*/

function getAnsTag(QueBlock){
  
   var QUE_ID_DATA_4 =   localStorage.getItem('QUE_ID_DATA_4').split(","),
       QUE_ID_DATA_11 =  localStorage.getItem('QUE_ID_DATA_11') ? 'Q11' + localStorage.getItem('QUE_ID_DATA_11') : '';

  if(QueBlock == 'S4') {
    
    if(jQuery.inArray("A2", QUE_ID_DATA_4) > -1) {
      return 'Q4A2';
    }

    return 'Default';
    
  } else if(QueBlock == 'B1') {
    
  	 return localStorage.getItem('QUE_ID_DATA_11') ? 'Q11' + localStorage.getItem('QUE_ID_DATA_11') : '';
         
  } else if(QueBlock == 'B3') {
    
    var climat = getlocalStorage('awsresponces','1').ratings.climate.humidity;

    if(climat < '35') {
       return 'Humidity.1';
    } else if(climat > '45'){
       return 'Humidity.3';
    }

    return 'Humidity.2';

  } else if(QueBlock == 'B4') {
    
    var B4_FinalANS = [];
    
    if(localStorage.getItem('QUE_ID_DATA_20') == 'A0' || localStorage.getItem('QUE_ID_DATA_20') == 'A1') {
      var QUE_ID_DATA_20 = 'Q20'+ localStorage.getItem('QUE_ID_DATA_20');
    } else if(localStorage.getItem('QUE_ID_DATA_20') == 'A2' || localStorage.getItem('QUE_ID_DATA_20') == 'A3' || localStorage.getItem('QUE_ID_DATA_20') == 'A4' ) {
      var QUE_ID_DATA_20 = 'default';
    }

    if(localStorage.getItem('QUE_ID_DATA_21') == 'A3') {
      var QUE_ID_DATA_21 = 'Q21'+localStorage.getItem('QUE_ID_DATA_21');
    } else if(localStorage.getItem('QUE_ID_DATA_21') == 'A0' || localStorage.getItem('QUE_ID_DATA_21') == 'A1'|| localStorage.getItem('QUE_ID_DATA_21') == 'A2')  {
      var QUE_ID_DATA_21 = 'default';
    }

    if(localStorage.getItem('QUE_ID_DATA_23') == 'A3'){
     var QUE_ID_DATA_23 = 'Q23'+localStorage.getItem('QUE_ID_DATA_23');
    }

    
    if(QUE_ID_DATA_20 == 'default' && QUE_ID_DATA_21 == 'Default' && QUE_ID_DATA_23 == 'default' ){
      return 'default';
    }else{
      if(QUE_ID_DATA_20){
         B4_FinalANS.push(QUE_ID_DATA_20);
      }
      if(QUE_ID_DATA_21){
        B4_FinalANS.push(QUE_ID_DATA_21);
      }
      if(QUE_ID_DATA_23){
       B4_FinalANS.push(QUE_ID_DATA_23);
      }
    }
    
    return B4_FinalANS;

  } else if(QueBlock == 'B8') {
     return  localStorage.getItem('QUE_ID_DATA_19') == 'A4' ? 'Q19A4' : 'default';
  }
}

/* Get Sku Tag Add  
 * function blogQue
*/

function blogQue(SKU){

    if(localStorage.getItem('Blog-'+SKU)){
        $("#"+ SKU).text(localStorage.getItem('Blog-'+SKU));
        if(localStorage.getItem('Blog-'+SKU) != null) return false;
    }
    var Blogtags = 0, //getBlogTags(),
      values = null;
        for(var i=1; i < 9; i++){
            if(values != null) return false;  
            var prioritycall = getAllTag(SKU,i,Blogtags);
            if(prioritycall != null ){
                values = prioritycall;
            }
        }
  if(values == null) {
    $("#"+ SKU).text("You will be happy with this great Skinsei product");
  }else{
  	$("#"+ SKU).text(values);
  }
}

/* priority wise add tag
 *  function getAllTag
*/
function getAllTag(sku,priority,blogTags){

 var    TopBF =  $.cookie('TopBenifits').split(","),
      	Top10 = TopBF.slice(0, 10),
        BlokRat = ["S4","B1","B3","B4","B8"],
        firstBf = null,
        SecondBf = null,
        prfirsttag = [];
     
    switch (priority) {
        case 1:
            firstBf = Top10[0];
            SecondBf = Top10[1];
            break;
        case 2:
            firstBf = Top10[0];
            SecondBf = Top10[2];
            break;
        case 3:
            firstBf = Top10[1];
            SecondBf = Top10[2];
            break;
        case 4:
            firstBf = Top10[0];
            SecondBf = Top10[3];
            break;
        case 5:
            firstBf = Top10[1];
            SecondBf = Top10[3];
            break;
        case 6:
            firstBf = Top10[2];
            SecondBf = Top10[3];
            break;
        case 7:
            firstBf = Top10[0];
            SecondBf = 'default';
            break;
        case 8:
            firstBf = Top10[1];
            SecondBf = 'default';
            break;
        default:
        	console.log("Not Found");
    }
        prfirsttag.push(sku);
        prfirsttag.push(firstBf);
        prfirsttag.push(SecondBf);
  
    if(jQuery.inArray(firstBf, BlokRat)  > -1 && jQuery.inArray(SecondBf, BlokRat)  > -1) {
        var first =  getAnsTag(firstBf),
            second=  getAnsTag(SecondBf);
        
      if(first.length == 2){
          first.forEach(function(value){
             prfirsttag.push(value);
          })
        
        } else {
          prfirsttag.push(first);
        }
        
        if(second.length == 2){
        	second.forEach(function(value){
            	prfirsttag.push(value);
        	})
        }else {
           prfirsttag.push(second);
        }
    } else if(jQuery.inArray(firstBf, BlokRat) > -1) {
      var first =  getAnsTag(firstBf);
      if(first.length == 2){
        	first.forEach(function(value){
            	 prfirsttag.push(value);
        	})
      }else {
      	 prfirsttag.push(first);
      }
    } else if(jQuery.inArray(SecondBf, BlokRat) > -1) {
        var first =  getAnsTag(SecondBf);
       if(first.length == 2){
        	first.forEach(function(value){
            	 prfirsttag.push(value);
        	})
       }else{
       	 prfirsttag.push(first);
       }
    }
    /* Blog Search */

    var blogToShow = null,
        stingsr = prfirsttag.toString(),
       quetag =  stingsr.split(",");
    $.unique(quetag);
    $.unique(prfirsttag);
    
    Object.keys(allBlogsc).forEach(function(key) {

      if(blogToShow != null) return false;

      var arrBlogTags = allBlogsc[key];
      var canShowThisBlog = quetag.every(function(tagId) {
          return arrBlogTags.indexOf(tagId) > -1;
      });

        if(canShowThisBlog) {
            var articals = getIdtoBlog(key); // Blog Id To get Articals
              	blogToShow = articals;
                $("#"+ sku).text(articals);
                localStorage.setItem('Blog-'+sku,articals);
        } else {
                  localStorage.setItem('Blog-'+sku,"You will be happy with this great Skinsei product");
            }
    })
    return blogToShow;
}

/* First char Get BF */
//getBefitisChar('S');
function getBefitisChar(vals){
    var Fillters = [];
  	if($.cookie('TopBenifits') == null){
  	  return null;
  	}
       var befitis = $.cookie('TopBenifits').split(",");
  
        befitis.forEach(function(values, index) {
            if(values.charAt(0) == vals){
                Fillters.push(values);
            }
        })
    return Fillters;
}

/* Blog Load 
 * function getBlogTags
*/

function getBlogTags(){
  var blogvalues = [];
   $.ajax({
          type: 'GET',
          //url: 'https://ab49e265ab7e187cda40bfe29a3b14a4:d8b3a4f5f7c21db8ba69e06e545a9e95@'+ window.location.hostname +'/admin/articles.json',
          url: 'https://starburstsandbox.myshopify.com/admin/articles.json?limit=250',
          crossDomain: true,
   				 beforeSend: function ( xhr ) {
        			xhr.setRequestHeader( 'Authorization', 'Basic ' + Base64.encode( 'e6d9573cc005b5999a39cc318292c4f4:9fa30b1a2d5b99edbacb40733bfc0051' ) );
    		},
    	  async: false,
          dataType: 'json',
            success: function(responcess) {
                responcess.articles.forEach(function(values, key){
                var tags = (values.tags),
                     tg = tags.replace(/\s/g,''), 
                     arrTag = tg.split(",");
                     blogvalues[values.id] = arrTag;
                })
            },
         error: function(jqXHR, textStatus, errorThrown) {
			console.log("error");
          	console.log(jqXHR.status);
          	console.log(textStatus);
          	console.log(errorThrown);
        }
    
       
    });
  return blogvalues;
}

function getIdtoBlog(blogid){
   var blogatrical = null;
    $.ajax({
                type: 'GET',
      			url: 'https://starburstsandbox.myshopify.com/admin/articles/'+blogid+'.json',
         		 crossDomain: true,
   				 beforeSend: function ( xhr ) {
        			xhr.setRequestHeader( 'Authorization', 'Basic ' + Base64.encode( 'e6d9573cc005b5999a39cc318292c4f4:9fa30b1a2d5b99edbacb40733bfc0051' ) );
    			},
      		  async: false,
              dataType: 'json',
              success: function(responcess) { 
                blogatrical =  responcess.article.body_html;
               }
            });
    return blogatrical;
}
// Blog Artical End

/* Details Page Blog Start */
function getQueIdtoTag(QueId){
     var QueNos =  QueId.split(","),
    FinalANSTags = [];

    QueNos.forEach(function(priority, index){
        switch (priority) {
            case 'Q1':
                var ansid =   localStorage.getItem('QUE_ID_DATA_1').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q2':
                var ansid =  localStorage.getItem('QUE_ID_DATA_2').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q3':
                var ansid =  localStorage.getItem('QUE_ID_DATA_3').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q4':
                var ansid =  localStorage.getItem('QUE_ID_DATA_4').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q5':
                var ansid =  localStorage.getItem('QUE_ID_DATA_5').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                console.log(ansid);
                break;
            case 'Q6':
                var ansid =  localStorage.getItem('QUE_ID_DATA_6').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q7':
                var ansid = localStorage.getItem('QUE_ID_DATA_7').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q8':
                var ansid =  localStorage.getItem('QUE_ID_DATA_8').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q9':
                var ansid =  localStorage.getItem('QUE_ID_DATA_9').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q10':
                var ansid =  localStorage.getItem('QUE_ID_DATA_10').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q11':
                var ansid =  localStorage.getItem('QUE_ID_DATA_11').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;
            case 'Q12':
                var ansid =  localStorage.getItem('QUE_ID_DATA_12').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q13':
                var ansid =  localStorage.getItem('QUE_ID_DATA_13').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q14':
                var ansid =  localStorage.getItem('QUE_ID_DATA_14').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q15':
                var ansid =  localStorage.getItem('QUE_ID_DATA_15').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q16':
                var ansid =  localStorage.getItem('QUE_ID_DATA_16').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q17':
                var ansid =  localStorage.getItem('QUE_ID_DATA_17').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q18':
                var ansid =  localStorage.getItem('QUE_ID_DATA_18').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q19':
                var ansid =  localStorage.getItem('QUE_ID_DATA_19').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q20':
                var ansid =  localStorage.getItem('QUE_ID_DATA_20').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q21':
                var ansid = localStorage.getItem('QUE_ID_DATA_21').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q22':
                var ansid =  localStorage.getItem('QUE_ID_DATA_22').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q23':
                var ansid =  localStorage.getItem('QUE_ID_DATA_23').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q24':
                var ansid =  localStorage.getItem('QUE_ID_DATA_24').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q25':
                var ansid =  localStorage.getItem('QUE_ID_DATA_25').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q26':
                var ansid =  localStorage.getItem('QUE_ID_DATA_26').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q27':
                var ansid =  localStorage.getItem('QUE_ID_DATA_27').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q28':
                var ansid =  localStorage.getItem('QUE_ID_DATA_28').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break; 
            case 'Q29':
                var ansid =  localStorage.getItem('QUE_ID_DATA_29').split(",");
                ansid.forEach(function(value, index){
                    FinalANSTags.push(priority+value);
                })
                break;                                                                                                                       
        }
    })
    return FinalANSTags;
   //console.log(FinalANSTags);
}

//getCategoryTotag('MINDFULNESS');

function getCategoryTotag(priority){
    var tags = [],
        topbf = getBefitisChar('S').slice(0,7);
  
    switch (priority) {
        case 'MINDFULNESS':
            tags.push(getQueIdtoTag("Q23,Q21"));
        
            if(jQuery.inArray("Q21A1", tags) > -1 ) {
                tags.push(getQueIdtoTag("Q20"));
            }
            if(jQuery.inArray("Q21A0", tags) > -1 ) {
               
            }else{
               tags.push(getQueIdtoTag("Q22"));
            }
        break;

        case 'EXERCISE':
            tags.push(getQueIdtoTag("Q14,Q13"));
            if(jQuery.inArray("Q13A3", tags) > -1 ) {
                tags.push("Q23");
            }
        break;
        
        case 'DIET':
            tags.push(getQueIdtoTag("Q19,Q18"));
        break;

        case 'SKIN':
            if(topbf[0] == 'S4' || topbf[1] == 'S4' ){
                tags.push(getQueIdtoTag("Q4"));
            }
        break;

        case 'AQI':
          if( $.cookie('AQIRating') < 51){
              tags.push("AQI.1");
          } else if($.cookie('AQIRating') > 50 && $.cookie('AQIRating') < 101 ) {
             tags.push("AQI.2");
          } else if($.cookie('AQIRating') > 100 && $.cookie('AQIRating') < 151) {
            tags.push("AQI.3");
          }else if($.cookie('AQIRating') > 150 ){
            tags.push("AQI.4");
          }
          
          if(localStorage.getItem('QUE_ID_DATA_12') == 'A0' || localStorage.getItem('QUE_ID_DATA_12') == 'A1'){
            tags.push("OD.1");
          } else if(localStorage.getItem('QUE_ID_DATA_12') == 'A2' || localStorage.getItem('QUE_ID_DATA_12') == 'A3' || localStorage.getItem('QUE_ID_DATA_12') == 'A4') {
            tags.push("OD.1");
          }
        
          if(localStorage.getItem('QUE_ID_DATA_11') == 'A0'){
            tags.push("SMK.1");
          } else if(localStorage.getItem('QUE_ID_DATA_11') == 'A1') {
            tags.push("SMK.1");
          } else if(localStorage.getItem('QUE_ID_DATA_11') == 'A2') {
            tags.push("SMK.3");
          } else if(localStorage.getItem('QUE_ID_DATA_11') == 'A3' || $.cookie('QUE_ID_DATA_12') == 'A4' ) {
            tags.push("SMK.4");
          }
        
        break;
        
      	case 'CLIMATE':
          if(getlocalStorage('awsresponces','1').ratings.climate.humidity < '35') {
             tags.push('Humidity.1');
          } else if(getlocalStorage('awsresponces','1').ratings.climate.humidity > '45'){
             tags.push('Humidity.3');
          } else {
            tags.push('Humidity.2');
          }
          var climate = getlocalStorage('awsresponces','1').ratings.climate.temp;
          if(climate < '40'){
             tags.push('Temperature.1');
          } else if(climate > '40' || climate< '50' ) {
            tags.push('Temperature.2');
          } else if(climate > '50' || climate < '60' ) {
            tags.push('Temperature.3');
          } else if(climate > '60' || climate < '70') {
            tags.push('Temperature.3');
          } else {
            tags.push('Temperature.4');
          }
        break;
        
        case 'UV':
        	if(localStorage.getItem('QUE_ID_DATA_12') == 'A0' ||  localStorage.getItem('QUE_ID_DATA_12') == 'A1' ){
              tags.push('OD.1');
            }else if(localStorage.getItem('QUE_ID_DATA_12') == 'A2' || localStorage.getItem('QUE_ID_DATA_12') == 'A3' ||  localStorage.getItem('QUE_ID_DATA_12') == 'A4' || $.cookie('QUE_ID_DATA_12') == 'A5') {
              tags.push('OD.2');
            }
            var skin = getlocalStorage('awsresponces','1').ratings.uv.UVTags.ST;
        	if( skin == 'ST1' || skin == 'ST2' ){
              	tags.push('ST.1');
            } else if(skin == 'ST3' || skin == 'ST4'){
             	tags.push('ST.2');
            } else if (skin == 'ST5') {
  				tags.push('ST.3');
            }
        	if($.cookie('uv-index') < '3'){
              tags.push('UV.1');
            } else if($.cookie('uv-index') > '3' || $.cookie('uv-index') < '6') {
              tags.push('UV.2');
            } else if($.cookie('uv-index') > '6') {
              tags.push('UV.3');
            }
        break;
    }
    
    return tags;
}


function CreateBlogSearchTag(priority,seconfpr){
     var  Blogtags = 0, //getBlogTags(),
      	  matchs  = null,
     	  prioritycall = null;
     
  switch (priority) {
      
        case 1:
          for(var i=1; i < 8; i++){
              if(matchs != null) return false;
              prioritycall = sellps(Blogtags,i,seconfpr);
              //console.log("Yuvi");
              if(prioritycall != null ){
                  matchs = prioritycall;
              }
          }
        break;
       case 2:
           prioritycall = sellps(Blogtags,'lg-2',seconfpr);
           if(prioritycall != null ){
                  matchs = prioritycall;
              }
        break;
    }
  
   if(matchs = null){
     localStorage.setItem('Blog-'+seconfpr,'You will be happy with this great Skinsei product');
     //$.cookie('Blog-'+seconfpr, "You will be happy with this great Skinsei product" ,{ expires: 2147483647, path: '/' });
     $("#detais--blog-third-ans").text("You will be happy with this great Skinsei product");
   }
     
}


function sellps(Blogtags,priority,secondPr){
  if( getBefitisChar('S') == null )
  {
  	return null;
  }
   var topbf = getBefitisChar('S').slice(0,7),
   // var topbf = ["S3","S4","S5","S6","S7","S1","S2"],
        blogToShow = null,
        Id = getCategoryTotag(secondPr),
        add=null;
  
	 switch (priority) {
        case 1:
        	add=topbf[0];
          break;
        case 2:
           add=topbf[1];
          break;
        case 3:
        	add=topbf[2];
          break;
      	case 4:
            add=topbf[3];
          break;
      	case 5:
            add=topbf[4];
          break;
      	case 6:
           add=topbf[5];
          break;
      	case 7:
           add=topbf[6];
          break;
       	case 'lg-2':
   		 	Id.push(topbf[0]);   
            Id.push(topbf[1]);   
         break;
    }
  
   Id.push(add);
   var stingc = Id.toString(),
       FinalAg = stingc.split(",");
       $.unique(FinalAg);
   Object.keys(allBlogsc).forEach(function(key) {

      if(blogToShow != null) return false;

      var arrBlogTags = allBlogsc[key];

      var canShowThisBlog = FinalAg.every(function(tagId) {
          return arrBlogTags.indexOf(tagId) > -1;
      });

        if(canShowThisBlog) {
            var articals = getIdtoBlog(key); // Blog Id To get Articals
              	blogToShow = articals;
                localStorage.setItem('Blog-'+secondPr, articals);
                $("#detais--blog-third-ans").text(articals);
        }else{
           localStorage.setItem('Blog-'+secondPr, "You will be happy with this great Skinsei product");
          $("#detais--blog-third-ans").text("You will be happy with this great Skinsei product");
        }
    })
   return blogToShow;
}

/* Details Page Blog Search End */

function setlocalStorage(name,values){
	localStorage.setItem(name,values);
  	return true;
}

function getlocalStorage(name,values){
  if(values){
  	return JSON.parse(localStorage.getItem(name));                
  }
   return localStorage.getItem(name);
}

/* Details Page Blog Search End */

/* All Js On Above */
/** QUE and ANS 
    function QueToAns
*/
  
function QueToAns(){
  
  var reqId = Math.random().toString(13).replace('0.', ''),
   	  letsbe = "Let's begin!",
   	  whereishome = "Where's home?",
   	  whatfacecarerou = "What's in your face care routine?",
   	  whatmakeuprou = "What's your makeup routine?",
   	  whatcantlive = "What can not you live without?";
  
  var dataString =  
    '{"requestId": "'+reqId+'","questionnaire": [{"questionId": "Q1", "OrderId": "1","category": "ABOUT YOU","question": "What should we call you?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_1') +'"] , "answer": ["'+ $.cookie('name') +'"]},'+

    '{"questionId": "Q2", "OrderId": "2","category": "ABOUT YOU","question": "What is your age?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_2') +'"] , "answer": ["'+ $.cookie('age') +'"]},'+

    '{"questionId": "Q3", "OrderId": "3","category": "CLIMATE","question": "'+whereishome+'","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_3') +'"] , "answer": ["'+ $.cookie('location') +'"]},'+

    '{"questionId": "Q4", "OrderId": "4","category": "SKIN","question": "Ok, face first. Describe your skin from forehead to chin.","answerType": "multiple", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_4') +'] , "answer": ['+ $.cookie('QUE_ANS_4') +']},'+

    '{"questionId": "Q5", "OrderId": "5","category": "SKIN","question": "How often do you breakout?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_5') +'"] , "answer": ["'+ $.cookie('skinconcern') + '"]},'+

    '{"questionId": "Q6", "OrderId": "6","category": "SKIN","question": "Any of these skin sensitivities sound familiar?","answerType": "multiple", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_6') +'] , "answer": ['+ $.cookie('QUE_ANS_6') + ']},'+

    '{"questionId": "Q8", "OrderId": "7","category": "SKIN","question": "Whats involved in your daily skincare routine?","answerType": "multiple", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_8') +'] , "answer": ['+ $.cookie('QUE_ANS_7') +']},'+

    '{"questionId": "Q26", "OrderId": "8","category": "SKIN","question": "What about makeup?","answerType": "multiple", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_26') +'] , "answer": ['+ $.cookie('QUE_ANS_8') +']},'+

    '{"questionId": "Q27", "OrderId": "9","category": "SKIN","question": "How much do you use?","answerType": "single","answerId": ["'+ localStorage.getItem('QUE_ID_DATA_27') +'"] , "answer": ["'+ $.cookie('makeuproutine') +'"]},'+

    '{"questionId": "Q29", "OrderId": "10","category": "SKIN","question": "What would you say is your main skin goal?","answerType": "single","answerId": ["'+ localStorage.getItem('QUE_ID_DATA_29') +'"] , "answer": ["'+ $.cookie('makeupoftergofor') +'"]},'+

    '{"questionId": "Q9", "OrderId": "11","category": "SKIN","question": "Where do you usually get your SPF?","answerType": "single","answerId": ["'+ localStorage.getItem('QUE_ID_DATA_9') +'"] , "answer": ["'+ $.cookie('feelskinoflegarme') +'"]},'+

    '{"questionId": "Q28", "OrderId": "12","category": "SKIN","question": "Hows the rest of your skin? Think shoulders to toes.","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_28') +'"] , "answer": ["'+ $.cookie('supplementforskinhealth') +'"]},'+

    '{"questionId": "Q25", "OrderId": "13","category": "SKIN","question": "Would you take supplements to boost your skins health?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_25') +'"] , "answer": ["'+ $.cookie('supplementboostskin') +'"]},'+
     
    '{"questionId": "Q10", "OrderId": "14","category": "AIR QUALITY","question": "Lets talk about everyday life. Whats your commute like?","answerType": "multiple", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_10') +'] , "answer": ['+ $.cookie('QUE_ANS_14') +']},'+

    '{"questionId": "Q11", "OrderId": "15","category": "AIR QUALITY","question": "How often do you smoke cigarettes?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_11') +'"] , "answer": ["'+ $.cookie('airdoyousmoke') +'"]},'+

    '{"questionId": "Q12", "OrderId": "16","category": "AIR QUALITY","question": "How long are you outside during daylight hours?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_12') +'"] , "answer": ["'+ $.cookie('uvoutdoortime') +'"]},'+

    '{"questionId": "Q20", "OrderId": "17","category": "SLEEP","question": "How much sleep are you getting each night?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_20') +'"] , "answer": ["'+ $.cookie('typicalnightsleepinghours') +'"]},'+

    '{"questionId": "Q21", "OrderId": "18","category": "SLEEP","question": "How do you feel when you wake up?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_21') +'"] , "answer": ["'+ $.cookie('feelwhenwakeupmorning') +'"]},'+

    '{"questionId": "Q22", "OrderId": "19","category": "SLEEP","question": "How many caffeinated beverages do you drink daily?","answerType": "single","answerId": ["'+ localStorage.getItem('QUE_ID_DATA_22') +'"] , "answer": ["'+ $.cookie('dailydrinkcaffeinated') +'"]},'+

    '{"questionId": "Q23", "OrderId": "20","category": "MIND","question": "How often do you feel overwhelmed by stress? ","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_23') +'"] , "answer": ["'+ $.cookie('oftenaffectedbystress') +'"]},'+

    '{"questionId": "Q24", "OrderId": "21","category": "UV","question": "How much time do you spend with digital devices?","answerType": "single","answerId": ["'+ localStorage.getItem('QUE_ID_DATA_24') +'"] , "answer": ["'+ $.cookie('uvdigitaldevicehour') +'"]},'+

    '{"questionId": "Q18", "OrderId": "22","category": "NUTRITION","question": "How many glasses of water do you drink each day?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_18') +'"] , "answer": ["'+ $.cookie('dailydrinkwater') +'"]},'+

    '{"questionId": "Q19", "OrderId": "23","category": "NUTRITION","question": "How much alcohol do you consume in a week?","answerType": "single","answerId": ["'+ localStorage.getItem('QUE_ID_DATA_19') +'"] , "answer": ["'+ $.cookie('weeklydrinkalcohol') + '"]},'+

    '{"questionId": "Q15", "OrderId": "24","category": "NUTRITION","question": "Any dietary restrictions?","answerType": "single", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_15') +'] , "answer": ['+ $.cookie('QUE_ANS_24') +']},'+

    '{"questionId": "Q16", "OrderId": "25","category": "NUTRITION","question": "What shows up most often on your plate?","answerType": "single", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_16') +'] , "answer": ['+ $.cookie('QUE_ANS_25') +']},'+

    '{"questionId": "Q17", "OrderId": "26","category": "NUTRITION","question": "How do you treat yourself ?","answerType": "single", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_17') +'] , "answer": ['+ $.cookie('QUE_ANS_26') + ']},'+

    '{"questionId": "Q13", "OrderId": "27","category": "EXERCISE","question": "Whats involved in your typical workout?","answerType": "multiple", "answerId": ['+ localStorage.getItem('QUE_ID_DATA_13') +'] , "answer": ['+ $.cookie('QUE_ANS_27') +']},'+

    '{"questionId": "Q14", "OrderId": "28","category": "EXERCISE","question": "How much exercise did you get last week?","answerType": "single", "answerId": ["'+ localStorage.getItem('QUE_ID_DATA_14') +'"] , "answer": ["'+ $.cookie('exerciselastmonth') +'"]}]}';
  
  var jsonObj = JSON.parse(dataString);
  setlocalStorage('questionnaires',JSON.stringify(jsonObj.questionnaire)); 
  return jsonObj;
}
function localQue(){
     var reqId = Math.random().toString(13).replace('0.', '');
	 var dataString =  
         '{"requestId": "'+reqId+'","questionnaire": '+localStorage.getItem("questionnaires")+'}';
    var jsonObj = JSON.parse(dataString);
    return jsonObj;

}
/* Selfi Responses Get */ 
function SendSelfi(SelfiData){
  var AllQUES = null;
  if(localStorage.getItem('selfi-now') == 'yes'){
  	 AllQUES = localQue();
  }else{
  	 AllQUES = QueToAns();
  }
  
  AllQUES['btbp'] = JSON.parse(SelfiData);
  $.each(AllQUES.btbp.skinConcerns,function(index,item) {
    delete item["Image"];
  });
   $.each(AllQUES.btbp.deepTags,function(index,item) {
     if(item.TagName == 'ORIGINAL_IMAGE'){
       $.each(item.TagValues,function(indexx,items) { 
       	 localStorage.setItem('ORIGINAL_IMAGE',items.Value);
       });
     }
   });
     /*console.log("Else", item.TagName);
     $.each(item.TagValues,function(index,;item) { 
       console.log(index);
       if(index == 'ORIGINAL_IMAGE'){
           delete item["Value"];
           console.log(item);
       }
     })*/
   var jsonPretty = JSON.stringify(AllQUES, null, '\t');
  	   SendAwsRquest(AllQUES); // AWS Function Call 
}
            
 /*
     Create AWS Send Que Data
     SendAwsRquest
 */
 function SendAwsRquest(jsonObj){
      console.log(JSON.stringify(jsonObj));
      $.ajax({
        headers	:	{ 
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': '*',
          'Authorization': 'Basic ' + btoa(skinseiskincareuser+':'+skinseiskincarepasswrd)
        },
        url: "https://www.skinseiskincare.com/api/v1/recommandation",
        type: 'POST',
        crossDomain: true,
        data: jsonObj,
        dataType: "json",

        success: function(data) {
 
            appProductsToCart(data);
            getbenefit(data.benefits);
            setlocalStorage('RequestId',data.requestId);
            setlocalStorage('awsresponces',JSON.stringify(data));
            setlocalStorage('new_cus','1');
        },
        error: function(jqXHR, textStatus, errorThrown) {
			console.log("error");
          	console.log(jqXHR.status);
          	console.log(textStatus);
          	console.log(errorThrown);
        }

      });
  }
/**
	appProductsToCart
  	Get product id using given products response from AWS based on SKU 
  */
 function appProductsToCart(data,redirect){
      var productsToAdd = {},
      	  arr = data.products.slice(0, 5),
       	  new_arr = arr.reverse();
      new_arr.forEach(function(product) {
          if(product.productId in allProducts){
              productsToAdd[allProducts[product.productId]] = 1;
          }
      })
      product_add(productsToAdd,redirect);
  }     
 
  function product_add(productsToAdd,redirect){

  	var productIds = Object.keys(productsToAdd); 
    $.cookie('Product-1', productIds[0], { expires: 1, path: '/' });
    $.cookie('Product-2', productIds[1], { expires: 1, path: '/' });
    $.cookie('Product-3', productIds[2], { expires: 1, path: '/' });
    $.cookie('Product-4', productIds[3], { expires: 1, path: '/' });
    $.cookie('Product-5', productIds[4], { expires: 1, path: '/' });
    $.ajax({
        type: "POST",
        url: '/cart/clear.js',
        data: '',
        async: false,
        dataType: 'json',
        success: function() { 
            $.ajax({
              type: 'POST',
              url: '/cart/update.js', 
              data: {updates:productsToAdd}, 
              dataType: 'json',
              success: function(responcess) { 
                if(redirect == null){
                   location.href = '/pages/processing-results';
                }else if(redirect == 'not_redirect'){
                  return null;
                }else {
                   location.href = redirect;
                }
              }
            }); 
        },
        error: function(XMLHttpRequest, textStatus) {
          $.ajax({
            type: 'POST',
            url: '/cart/update.js', 
            data: {updates:productsToAdd}, 
            dataType: 'json',
            success: function(responcess) { 
              if(redirect == null){
                location.href = '/pages/processing-results';
              }else if(redirect == 'not_redirect'){
                return null;
              }else {
                location.href = redirect;
              }
            }
          });
        }
      });
  } 
function getbenefit(data){
  var benifitsIds = [],
   	  benifit = [];
  data.forEach(function(benefit) {
       var ids = benefit.benefitId;
       if(benefit.benefitId[0] == 'B' || benefit.benefitId[0] == 'S'){
         	benifitsIds.push(benefit.benefitId);
         	benifit.push(benefit.benefit);
     	}
     })
  $.cookie('TopBenifits', benifitsIds, { expires: 1, path: '/' });
  $.cookie('benifit', benifit, { expires: 1, path: '/' });
}
if(performance.navigation.type == 2){
   location.reload(true);
}

/* AWS Rating & Category Response Data's*/
if(getlocalStorage('awsresponces','1') != null) {
	$(".java-exer-Rating").text(getlocalStorage('awsresponces','1').ratings.Exercise.ExerciseRating);
    $(".cat-exe").text(getlocalStorage('awsresponces','1').ratings.Exercise.ExerciseCategory);
} 

$('.dashtab-btn .dashtablinks').click(function() {
	if (!($('.dashtab-btn .dashtablinks.dash-summary').hasClass("active"))) {
		$('.dashboard-pg .dashborad-btns').css('display','none');
	}
	else {
		$('.dashboard-pg .dashborad-btns').css('display','block');
	}
});

// Trigger tab click for button
      $('.dashborad-btns .dashborad-diagnostic-details').click(function() {
        $('.dashtab-btn .dash-diagnostic').trigger('click');
        $('.dashboard-pg .dashborad-btns').css('display','none');
        document.documentElement.scrollTop = 0;
      });
      $('.dashborad-btns .dashborad-regimen').click(function() {
        $('.dashtab-btn .dash-regimen').trigger('click');
        $('.dashboard-pg .dashborad-btns').css('display','none');
        document.documentElement.scrollTop = 0;
      });

var lastmenucount =  $(".mobile-nav-wrapper .mobile-nav .mobile-nav__item").size();
  $(".mobile-nav-wrapper .mobile-nav .mobile-nav__item:nth-child("+lastmenucount+")").addClass("last-mobile-nav__item");
var menucount =  $(".mobile-nav-wrapper .mobile-nav .mobile-nav__item").size() - 1;
  $(".mobile-nav-wrapper .mobile-nav .mobile-nav__item:nth-child("+menucount+")").addClass("last-before-mobile-nav__item");

$('.site-header .site-header__icons-wrapper .site-header__menu.mobile-nav--open').click(function() {
  $('.site-header .site-header__icons-wrapper .site-header__search-toggle').toggle();
  $('.site-header .site-header__icons-wrapper .site-header__account').toggle();
  $('.site-header .site-header__icons-wrapper .site-header__cart').toggle();
});

$()

/* Diagnostic Details AWS Data BenefitID Rank 1 */
var rankskinbene = getBefitisChar('S'),
	rankskinbeneb = getBefitisChar('B');

if(rankskinbene != null){ 
  if (rankskinbene[0] == 'S1') {
    localStorage.setItem('benefit_01','Healthy skin');
    $('.ds-top-rank-benefit-1').text('Healthy skin');
  } else if (rankskinbene[0] == 'S2') {
     localStorage.setItem('benefit_01','Sensitive skin');
     $('.ds-top-rank-benefit-1').text('Sensitive skin');
  } else if (rankskinbene[0] == 'S3') {
    localStorage.setItem('benefit_01','Acne-prone skin');
    $('.ds-top-rank-benefit-1').text('Acne-prone skin');
  }else if (rankskinbene[0] == 'S4') {
    if ((localStorage.getItem('QUE_ID_DATA_4') == 'A2')) {
      localStorage.setItem('benefit_01','Combination skin');
      $('.ds-top-rank-benefit-1').text('Combination skin');
    }
    else {
      localStorage.setItem('benefit_01','Oily skin');
      $('.ds-top-rank-benefit-1').text('Oily skin');
    }
  }else if (rankskinbene[0] == 'S5') {
    localStorage.setItem('benefit_01','Dry skin');
    $('.ds-top-rank-benefit-1').text('Dry skin');
  }else if (rankskinbene[0] == 'S6') {
    localStorage.setItem('benefit_01','Uneven skin');
    $('.ds-top-rank-benefit-1').text('Uneven skin');
  }else if (rankskinbene[0] == 'S7') {
    localStorage.setItem('benefit_01','Aging skin');
    $('.ds-top-rank-benefit-1').text('Aging skin');
  }
}
/* Diagnostic Details AWS Data BenefitID Rank 2 */ 
//console.log(rankskinbeneb[1]);
if(rankskinbeneb != null){
  if (rankskinbeneb[1] == 'B1') {
    $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/air-quality-active.svg?16519926549871132732');
    if (localStorage.getItem('QUE_ID_DATA_11') == 'A3') {
      localStorage.setItem('benefit_02','High pollution');
      $('.ds-top-rank-benefit-2').text('High pollution');
    } else if (((localStorage.getItem('QUE_ID_DATA_11') == 'A0') || (localStorage.getItem('QUE_ID_DATA_11') == 'A2'))) {
      localStorage.setItem('benefit_02','Smoking damage');
      $('.ds-top-rank-benefit-2').text('Smoking damage');
    }else if ((localStorage.getItem('QUE_ID_DATA_11') == 'A1')) {
      localStorage.setItem('benefit_02','Smoke damage');
      $('.ds-top-rank-benefit-2').text('Smoke damage');
    }
  } 
  else if (rankskinbeneb[1] == 'B2') {
    $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/climate-active.svg?109164650182287377');
    localStorage.setItem('benefit_02','Cold climate');
    $('.ds-top-rank-benefit-2').text('Cold climate');
  } 
  else if (rankskinbeneb[1] == 'B3') {
    $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/climate-active.svg?109164650182287377');
    localStorage.setItem('benefit_02','Hot climate');
    $('.ds-top-rank-benefit-2').text('Hot climate');
  } 
  else if (rankskinbeneb[1] == 'B4') {
    $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/sleep-active.svg?8260723955278897233');
    if ((localStorage.getItem('QUE_ID_DATA_21') == 'A3') || (localStorage.getItem('QUE_ID_DATA_20') == 'A0') || (localStorage.getItem('QUE_ID_DATA_20') == 'A1')) {
      localStorage.setItem('benefit_02','Poor sleep');
      $('.ds-top-rank-benefit-2').text('Poor sleep');
    }
    if ((localStorage.getItem('QUE_ID_DATA_23') == 'A3') || (localStorage.getItem('QUE_ID_DATA_23') == 'A4')) {
      localStorage.setItem('benefit_02','High stress');
      $('.ds-top-rank-benefit-2').text('High stress');
    }
  } 
  else if (rankskinbeneb[1] == 'B5') {
    $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/exercise-active.svg?109164650182287377');
    localStorage.setItem('benefit_02','Active lifestyle');
    $('.ds-top-rank-benefit-2').text('Active lifestyle');
  } 
  else if (rankskinbeneb[1] == 'B6') {
      $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/uv-active.svg?109164650182287377');
      localStorage.setItem('benefit_02','High UV');
      $('.ds-top-rank-benefit-2').text('High UV');
  } 
  else if (rankskinbeneb[1] == 'B7') {
      $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/diet-active.svg');
      localStorage.setItem('benefit_02','Unbalanced Diet');
      $('.ds-top-rank-benefit-2').text('Unbalanced Diet');
  } 
  else if (rankskinbeneb[1] == 'B8') {
    $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/diet-active.svg');
    if ((localStorage.getItem('QUE_ID_DATA_19') == 'A4')) {
      localStorage.setItem('benefit_02','Bad hydration (alcohol)');
      $('.ds-top-rank-benefit-2').text('Bad hydration (alcohol)');
    }
    else {
      localStorage.setItem('benefit_02','Low water intake');
      $('.ds-top-rank-benefit-2').text('Low water intake');
    }    
  } 
  else if (rankskinbeneb[1] == 'B10') {
      $('.ds-top-rank-benefit-2-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/uv-active.svg?109164650182287377');
      localStorage.setItem('benefit_02','Blue light');
      $('.ds-top-rank-benefit-2').text('Blue light');
  }
}
/* Diagnostic Details AWS Data BenefitID Rank 3 */
if(rankskinbeneb != null){
  if (rankskinbeneb[2] == 'B1') {
    $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/air-quality-active.svg?16519926549871132732');
    if (localStorage.getItem('QUE_ID_DATA_11') == 'A3') {
      localStorage.setItem('benefit_03','High pollution');
      $('.ds-top-rank-benefit-3').text('High pollution');
    } else if (((localStorage.getItem('QUE_ID_DATA_11') == 'A0') || (localStorage.getItem('QUE_ID_DATA_11') == 'A2'))) {
      localStorage.setItem('benefit_03','Smoking damage');
      $('.ds-top-rank-benefit-3').text('Smoking damage');
    } else if ((localStorage.getItem('QUE_ID_DATA_11') == 'A1')) {
      localStorage.setItem('benefit_03','Smoke damage');
      $('.ds-top-rank-benefit-3').text('Smoke damage');
    }
  }
  else if (rankskinbeneb[2] == 'B2') {
    $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/climate-active.svg?109164650182287377');
    localStorage.setItem('benefit_03','Cold climate');
    $('.ds-top-rank-benefit-3').text('Cold climate');
  } 
  else if (rankskinbeneb[2] == 'B3') {
    $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/climate-active.svg?109164650182287377');
    localStorage.setItem('benefit_03','Hot climate');
    $('.ds-top-rank-benefit-3').text('Hot climate');
  } 
  else if (rankskinbeneb[2] == 'B4') {
    $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/sleep-active.svg?8260723955278897233');
    if ((localStorage.getItem('QUE_ID_DATA_21') == 'A3') || (localStorage.getItem('QUE_ID_DATA_20') == 'A0') || (localStorage.getItem('QUE_ID_DATA_20') == 'A1')) {
      localStorage.setItem('benefit_03','Poor sleep');
      $('.ds-top-rank-benefit-3').text('Poor sleep');
    }
    if ((localStorage.getItem('QUE_ID_DATA_23') == 'A3') || (localStorage.getItem('QUE_ID_DATA_23') == 'A4')) {
      localStorage.setItem('benefit_03','High stress');
      $('.ds-top-rank-benefit-3').text('High stress');
    }
  }
  else if (rankskinbeneb[2] == 'B5') {
    $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/exercise-active.svg?109164650182287377');
    localStorage.setItem('benefit_03','Active lifestyle');
    $('.ds-top-rank-benefit-3').text('Active lifestyle');
  }
  else if (rankskinbeneb[2] == 'B6') {
    $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/uv-active.svg?109164650182287377');
    localStorage.setItem('benefit_03','High UV');
    $('.ds-top-rank-benefit-3').text('High UV');
  } 
  else if (rankskinbeneb[2] == 'B7') {
      $('.ds-top-rank-benefit-3-img').attr('src', 'https://cdn.shopify.com/s/files/1/1891/4477/t/19/assets/diet-active.svg?9544524860848774422');
      localStorage.setItem('benefit_03','Unbalanced Diet');
      $('.ds-top-rank-benefit-3').text('Unbalanced Diet');
  } 
  else if (rankskinbeneb[2] == 'B8') {
    $('.ds-top-rank-benefit-3-img').attr('src', 'https://cdn.shopify.com/s/files/1/1891/4477/t/19/assets/diet-active.svg?9544524860848774422');
    if ((localStorage.getItem('QUE_ID_DATA_19') == 'A4')) {
      localStorage.setItem('benefit_03','Bad hydration (alcohol)');
      $('.ds-top-rank-benefit-3').text('Bad hydration (alcohol)');
    }
    else {
      localStorage.setItem('benefit_03','Low water intake');
      $('.ds-top-rank-benefit-3').text('Low water intake');
    }    
  } 
  else if (rankskinbeneb[2] == 'B10') {
      $('.ds-top-rank-benefit-3-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/uv-active.svg?109164650182287377');
      localStorage.setItem('benefit_03','Blue light');
      $('.ds-top-rank-benefit-3').text('Blue light');
  }
}
/* Diagnostic Details AWS Data BenefitID Rank 4 */
if(rankskinbeneb != null){
  
 if (rankskinbeneb[3] == 'B1') {
   $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/air-quality-active.svg?16519926549871132732');
    if (localStorage.getItem('QUE_ID_DATA_11') == 'A3') {
      $('.ds-top-rank-benefit-4').text('High pollution');
    }
    if (((localStorage.getItem('QUE_ID_DATA_11') == 'A0') || (localStorage.getItem('QUE_ID_DATA_11') == 'A2'))) {
      $('.ds-top-rank-benefit-4').text('Smoking damage');
    }
    if ((localStorage.getItem('QUE_ID_DATA_11') == 'A1')) {
      $('.ds-top-rank-benefit-4').text('Smoke damage');
    }
  } 
  else if (rankskinbeneb[3] == 'B2') {
    $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/climate-active.svg?109164650182287377');
    $('.ds-top-rank-benefit-4').text('Cold climate');
  } 
  else if (rankskinbeneb[3] == 'B3') {
    $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/climate-active.svg?109164650182287377');
    $('.ds-top-rank-benefit-4').text('Hot climate');
  } 
  else if (rankskinbeneb[3] == 'B4') {
    $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/19/assets/sleep-active.svg?8260723955278897233');
    if ((localStorage.getItem('QUE_ID_DATA_21') == 'A3') || (localStorage.getItem('QUE_ID_DATA_20') == 'A0') || (localStorage.getItem('QUE_ID_DATA_20') == 'A1')) {
      $('.ds-top-rank-benefit-4').text('Poor sleep');
    }
    if ((localStorage.getItem('QUE_ID_DATA_23') == 'A3') || (localStorage.getItem('QUE_ID_DATA_23') == 'A4')) {
      $('.ds-top-rank-benefit-4').text('High stress');
    }
  } 
  else if (rankskinbeneb[3] == 'B5') {
    $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/exercise-active.svg?109164650182287377');
    $('.ds-top-rank-benefit-4').text('Active lifestyle');
  } 
  else if (rankskinbeneb[3] == 'B6') {
      $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/uv-active.svg?109164650182287377');
      $('.ds-top-rank-benefit-4').text('High UV');
  } 
  else if (rankskinbeneb[3] == 'B7') {
      $('.ds-top-rank-benefit-4-img').attr('src', 'https://cdn.shopify.com/s/files/1/1891/4477/t/19/assets/diet-active.svg?9544524860848774422');
      $('.ds-top-rank-benefit-4').text('Unbalanced Diet');
  } 
  else if (rankskinbeneb[3] == 'B8') {
    $('.ds-top-rank-benefit-4-img').attr('src', 'https://cdn.shopify.com/s/files/1/1891/4477/t/19/assets/diet-active.svg?9544524860848774422');
    if ((localStorage.getItem('QUE_ID_DATA_19') == 'A4')) {
      $('.ds-top-rank-benefit-4').text('Bad hydration (alcohol)');
    }
    else {
      $('.ds-top-rank-benefit-4').text('Low water intake');
    }    
  } 
  else if (rankskinbeneb[3] == 'B10') {
      $('.ds-top-rank-benefit-4-img').attr('src', '//cdn.shopify.com/s/files/1/1891/4477/t/36/assets/uv-active.svg?109164650182287377');
      $('.ds-top-rank-benefit-4').text('Blue light');
  }
}


// Customer ID and Subscription ID (Order ID) send to AWS
function updateCustomerId(jsonObje){
      $.ajax({
        headers	:	{ 
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': '*',
          'Authorization': 'Basic ' + btoa(skinseiskincareuser+':'+skinseiskincarepasswrd)
        },
        url: "https://www.skinseiskincare.com/api/v1/updateCustomerId",
        type: 'POST',
        crossDomain: true,
        data: jsonObje,
        dataType: "text",
        success: function(data) {
          //console.log(data);
          $("#save-and-show").show();
          $("#with-out-login").hide();
          $("#allready-register").hide();
          return 'update successfully';
		},
        error: function(jqXHR, textStatus, errorThrown) {
           $("#save-and-show").hide();
           $("#with-out-login").hide();
           $("#allready-register").show();
        }
      });
  }
function getRecommandation(customerId, requestId){
	  $.ajax({
        headers	:	{ 
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': '*',
          'Authorization': 'Basic ' + btoa(skinseiskincareuser+':'+skinseiskincarepasswrd),
          'customerId': customerId
        },
        url: "https://www.skinseiskincare.com/api/v1/getRecommandation",
        type: 'GET',
        crossDomain: true,
        dataType: "json",
        async: false,
        success: function(data) {
         	 data[0]['benefits']= data[0].recommandation.requirements;
          	 data[0]['products']= data[0].recommandation.products;
             data[0]['ratings']= data[0].recommandation.ratings;
             setlocalStorage('questionnaires',JSON.stringify(data[0].questionnaires));
          	if(data[0].btbp != null){
              setlocalStorage('ORIGINAL_IMAGE',data[0].btbp.deepTags[7].TagValues[0].Value);
          	}
             delete data[0].recommandation;
             data = data[0];
             delete data[0];
             CreateQuedata(data);
             getbenefit(data.benefits);
             setlocalStorage('RequestId',data.requestId);
             setlocalStorage('awsresponces',JSON.stringify(data));
             appProductsToCart(data,'/account');
          	 
        },
        error: function(jqXHR, textStatus, errorThrown) {
          if(localStorage.getItem('new_cus') == null){
          		clearLocalStorage();
            	//clearcookies();
          }
        }

      });
}
function purchase(jsonObj){
	$.ajax({
        headers	:	{ 
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': '*',
          'Authorization': 'Basic ' + btoa(skinseiskincareuser+':'+skinseiskincarepasswrd)
        },
        url: "https://www.skinseiskincare.com/api/v1/purchase",
        type: 'POST',
        crossDomain: true,
        data: jsonObje,
        dataType: "json",
        success: function(data) {	
          		console.log(data);
			},
      	error: function(jqXHR, textStatus, errorThrown) {
			console.log("error");
          	console.log(jqXHR.status);
          	console.log(textStatus);
          	console.log(errorThrown);
        }
      });
}
function clearLocalStorage(){
    return localStorage.clear();
}
function clearcookies(){
	var cookies = $.cookie();
	for(var cookie in cookies) {
   		$.removeCookie(cookie);
	}
}
var hours = 24,
    now = new Date().getTime(),
    setupTime = localStorage.getItem('setupTime');
if (setupTime == null) {
    localStorage.setItem('setupTime', now)
} else {
    if(now-setupTime > hours*60*60*1000) {
        localStorage.clear()
        localStorage.setItem('setupTime', now);
    }
}
function CreateQuedata(data){
  
   	$.cookie('name', data.questionnaires[0].answer[0], { expires: 2147483647 , path: '/' });
    localStorage.setItem('QUE_ID_DATA_1', data.questionnaires[0].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_2', data.questionnaires[1].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_3', data.questionnaires[2].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_5', data.questionnaires[4].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_9', data.questionnaires[10].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_11', data.questionnaires[10].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_12', data.questionnaires[15].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_14', data.questionnaires[27].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_18', data.questionnaires[21].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_19', data.questionnaires[22].answerId[0]);    
    localStorage.setItem('QUE_ID_DATA_20', data.questionnaires[16].answerId[0]); 
    localStorage.setItem('QUE_ID_DATA_21', data.questionnaires[17].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_22', data.questionnaires[18].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_23', data.questionnaires[19].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_24', data.questionnaires[20].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_25', data.questionnaires[12].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_27', data.questionnaires[8].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_28', data.questionnaires[11].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_29', data.questionnaires[9].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_4', data.questionnaires[3].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_6', data.questionnaires[5].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_8', data.questionnaires[6].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_10', data.questionnaires[13].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_15', data.questionnaires[23].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_16', data.questionnaires[24].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_17', data.questionnaires[25].answerId[0]);
    localStorage.setItem('QUE_ID_DATA_13', data.questionnaires[26].answerId[0]);
}

// Disable other checkboxs if "None" selected
// For Que 6
$('.yoursensitivityorredness-6').change(function() { 
	if(!$('.yoursensitivityorredness-6').is(":checked")){
		$("#skininput3 label .radiore").removeAttr("checked");
	}
	else if($('.yoursensitivityorredness-6').is(":checked")){
		$("#skininput3 label .radiore").removeAttr("checked");
		$("#skininput3 label .radiore+.inputspan").css("opacity",".8");
		$('.yoursensitivityorredness-6').prop("checked",true);
		$('.yoursensitivityorredness-6+.inputspan').css("opacity","initial");
	}		
});
$('#skininput3 label .radiore').change(function() { 
	if($('.yoursensitivityorredness-6').is(":checked")){
		$("#skininput3 label .radiore").removeAttr("checked");
		$('.yoursensitivityorredness-6').prop("checked",true);
		$('.yoursensitivityorredness-6+.inputspan').css("opacity","initial");
	}
	else {
		$("#skininput3 label .radiore+.inputspan").css("opacity","initial");
	}
});

// For Que 8
$('.spfprotectio-6').change(function() { 
	if(!$('.spfprotectio-6').is(":checked")){
		$("#skininput5 label .radiore").removeAttr("checked");
	}
	else if($('.spfprotectio-6').is(":checked")){
		$("#skininput5 label .radiore").removeAttr("checked");
		$("#skininput5 label .radiore+.inputspan").css("opacity",".8");
		$('.spfprotectio-6').prop("checked",true);
		$('.spfprotectio-6+.inputspan').css("opacity","initial");
	}		
});
$('#skininput5 label .radiore').change(function() { 
	if($('.spfprotectio-6').is(":checked")){
		$("#skininput5 label .radiore").removeAttr("checked");
		$('.spfprotectio-6').prop("checked",true);
		$('.spfprotectio-6+.inputspan').css("opacity","initial");
	}
	else {
		$("#skininput5 label .radiore+.inputspan").css("opacity","initial");
	}
});

// For Que 14
$('.airgettowork6').change(function() { 
	if(!$('.airgettowork6').is(":checked")){
		$("#airinput1 label .radiore").removeAttr("checked");
	}
	else if($('.airgettowork6').is(":checked")){
		$("#airinput1 label .radiore").removeAttr("checked");
		$("#airinput1 label .radiore+.inputspan").css("opacity",".8");
		$('.airgettowork6').prop("checked",true);
		$('.airgettowork6+.inputspan').css("opacity","initial");
	}		
});
$('#airinput1 label .radiore').change(function() { 
	if($('.airgettowork6').is(":checked")){
		$("#airinput1 label .radiore").removeAttr("checked");
		$('.airgettowork6').prop("checked",true);
		$('.airgettowork6+.inputspan').css("opacity","initial");
	}
	else {
		$("#airinput1 label .radiore+.inputspan").css("opacity","initial");
	}
});

// For Que 24
$('.youcantlivewithout6').change(function() { 
	if(!$('.youcantlivewithout6').is(":checked")){
		$("#nutriinput3 label .radiore").removeAttr("checked");
	}
	else if($('.youcantlivewithout6').is(":checked")){
		$("#nutriinput3 label .radiore").removeAttr("checked");
		$("#nutriinput3 label .radiore+.inputspan").css("opacity",".8");
		$('.youcantlivewithout6').prop("checked",true);
		$('.youcantlivewithout6+.inputspan').css("opacity","initial");
	}		
});
$('#nutriinput3 label .radiore').change(function() { 
	if($('.youcantlivewithout6').is(":checked")){
		$("#nutriinput3 label .radiore").removeAttr("checked");
		$('.youcantlivewithout6').prop("checked",true);
		$('.youcantlivewithout6+.inputspan').css("opacity","initial");
	}
	else {
		$("#nutriinput3 label .radiore+.inputspan").css("opacity","initial");
	}
});

// For Que 25
$('.fruitandvegeatdaily6').change(function() { 
	if(!$('.fruitandvegeatdaily6').is(":checked")){
		$("#nutriinput4 label .radiore").removeAttr("checked");
	}
	else if($('.fruitandvegeatdaily6').is(":checked")){
		$("#nutriinput4 label .radiore").removeAttr("checked");
		$("#nutriinput4 label .radiore+.inputspan").css("opacity",".8");
		$('.fruitandvegeatdaily6').prop("checked",true);
		$('.fruitandvegeatdaily6+.inputspan').css("opacity","initial");
	}		
});
$('#nutriinput4 label .radiore').change(function() { 
	if($('.fruitandvegeatdaily6').is(":checked")){
		$("#nutriinput4 label .radiore").removeAttr("checked");
		$('.fruitandvegeatdaily6').prop("checked",true);
		$('.fruitandvegeatdaily6+.inputspan').css("opacity","initial");
	}
	else {
		$("#nutriinput4 label .radiore+.inputspan").css("opacity","initial");
	}
});

// For Que 26
$('.eatmorethanXXaweek6').change(function() { 
	if(!$('.eatmorethanXXaweek6').is(":checked")){
		$("#nutriinput5 label .radiore").removeAttr("checked");
	}
	else if($('.eatmorethanXXaweek6').is(":checked")){
		$("#nutriinput5 label .radiore").removeAttr("checked");
		$("#nutriinput5 label .radiore+.inputspan").css("opacity",".8");
		$('.eatmorethanXXaweek6').prop("checked",true);
		$('.eatmorethanXXaweek6+.inputspan').css("opacity","initial");
	}		
});
$('#nutriinput5 label .radiore').change(function() { 
	if($('.eatmorethanXXaweek6').is(":checked")){
		$("#nutriinput5 label .radiore").removeAttr("checked");
		$('.eatmorethanXXaweek6').prop("checked",true);
		$('.eatmorethanXXaweek6+.inputspan').css("opacity","initial");
	}
	else {
		$("#nutriinput5 label .radiore+.inputspan").css("opacity","initial");
	}
});

// For Que 27
$('.exercisekind-5').change(function() { 
	if(!$('.exercisekind-5').is(":checked")){
		$("#exerciseinput1 label .radiore").removeAttr("checked");
	}
	else if($('.exercisekind-5').is(":checked")){
		$("#exerciseinput1 label .radiore").removeAttr("checked");
		$("#exerciseinput1 label .radiore+.inputspan").css("opacity",".8");
		$('.exercisekind-5').prop("checked",true);
		$('.exercisekind-5+.inputspan').css("opacity","initial");
	}		
});
$('#exerciseinput1 label .radiore').change(function() { 
	if($('.exercisekind-5').is(":checked")){
		$("#exerciseinput1 label .radiore").removeAttr("checked");
		$('.exercisekind-5').prop("checked",true);
		$('.exercisekind-5+.inputspan').css("opacity","initial");
	}
	else {
		$("#exerciseinput1 label .radiore+.inputspan").css("opacity","initial");
	}
});

$(theme.init);